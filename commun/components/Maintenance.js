import React, {Component} from 'react';

import {Button} from 'primereact/button';

export default class Maintenace extends Component {

	render() {
		return <div className="exception-body  error-page">
			<div className="exception-type" style={{ backgroundColor: '#4caf50',  padding: '0px 100px 0 100px'}}>
				<img className="img-maintenance" src="/assets/layout/images/maintenance_v2.png" alt="ultima" />
			</div>

			<div className="card exception-panel" style={{  margin: '-18px auto 0 auto'}}>
				
				<h1>Maintenance</h1>
				<div className="exception-detail" style={{margin: '10px 0px 30px 0px'}}>Site en maintenance</div>
				<Button label="Retour" onClick={() => {window.location = "/"}} />
			</div>
		</div>
	}
}