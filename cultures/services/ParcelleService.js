import axios from "axios";

import { EnumConseilIrrigation } from "../../enums/enums";
import {
  IconError,
  IconConsigneGreen,
  IconConsigneOrange,
  IconConsigneRed,
  URIWSServeur,
} from "../../constants/constants";

import L from "leaflet";
//import leafletImage from 'leaflet-image';
import { EvenementService } from "../../services/EvenementService";
import { JournalService } from "../../services/JournalService";
import { UtilisateurService } from "../../services/UtilisateurService";
import { HabilitationService } from "../../services/HabilitationService";

export class ParcelleService {
  constructor() {
    this.evenementService = new EvenementService();
    this.journalService = new JournalService();
    this.utilisateurService = new UtilisateurService();
    this.habilitationService = new HabilitationService();
  }

  getMesParcelles() {
    return axios.get(URIWSServeur + "/ws/parcelle/mes-parcelles-list");
  }

  displayDashboardMesMesParcellesMap(vue, map, source, idUtilisateur) {
    var url = URIWSServeur + "/ws/parcelle/mes-parcelles";

    if (idUtilisateur && idUtilisateur > 0) {
      url += "?idUtilisateur=" + idUtilisateur;
    }

    axios
      .get(url, { cancelToken: source.token })
      .then((response) => response.data)
      .then((data) => {
        //alert(JSON.stringify(data))
        //console.log(data)
        // console.log(data.coordHG)
        //alert(data.length)

        var nbrs = data ? data.length : 0;

        if (nbrs == 0) {
          vue.setState({
            nbrTotalSituation: localStorage.getItem("nbr_situation_global"),
          });
        }

        vue.setState({ nbrParcelles: nbrs });
        vue.setState({ displayDialogBienvenue: nbrs == 0 ? true : false });
        vue.setState({ nbrAucuneIrrigation: 0 });
        vue.setState({ nbrVigilanceIrrigation: 0 });
        vue.setState({ nbrIrrigation: 0 });
        vue.setState({ nbrError: 0 });
        vue.setState({ mapDatasJournalieresParcelle: [] });

        vue.setState({ countLoadingParcelle: 0 });

        var markersArray = [];

        if (data.length > 0) {
          data.map((parcelle, idx) => {
            markersArray.push([parcelle.latitude, parcelle.longitude]);

            // {alert(JSON.stringify(parcelle))}

            axios
              .get(
                URIWSServeur + "/ws/etat/detect-etat?idParcelle=" + parcelle.id,
                { cancelToken: source.token }
              )
              .then((response) => response.data)
              .then((data) => {
                var iconConstraint = IconError;

                //alert(data.date);

                const parcelleWithDataJ = {
                  parcelle: parcelle,
                  data: data,
                  date: new Date(Date.parse(data.date)).toLocaleDateString(
                    "fr"
                  ),
                };
                vue.state.mapDatasJournalieresParcelle.push(parcelleWithDataJ);
                vue.setState({
                  mapDatasJournalieresParcelle:
                    vue.state.mapDatasJournalieresParcelle,
                });

                if (data && data.conseil) {
                  if (
                    data.conseil.code === EnumConseilIrrigation.INUTILE.code
                  ) {
                    iconConstraint = IconConsigneGreen;
                    vue.setState({
                      nbrAucuneIrrigation: vue.state.nbrAucuneIrrigation + 1,
                    });
                  } else if (
                    data.conseil.code ===
                    EnumConseilIrrigation.AVERTISSEMENT.code
                  ) {
                    iconConstraint = IconConsigneOrange;
                    vue.setState({
                      nbrVigilanceIrrigation:
                        vue.state.nbrVigilanceIrrigation + 1,
                    });
                  } else if (
                    data.conseil.code === EnumConseilIrrigation.IRRIGATION.code
                  ) {
                    iconConstraint = IconConsigneRed;
                    vue.setState({
                      nbrIrrigation: vue.state.nbrIrrigation + 1,
                    });
                  } else {
                    iconConstraint = IconError;
                    vue.setState({ nbrError: vue.state.nbrError + 1 });
                  }
                } else {
                  iconConstraint = IconError;
                  vue.setState({ nbrError: vue.state.nbrError + 1 });
                }

                if (map) {
                  var rectangle = new L.rectangle(
                    L.latLngBounds(
                      [parcelle.latitude - 0.005, parcelle.longitude - 0.005],
                      [parcelle.latitude + 0.005, parcelle.longitude + 0.005]
                    ),
                    {
                      parcelle: parcelle,
                      color: data.conseil.couleur,
                      weight: 1,
                    }
                  )
                    .on("click", vue.onClickMarker)
                    .on("click", vue.onClickPolygon)
                    .addTo(map);
                  vue.state.mapParcelles.push(rectangle);

                  // pb si continue a charger et on a achanger d epage
                  var marker = new L.Marker(
                    [parcelle.latitude, parcelle.longitude],
                    { icon: iconConstraint, parcelle: parcelle }
                  )
                    .on("click", vue.onClickMarker)
                    .addTo(map);
                  vue.state.mapParcelles.push(marker);
                }

                //var marker = new L.CircleMarker([parcelle.latitude, parcelle.longitude], { radius: 20, parcelle: parcelle, color: data.conseil.couleur, weight: 1 }).on('click', vue.onClickMarker).addTo(map);
                //vue.state.mapParcelles.push(marker);

                vue.setState({
                  countLoadingParcelle: vue.state.countLoadingParcelle + 1,
                });

                if (vue.state.countLoadingParcelle === vue.state.nbrParcelles) {
                  vue.setState({ loadingParcelles: false });
                }
              });

            return parcelle;
          });
        } else {
          vue.setState({ loadingParcelles: false });
        }

        if (markersArray && markersArray.length > 1) {
          const bounds = window.L.latLngBounds(markersArray);

          // fitbounds marche que si ya 2 markers !
          if (bounds.isValid()) {
            map.fitBounds(bounds, {
              padding: [10, 10],
            });
          }
        } else if (data && data.length === 1) {
          vue.setState({ mapCenter: [data[0].latitude, data[0].longitude] });
        }
      })
      .catch((err) => {
        vue.setState({ isErrorLoading: true });
        vue.setState({ loadingParcelles: false });
      });
  }

  displayMesMesParcellesMap(vue, dateCarte, map, source) {
    vue.setState({ isLoadingPixels: true });
    vue.setState({ countLoadingParcelle: 0 });

    axios
      .get(URIWSServeur + "/ws/parcelle/mes-parcelles", {
        cancelToken: source.token,
      })
      .then((response) => response.data)
      .then((data) => {
        vue.setState({ nbrParcelles: data ? data.length : 0 });
        vue.setState({ mapDatasJournalieresParcelle: [] });

        var markersArray = [];

        if (data.length > 0) {
          data.map((parcelle) => {
            markersArray.push([parcelle.latitude, parcelle.longitude]);

            if (map) {
              var uri =
                URIWSServeur + "/ws/etat/detect-etat?idParcelle=" + parcelle.id;

              if (dateCarte) {
                uri += "&datereference=" + dateCarte.getTime();
              }

              axios
                .get(uri, { cancelToken: source.token })
                .then((response) => response.data)
                .then((data) => {
                  var iconConstraint = IconError;

                  const parcelleWithDataJ = { parcelle: parcelle, data: data };
                  vue.state.mapDatasJournalieresParcelle.push(
                    parcelleWithDataJ
                  );
                  vue.setState({
                    mapDatasJournalieresParcelle:
                      vue.state.mapDatasJournalieresParcelle,
                  });

                  if (
                    data.conseil.code === EnumConseilIrrigation.INUTILE.code
                  ) {
                    iconConstraint = IconConsigneGreen;
                    vue.setState({
                      nbrAucuneIrrigation: vue.state.nbrAucuneIrrigation + 1,
                    });
                  } else if (
                    data.conseil.code ===
                    EnumConseilIrrigation.AVERTISSEMENT.code
                  ) {
                    iconConstraint = IconConsigneOrange;
                    vue.setState({
                      nbrVigilanceIrrigation:
                        vue.state.nbrVigilanceIrrigation + 1,
                    });
                  } else if (
                    data.conseil.code === EnumConseilIrrigation.IRRIGATION.code
                  ) {
                    iconConstraint = IconConsigneRed;
                    vue.setState({
                      nbrIrrigation: vue.state.nbrIrrigation + 1,
                    });
                  } else {
                    iconConstraint = IconError;
                    vue.setState({ nbrError: vue.state.nbrError + 1 });
                  }

                  var rectange = new L.rectangle(
                    L.latLngBounds(
                      [parcelle.latitude - 0.005, parcelle.longitude - 0.005],
                      [parcelle.latitude + 0.005, parcelle.longitude + 0.005]
                    ),
                    {
                      parcelle: parcelle,
                      color: data.conseil.couleur,
                      weight: 1,
                    }
                  )
                    .on("click", vue.onClickMarker)
                    .addTo(map);
                  vue.state.mapParcelles.push(rectange);

                  var marker = new L.Marker(
                    [parcelle.latitude, parcelle.longitude],
                    { icon: iconConstraint, parcelle: parcelle }
                  )
                    .on("click", vue.onClickMarker)
                    .addTo(map);
                  vue.state.mapParcelles.push(marker);

                  vue.setState({
                    countLoadingParcelle: vue.state.countLoadingParcelle + 1,
                  });

                  if (
                    vue.state.countLoadingParcelle === vue.state.nbrParcelles
                  ) {
                    vue.setState({ isLoadingPixels: false });
                  }
                })

                .catch((err) => {
                  //     vue.setState({ isErrorLoading: true });

                  if (!axios.isCancel(err)) {
                    vue.setState({ isLoadingPixels: false });
                  }
                });
            }

            return parcelle;
          });
        } else {
          vue.setState({ loadingParcelles: false });
          vue.setState({ isLoadingPixels: false });
        }

        if (markersArray && markersArray.length > 1) {
          const bounds = window.L.latLngBounds(markersArray);

          // fitbounds marche que si ya 2 markers !
          if (bounds.isValid()) {
            map.fitBounds(bounds, {
              padding: [10, 10],
            });
          }
        } else if (data && data.length === 1) {
          vue.setState({ mapCenter: [data[0].latitude, data[0].longitude] });
        }
      })
      .catch((err) => {
        if (!axios.isCancel(err)) {
          vue.setState({ isErrorLoading: true });
          vue.setState({ loadingParcelles: false });
        }

        vue.setState({ isLoadingPixels: false });
      });
  }

  nbrJourModificationRestantGlobal(vue) {
    axios
      .get(URIWSServeur + "/ws/parcelle/nbr-jour-restant", {
        cancelToken: vue.state.source.token,
      })
      .then((res) => res.data)
      .then((responseJson) => {
        vue.setState({ nbrJours: responseJson });

        vue.setState({
          displayNbrJoursRestant: responseJson > 0 ? true : false,
        });

        //pour eviter que message apparait sur une creation ou sur une modif

        if (
          window.location.pathname === "/" ||
          window.location.pathname === "/situations"
        ) {
          if (
            responseJson !== 0 &&
            (localStorage.getItem("ignore_rappel") === null ||
              localStorage.getItem("ignore_rappel") === "false")
          ) {
            vue.setState({ displayAvertissementModification: true });
          }
        }
      });
  }

  isAuthoriseModification(vue, idParcelle) {
    axios
      .get(
        URIWSServeur +
          "/ws/parcelle/is-authorise-modification?idParcelle=" +
          idParcelle,
        { cancelToken: vue.state.source.token }
      )
      .then((res) => res.data)
      .then((responseJson) => {
        vue.setState({ isAuthoriseModificaton: responseJson });
      });
  }

  findParcelle(idParcelle, vue, source) {
    axios
      .get(
        URIWSServeur + "/ws/parcelle/find-parcelle?idParcelle=" + idParcelle,
        { cancelToken: source.token }
      )
      .then((res) => {
        return res.data;
      })
      .then((data) => {

        console.log(data)
        vue.setState({ parcelle: data });
        vue.detectPhenologie(data);
        vue.setState({ mapCenter: [data.latitude, data.longitude] });
      })
      .finally(() => {
        vue.setState({ loading: false });
      });
  }

  checkEtatParcelle(idParcelle, vue, source) {
    // check etat de la parcelle
    axios
      .get(URIWSServeur + "/ws/etat/detect-etat?idParcelle=" + idParcelle, {
        cancelToken: source.token,
      })
      .then((response) => response.data)
      .then((data) => {
        vue.changeEtatParcelle(data.conseil.couleur);
      })
      .catch((err) => {
        vue.changeEtatParcelle(null);
      });
  }

  creationParcelle(parcelle, culture, map, vue) {
    axios
      .post(URIWSServeur + "/ws/parcelle/" + culture.toLocaleLowerCase() +"/save-parcelle", parcelle)
      .then((res) => res.data)
      .then((responseJson) => {
        // je déscative cette option pour le moment
        // car la conversion en image prend trop de temps....

        // leafletImage(map, function (err, canvas) {

        //     var img = document.createElement('img');
        //     img.width = 220;
        //     img.height = 100;
        //     //img.src = canvas.toDataURL('image/png', 0.5);
        //     var imgBase64 = canvas.toDataURL('image/png', 0.5);

        //     var entity = {
        //         nom: parcelle.nom,
        //         image: imgBase64,
        //         idEvent: responseJson.idEvent,
        //     }

        //     //alert('post image map')
        //     axios.post(URIWSServeur + '/ws/evenement/add-parcelle-event', entity)
        //         .catch(error => {
        //             console.log(error)
        //         })
        //         .finally((fin) => {

        //         });
        // });

        var entity = {
          nom: parcelle.nom,
          image: "",
          idEvent: responseJson.idEvent,
        };

        //alert('post image map')
        axios
          .post(URIWSServeur + "/ws/evenement/add-parcelle-event", entity)
          .catch((error) => {
            console.log(error);
          })
          .finally((fin) => {});

        // refreshr topbar
        vue.props.callUpdateTopBar();

        //alert('redirect')
        vue.props.history.push({
          pathname: "/situation/" + culture.toLocaleLowerCase(),
          state: {
            idParcelle: responseJson.idParcelle,
            nbrSituations: responseJson.nbrSituations,
            creation: "succes",
          },
        });

        //alert('redirect')
        // vue.props.history.push({
        //     pathname: '/situation',
        //     state: { idParcelle: responseJson.idParcelle ,  creation: 'succes' }
        // })
      })
      .catch((error) => {
        //log.debug(err)

        if (error.response && error.response.data) {
          var errorJson = error.response.data;

          //alert(JSON.stringify(errorJson.message))

          if (errorJson.severity === "error") {
            if (errorJson.fields && errorJson.fields.includes("nom")) {
              vue.setState({ errorNom: true });
            }

            vue.errorsMessages.show({
              severity: "error",
              summary: errorJson.message,
            });
            return false;
          } else {
            vue.errorsMessages.show({
              severity: "error",
              summary: "Erreur interne pendant la création de la situation",
            });
            return false;
          }
        }
      })
      .finally(() => {
        vue.setState({ loadingSave: false });
      });
  }

  async updateParcelle(parcelle,  vue) {
    await axios
      .post(URIWSServeur + "/ws/parcelle/" +  parcelle.culture.code.toLocaleLowerCase() + "/update-parcelle", parcelle)
      .then((data) => {
        vue.setState({ isModification: false });
        vue.setState({ retourModification: "succes" });
        //vue.reloadClasses();
      })
      .catch((error) => {
        //log.debug(err)

        if (error.response && error.response.data) {
          vue.setState({ retourModification: "error" });

          var errorJson = error.response.data;

          //alert(JSON.stringify(errorJson.message))

          if (errorJson.severity === "error") {
            if (errorJson.fields && errorJson.fields.includes("nom")) {
              vue.setState({ errorNom: true });
            }

            vue.errorsMessages.show({
              severity: "error",
              summary: errorJson.message,
            });
            return false;
          } else {
            vue.errorsMessages.show({
              severity: "error",
              summary: "Erreur interne pendant la mise à jour de la situation",
            });
            return false;
          }
        }
      });
  }

  deleteParcelle(parcelle, vue) {
    axios
      .post(URIWSServeur + "/ws/parcelle/" +   parcelle.culture.code.toLocaleLowerCase() +   "/delete-parcelle", parcelle)
      .then((data) => {
        // retour liste des situations + message confirmation !
        vue.props.history.push({
          pathname: "/situations",
          state: { delete: "succes", nomdelete: parcelle.nom },
        });
      })
      .catch((error) => {
        //log.debug(err)

        if (error.response && error.response.data) {
          vue.setState({ retourModification: "error" });

          var errorJson = error.response.data;

          //alert(JSON.stringify(errorJson.message))

          if (errorJson.severity === "error") {
            vue.errorsMessages.show({
              severity: "error",
              summary: errorJson.message,
            });
            return false;
          } else {
            vue.errorsMessages.show({
              severity: "error",
              summary: "Erreur interne pendant la suppression de la situation",
            });
            return false;
          }
        }
      });
  }
  
}
