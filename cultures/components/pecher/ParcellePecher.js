import React, { Component } from "react";


import {
  Map,
  TileLayer,
  Marker,
  Rectangle,
  LayersControl,
} from "react-leaflet";

import L from "leaflet";
import "leaflet/dist/leaflet.css";

import { RadioButton } from "primereact/radiobutton";
import { Chart } from "primereact/chart";
import { OverlayPanel } from "primereact/overlaypanel";

import { EnumTypeSol, EnumPhenologie } from "../../../enums/enums";


import {
  IconError,
  IconConsigneGreen,
  URIWSServeur,
  IconConsigneRed,
  IconConsigneOrange,
  IconMarkerBlue,
  isMobile,
} from "../../../constants/constants";
import { Button } from "primereact/button";
import { ParcelleService } from "../../services/ParcelleService";
import { InputText } from "primereact/inputtext";
import { ModelService } from "../../../services/ModelService";
import axios from "axios";
import { Message } from "primereact/message";
import { Growl } from "primereact/growl";
import { RingLoader } from "react-spinners";
import { HabilitationService } from "../../../services/HabilitationService";

import { ReactLeafletSearch } from "react-leaflet-search";
import { Dialog } from "primereact/dialog";
import { config } from "../../../../config";
import { Fieldset } from "primereact/fieldset";

// Require utile pour zoom sur les charts
require("hammerjs");
require("chartjs-plugin-zoom");

const CancelToken = axios.CancelToken;

export default class ParcellePecher extends Component {
  constructor() {
    super();

    this.state = {
      source: CancelToken.source(),

      loading: true,

      mapCenter: [43.6, 3.8],
      mapZoom: 9,

      isModification: false,
      isAuthoriseModificaton: false,
      etatParcelle: "colorbox-white",
      iconEtatParcelle: "",
      iconMarkerParcelle: IconError,
      lineChartDataResults: {
        datasets: [],
      },
      lineChartDataETP: {
        datasets: [],
      },
      lineChartDataPluie: {
        datasets: [],
      },

      lineChartDataTemp: {
        datasets: [],
      },

      displayResetZoomGraphiqueResults: false,
      displayResetZoomETP: false,
      displayResetZoomTemp: false,

      isSurperpositionClasses: false,
      displayMobile: isMobile(),
    };

    /*  events: [{ id: 1, title: "All Day Event", start: "2020-10-15" }],
    options: {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      defaultView: "dayGridMonth",
      locale: frLocale,
      editable: true,
      dateClick: (e) => {
        alert("ici");
      },
    },*/

    this.loadData = this.loadData.bind(this);
    this.updateParcelle = this.updateParcelle.bind(this);
    this.deleteParcelle = this.deleteParcelle.bind(this);
    this.annuler = this.annuler.bind(this);

    this.resetZoom = this.resetZoom.bind(this);
    this.reloadGraphique = this.reloadGraphique.bind(this);

    this.onModification = this.onModification.bind(this);

    this.displayButtonResetZoomGraphiqueResults = this.displayButtonResetZoomGraphiqueResults.bind(
      this
    );

    this.displayButtonResetZoomETP = this.displayButtonResetZoomETP.bind(this);
    this.displayButtonResetZoomTemp = this.displayButtonResetZoomTemp.bind(
      this
    );

    this.exportResultModel = this.exportResultModel.bind(this);

    this.parcelleService = new ParcelleService();
    this.modelService = new ModelService();
    this.habilitationService = new HabilitationService();

    this.refPluieChart = React.createRef();
    this.refTempChart = React.createRef();

    this.resultsChartOptions = {
      title: {
        //display: true,
        display: false,
        text: "Bilan hydrique",
        fontSize: 16,
      },
      legend: {
        display: true,
        position: "bottom",
        labels: {
          usePointStyle: true,
        },
        onClick: function (evt, legendItem) {
          //alert('legend onClick: event:' + evt+'item :'+ legendItem.text);
          //alert(JSON.stringify(legendItem))


          if (
            legendItem.text !== "Réserve utile" &&
            legendItem.text !== "Irrigation" &&
            legendItem.text !== "Pluviométrie"
          ) {
            alert("Cette légende ne peut pas être désactivé");
            evt.stopPropagation();
          } else {

            this.chart.getDatasetMeta(
              legendItem.datasetIndex
            ).hidden = !this.chart.getDatasetMeta(legendItem.datasetIndex)
              .hidden;

            if (legendItem.datasetIndex == 1 || legendItem.datasetIndex == 2) {
              this.chart.options.scales.yAxes[1].display = !this.chart.options
                .scales.yAxes[1].display;
            } else {
              this.chart.options.scales.yAxes[0].display = !this.chart.options
                .scales.yAxes[0].display;
            }

            this.chart.update();
          }
        },
      },
      hover: {
        mode: "single",
      },
      tooltips: {
        mode: "single",
        callbacks: {},
      },
      scales: {
        xAxes: [
          {
            type: "time",
            time: {
              unit: "month",
              displayFormats: {
                month: "MMM YYYY",
              },
            },
          },
        ],

        yAxes: [
          {
            id: "y-axis-left",
            display: true,
            position: "left",
            scaleLabel: {
              display: true,
              labelString: "mm",
              //fontSize: 10,
              //fontColor: '#03A9F4',
            },
            ticks: {
              //min: 0,
              //max: 0,
              //fontColor: '#03A9F4',
            },
          },
          {
            id: "y-axis-right",
            display: true,
            position: "right",
            scaleLabel: {
              display: true,
              labelString: "mm",
              fontColor: "#0073cc",
              position: "top",
            },
            ticks: {
              min: 0,
              fontColor: "#0073cc",
            },
          },
        ],
      },

      // plugins: {
      //     zoom: {
      //         pan: {
      //             enabled: true,
      //             mode: 'xy'
      //         },
      //         zoom: {
      //             enabled: true,
      //             mode: 'xy'
      //         }
      //     }
      // }

      plugins: {
        filler: {
          propagate: true,
        },

        zoom: {
          // Container for zoom options
          zoom: {
            // Boolean to enable zooming
            enabled: true,

            // Enable drag-to-zoom behavior
            //drag: true,
            drag: {
              borderColor: "black",
              borderWidth: 1,
              backgroundColor: "rgb(225,225,225)",
            },

            // Drag-to-zoom rectangle style can be customized
            // drag: {
            // 	 borderColor: 'rgba(225,225,225,0.3)'
            // 	 borderWidth: 5,
            // 	 backgroundColor: 'rgb(225,225,225)'
            // },

            // Zooming directions. Remove the appropriate direction to disable
            // Eg. 'y' would only allow zooming in the y direction
            mode: "xy",

            rangeMin: {
              // Format of min zoom range depends on scale type
              x: null,
              y: null,
            },
            rangeMax: {
              // Format of max zoom range depends on scale type
              x: null,
              y: null,
            },

            // Speed of zoom via mouse wheel
            // (percentage of zoom on a wheel event)
            speed: 0.1,

            onZoom: this.displayButtonResetZoomGraphiqueResults,
            // Function called once zooming is completed
            // Useful for dynamic data loading
            //onZoom: function({chart}) { console.log(`I was zoomed!!!`); }
          },
        },
      },
    };

    this.etpChartOptions = {
      maintainAspectRatio: false,
      title: {
        //display: true,
        display: false,
        text: "ETP",
        fontSize: 16,
      },
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            type: "time",
            time: {
              unit: "month",
              displayFormats: {
                month: "MMM YYYY",
              },
            },
          },
        ],
      },

      plugins: {
        zoom: {
          // Container for zoom options
          zoom: {
            // Boolean to enable zooming
            enabled: true,

            // Enable drag-to-zoom behavior
            //drag: true,
            drag: {
              borderColor: "black",
              borderWidth: 1,
              backgroundColor: "rgb(225,225,225)",
            },

            // Drag-to-zoom rectangle style can be customized
            // drag: {
            // 	 borderColor: 'rgba(225,225,225,0.3)'
            // 	 borderWidth: 5,
            // 	 backgroundColor: 'rgb(225,225,225)'
            // },

            // Zooming directions. Remove the appropriate direction to disable
            // Eg. 'y' would only allow zooming in the y direction
            mode: "xy",

            rangeMin: {
              // Format of min zoom range depends on scale type
              x: null,
              y: null,
            },
            rangeMax: {
              // Format of max zoom range depends on scale type
              x: null,
              y: null,
            },

            // Speed of zoom via mouse wheel
            // (percentage of zoom on a wheel event)
            speed: 0.1,

            // Function called once zooming is completed
            // Useful for dynamic data loading
            //onZoom: function({chart}) { console.log(`I was zoomed!!!`); }
            onZoom: this.displayButtonResetZoomETP,
          },
        },
      },
    };

    this.tempChartOptions = {
      maintainAspectRatio: false,
      title: {
        //display: true,
        display: false,
        text: "Température",
        fontSize: 16,
      },
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            type: "time",
            time: {
              unit: "month",
              displayFormats: {
                month: "MMM YYYY",
              },
            },
          },
        ],
      },
      plugins: {
        zoom: {
          // Container for zoom options
          zoom: {
            // Boolean to enable zooming
            enabled: true,

            drag: {
              borderColor: "black",
              borderWidth: 1,
              backgroundColor: "rgb(225,225,225)",
            },

            mode: "xy",

            rangeMin: {
              // Format of min zoom range depends on scale type
              x: null,
              y: null,
            },
            rangeMax: {
              // Format of max zoom range depends on scale type
              x: null,
              y: null,
            },

            speed: 0.1,

            onZoom: this.displayButtonResetZoomTemp,
          },
        },
      },
    };
  }

  componentDidMount() {
    if (!(this.props.location.state && this.props.location.state.idParcelle)) {
      this.props.history.push("/situations");
      return;
    }

    window.addEventListener("resize", this.updateDimensions.bind(this));

    if (this.props.location.state.creation === "succes") {
      this.setState({
        visibleDialogCreationSuivante:
          parseInt(this.props.location.state.nbrSituations) <
          parseInt(localStorage.getItem("nbr_situation_global")),
      });
    }

    this.loadParcelle(this.props.location.state.idParcelle);
    this.loadData(this.props.location.state.idParcelle);

    this.loadGraphique(
      this.props.location.state.idParcelle,
      this.state.isSurperpositionClasses
    );

    setTimeout(
      function () {
        //Start the timer
        //alert('delay')

        if (this.props.location.state) {
          this.props.location.state.creation = "";
        }
      }.bind(this),
      5000
    );

    //  var map = this.refs.mapParcelle.leafletElement;
    //  map.invalidateSize(true);
  }

  displayButtonResetZoomGraphiqueResults() {
    this.setState({ displayResetZoomGraphiqueResults: true });
  }

  displayButtonResetZoomETP() {
    this.setState({ displayResetZoomETP: true });
  }

  displayButtonResetZoomTemp() {
    this.setState({ displayResetZoomTemp: true });
  }

  updateDimensions() {
    this.setState({ displayMobile: isMobile() });
  }

  componentWillUnmount() {
    this.state.source.cancel("Operation canceled situation - change page.");
  }

  annuler() {
    if (this.state.currentMarker != null) {
      var map = this.refs.mapParcelle.leafletElement;
      map.removeLayer(this.state.currentMarker);
      map.removeLayer(this.state.currentCircle);
    }

    this.loadParcelle(this.props.location.state.idParcelle);
    this.reloadGraphique();
  }

  onModification() {
    if (this.state.isAuthoriseModificaton) {
      this.setState({ isModification: true });
      this.state.source.cancel("Operation canceled situation - modification.");
      this.setState({ csv: "" });
      this.setState({ source: CancelToken.source() });

      var map = this.refs.mapParcelle.leafletElement;
      var marker = new L.Marker([
        this.state.parcelle.latitude,
        this.state.parcelle.longitude,
      ] , { icon: IconMarkerBlue }).addTo(map);

      //var circle = new L.Circle(e.latlng, { radius: 1000, weight: 1 }).on('click', this.onClickMarker).addTo(map);
      var circle = new L.rectangle(
        L.latLngBounds(
          [
            this.state.parcelle.latitude - 0.005,
            this.state.parcelle.longitude - 0.005,
          ],
          [
            this.state.parcelle.latitude + 0.005,
            this.state.parcelle.longitude + 0.005,
          ]
        )
      ).addTo(map);

      this.setState({ currentMarker: marker });
      this.setState({ currentCircle: circle });

      //this.setState({ currentMarker: null });
      this.setState({ currentMarker: marker });
      this.setState({ iconMarkerParcelle: IconMarkerBlue });
    } else {
      this.setState({ displayDialogNonModification: true });
    }
  }

  clickDisabled() {
    if (!this.state.isAuthoriseModificaton) {
      this.setState({ displayDialogNonModification: true });
    }
  }

  addMarker = (e) => {
    if (this.state.isModification) {
      var map = this.refs.mapParcelle.leafletElement;

      if (this.state.currentMarker != null) {
        map.removeLayer(this.state.currentMarker);
        map.removeLayer(this.state.currentCircle);
      }

      var marker = new L.Marker(e.latlng , { icon: IconMarkerBlue }).addTo(map);

      var circle = new L.rectangle(
        L.latLngBounds(
          [e.latlng.lat - 0.005, e.latlng.lng - 0.005],
          [e.latlng.lat + 0.005, e.latlng.lng + 0.005]
        )
      ).addTo(map);

      this.setState({ currentMarker: marker });
      this.setState({ currentCircle: circle });

      var parcelleMAJ = this.state.parcelle;

      parcelleMAJ.longitude = e.latlng.lng;
      parcelleMAJ.latitude = e.latlng.lat;
      this.setState({ parcelle: parcelleMAJ });

      map.setView(e.latlng);
    }
  };

  loadParcelle(idParcelle) {
    this.parcelleService.findParcelle(idParcelle, this, this.state.source);
    this.parcelleService.isAuthoriseModification(this, idParcelle);
    this.parcelleService.checkEtatParcelle(idParcelle, this, this.state.source);
  }

  changeEtatParcelle(couleur) {
    this.setState({ couleurCircle: couleur });

    if (couleur) {
      this.setState({ etatParcelle: "colorbox-" + couleur });

      if (couleur === "green") {
        this.setState({ iconMarkerParcelle: IconConsigneGreen });
        this.setState({ iconEtatParcelle: "insert_emoticon" });
      } else if (couleur === "yellow") {
        this.setState({ iconMarkerParcelle: IconConsigneOrange });
        this.setState({ iconEtatParcelle: "sentiment_dissatisfied" });
      }
      if (couleur === "red") {
        this.setState({ iconMarkerParcelle: IconConsigneRed });
        this.setState({ iconEtatParcelle: "mood_bad" });
      }
    } else {
      this.setState({ etatParcelle: "colorbox-gray" });
      this.setState({ iconEtatParcelle: "warning" });
      this.setState({ iconMarkerParcelle: IconError });
    }
  }

  detectPhenologie(parcelle) {
    if (parcelle.recolte === "15/08") {
      this.setState({ phenologie: EnumPhenologie.PRECOCE });
    } else if (parcelle.recolte === "10/09") {
      this.setState({ phenologie: EnumPhenologie.MOYENNE });
    } else if (parcelle.recolte === "30/09") {
      this.setState({ phenologie: EnumPhenologie.TARDIVE });
    }
  }

  changePhenologie(phenologie) {
    var parcelle = this.state.parcelle;

    if (phenologie === EnumPhenologie.PRECOCE) {
      parcelle.debourrement = "01/04";
      parcelle.floraison = "22/05";
      parcelle.veraison = "12/07";
      parcelle.recolte = "15/08";
    } else if (phenologie === EnumPhenologie.MOYENNE) {
      parcelle.debourrement = "01/04";
      parcelle.floraison = "01/06";
      parcelle.veraison = "01/08";
      parcelle.recolte = "10/09";
    } else if (phenologie === EnumPhenologie.TARDIVE) {
      parcelle.debourrement = "01/04";
      parcelle.floraison = "08/06";
      parcelle.veraison = "16/08";
      parcelle.recolte = "30/09";
    }

    this.setState({ parcelle: parcelle });
  }

  loadData(idParcelle) {
    // LOAD ETP

    if (
      this.habilitationService.isAuthorize("ADMIN2", "NIVEAU_2", "NIVEAU_3")
    ) {
      axios
        .get(
          URIWSServeur +
            "/ws/meteo/data-by-parcelle-param?idParcelle=" +
            idParcelle +
            "&param=ETP",
          { cancelToken: this.state.source.token }
        )
        .then((response) => response.data)
        .then((data) => {
          var _lineChartData = {
            datasets: [],
          };
          _lineChartData.datasets.unshift({
            type: "line",
            label: "ETP",
            data: data,
            fill: false,
            pointRadius: 0,
            borderColor: "#0073cc", //#03A9F4
          });

          this.setState({ lineChartDataETP: _lineChartData });
          this.refs.etpChartLine.refresh();
        })
        .catch((err) => console.error(this.props.url, err.toString()));

      this.modelService.loadTemp(
        this.props.location.state.idParcelle,
        this,
        this.state.source
      );
    }
  }

  reloadGraphique() {
    this.loadGraphique(
      this.props.location.state.idParcelle,
      this.state.isSurperpositionClasses
    );
  }

  async loadGraphique(idParcelle, isSurperpositionClasses) {
    var _lineChartData = {
      datasets: [],
    };

    _lineChartData.datasets.push({
      type: "line",
      label: "Réserve utile",
      pointStyle: "line",
      data: [],
      fill: false,
      pointRadius: 0,
      borderColor: "#03A9F4", //#03A9F4
    });

    _lineChartData.datasets.push({
      type: "bar",
      label: "Irrigation",
      pointStyle: "rect",
      data: [],
      fill: false,
      pointRadius: 0,
      borderColor: "#03A9F4", //#03A9F4
    });

    if (this.habilitationService.isAuthorize("NIVEAU_2", "NIVEAU_3")) {
      _lineChartData.datasets.push({
        type: "bar",
        label: "Pluviométrie",
        pointStyle: "rect",
        data: [],
        fill: false,
        pointRadius: 0,
        backgroundColor: "#0073cc",
        yAxisID: "y-axis-right",
      });
    }

    this.setState({ lineChartDataResults: _lineChartData });

    this.loadContrainteHydrique(idParcelle);
  }

  loadContrainteHydrique(idParcelle) {
    // load Contrainte hydrique

    if (this.habilitationService.isAuthorize("NIVEAU_2", "NIVEAU_3")) {
      axios
        .get(
          URIWSServeur +
            "/ws/meteo/data-by-parcelle-param?idParcelle=" +
            idParcelle +
            "&param=PLUIE",
          { cancelToken: this.state.source.token }
        )
        .then((response) => response.data)
        .then((data) => {
          // alert(JSON.stringify(data))
          var _lineChartData = { ...this.state.lineChartDataResults };
          _lineChartData.datasets[2] = {
            type: "bar",
            label: "Pluviométrie",
            pointStyle: "rect",
            data: data,
            fill: false,
            pointRadius: 0,
            backgroundColor: "#0073cc",
            yAxisID: "y-axis-right",
          };

          if (this.refs.graphiqueResultLine) {
            this.refs.graphiqueResultLine.refresh();
          }
        })
        .catch((err) => console.error(err.toString()));
    }

    // // Model bilan hydrique by
    axios
      .get(
        URIWSServeur +
          "/ws/model/resultat-bilan-hydrique?idParcelle=" +
          idParcelle +
          "&param=ATSW",
        { cancelToken: this.state.source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        var _lineChartData = { ...this.state.lineChartDataResults };
        _lineChartData.datasets[0] = {
          type: "line",
          label: "Réserve utile",
          pointStyle: "line",
          data: data,
          fill: false,
          pointRadius: 0,
          borderColor: "#03A9F4",
          yAxisID: "y-axis-left",
        };

        if (this.refs.graphiqueResultLine) {
          this.refs.graphiqueResultLine.refresh();
        }
      });

    axios
      .get(
        URIWSServeur +
          "/ws/model/resultat-bilan-hydrique?idParcelle=" +
          idParcelle +
          "&param=DOSE_IRRIGATION_OPTIMISE",
        { cancelToken: this.state.source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        var _lineChartData = { ...this.state.lineChartDataResults };
        _lineChartData.datasets[1] = {
          type: "bar",
          label: "Irrigation",
          pointStyle: "rect",
          data: data,
          fill: false,
          pointRadius: 0,
          backgroundColor: "#4CAF50",
      //    borderColor: "#4CAF50",
          yAxisID: "y-axis-left",
        };

        if (this.refs.graphiqueResultLine) {
          this.refs.graphiqueResultLine.refresh();
        }
      });
  }

  async updateParcelle() {
    var errorsMessagesTexte = [];

    this.setState({ errorNom: false });

    if (!this.state.parcelle.nom) {
      this.setState({ errorNom: true });
      errorsMessagesTexte.push("Nom");
    }

    if (this.state.parcelle.nom.length > 120) {
      this.setState({ errorNom: true });
      errorsMessagesTexte.push("Nom (120 caractères)");
    }

    if (errorsMessagesTexte.length > 0) {
      this.errorsMessages.show({
        severity: "error",
        summary: "Champs non renseignés : ",
        detail: errorsMessagesTexte.join(", "),
      });
    } else {
      // call ws pour save parcelle
      await this.parcelleService.updateParcelle(this.state.parcelle, this);

      if (this.state.currentMarker != null) {
        var map = this.refs.mapParcelle.leafletElement;
        map.removeLayer(this.state.currentMarker);
        map.removeLayer(this.state.currentCircle);
      }

      this.reloadGraphique();
      this.loadData(this.state.parcelle.id);
      this.loadParcelle(this.state.parcelle.id);

      setTimeout(
        function () {
          //Start the timer

          if (this.state.retourModification) {
            this.setState({ retourModification: "" });
          }
        }.bind(this),
        5000
      );
    }
  }

  deleteParcelle() {
    this.parcelleService.deleteParcelle(this.state.parcelle, this);
    // refreshr topbar
    this.props.callUpdateTopBar();
  }

  resetZoom(ref, title) {
    // permet de voir le detail de l'object
    //console.log('Object: ', ref);

    var chart = ref.chart;
    chart.resetZoom();

    if (title === "graphique_results") {
      this.setState({ displayResetZoomGraphiqueResults: false });
    } else if (title === "etp") {
      this.setState({ displayResetZoomETP: false });
    } else if (title === "temperature") {
      this.setState({ displayResetZoomTemp: false });
    }
  }

  exportResultModel() {
    this.setState({ loadingExport: true });

    this.modelService.exportResultModel(
      this.props.location.state.idParcelle,
      2020,
      this,
      this.state.source
    );
  }

  render() {
    const footerDelete = (
      <div>
        <Button
          label="Annuler"
          icon="pi pi-times"
          onClick={(e) => this.setState({ visibleConfirmationDelete: false })}
        />

        <Button
          label="Confirmer"
          icon="pi pi-check"
          onClick={(e) => this.deleteParcelle()}
        />
      </div>
    );

    const footerSuivant = (
      <div>
        <Button
          label="Fermer"
          icon="pi pi-times"
          onClick={(e) =>
            this.setState({ visibleDialogCreationSuivante: false })
          }
        />

        <Button
          label="Nouvelle situation"
          icon="pi pi-plus"
          onClick={(e) => {
            this.props.history.push({
              pathname: "/situation/pecher/création",
              state: { idParcelle: null },
            });
          }}
        />
      </div>
    );

    let loading = (
      <div className="loader">
        <div style={{ width: "50px", margin: "0 auto" }}>
          <RingLoader
            margin={150}
            sizeUnit={"px"}
            size={50}
            color={"#123abc"}
            loading={true}
            style={{ margin: "0 auto" }}
          />
        </div>

        <div style={{ fontSize: "16px", margin: "0 auto", color: "#3F51B5" }}>
          Chargement de la situation
        </div>
      </div>
    );

    if (this.state.loading) {
      return <div className="p-grid dashboard"> {loading} </div>;
    }

    if (this.state.loadingExport) {
      return (
        <div className="loader">
          <div style={{ width: "50px", margin: "0 auto" }}>
            <RingLoader
              margin={150}
              sizeUnit={"px"}
              size={50}
              color={"#123abc"}
              loading={true}
              style={{ margin: "0 auto" }}
            />
          </div>

          <div style={{ fontSize: "16px", margin: "0 auto", color: "#3F51B5" }}>
            Export en cours
          </div>
        </div>
      );
    }

    return (
      <div>
        {/* <script src="https://cdn.jsdelivr.net/npm/hammerjs@2.0.8"></script>       <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.0"></script> */}

        <Dialog
          header="Modification désactivée"
          style={{ width: "30vw" }}
          visible={this.state.displayDialogNonModification}
          modal={true}
          dismissableMask={true}
          onHide={(e) => {
            this.setState({ displayDialogNonModification: false });
          }}
        >
          <div className="p-grid">
            <div className="p-col-12">Contactez-nous</div>
            <div
              className="p-col-12"
              style={{ textAlign: "center", paddingTop: "1.5rem" }}
            >
              <Button
                id="button_abonner"
                className="pink-btn "
                label="Contactez-nous"
                icon="pi-md-email"
                onClick={() => {
                  window.location.href = "/nous-contacter";
                }}
                style={{ marginBottom: "0px" }}
              />
            </div>
          </div>
        </Dialog>

        <OverlayPanel
          ref={(el) => (this.helpTypeSol = el)}
          style={{ width: "280px" }}
        >
          <p>
            Il s’agit de caractériser la réserve utile de votre sol, qui résulte
            de sa profondeur et de sa texture.
          </p>
          <p>
            - Faible réserve en eau : il peut s’agir de sols à texture grossière
            (sableuse, et/ou caillouteuse), ou de sols à texture plus fine, mais
            de faible profondeur (par exemple moins d’un mètre de terre sur la
            roche mère).
          </p>
          <p>
            - Réserve moyenne en eau : sols à texture moyenne, plutôt limoneuse,
            ou sol moyennement profond (entre 1 et 2 m de terre sur la roche
            mère).
          </p>
          <p>
            - Réserve en eau élevée : sols à texture fine, plutôt argileuse, ou
            a texture plus grossière mais très profonds (exemple sols de
            Costières).
          </p>
        </OverlayPanel>

        <OverlayPanel
          ref={(el) => (this.helpPrecocite = el)}
          style={{ width: "280px" }}
        >
          <p>
            Les gammes de précocité sont établies sur les dates moyennes de
            stades phénologiques suivantes :
          </p>

          <table>
            <thead>
              <tr>
                <th></th>
                <th>Précoce</th>
                <th>Moyen</th>
                <th>Tardif</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>Débourrement</td>
                <td>01/04</td>
                <td>01/04</td>
                <td>01/04</td>
              </tr>

              <tr>
                <td>Floraison</td>
                <td>22/05</td>
                <td>01/06</td>
                <td>08/06</td>
              </tr>
              <tr>
                <td>Véraison</td>
                <td>12/07</td>
                <td>01/08</td>
                <td>16/08</td>
              </tr>

              <tr>
                <td>Vendange</td>
                <td>15/08</td>
                <td>10/09</td>
                <td>30/09</td>
              </tr>
            </tbody>
          </table>
        </OverlayPanel>

        {this.props.location.state.creation === "succes" && (
          <div>
            <Message
              style={{ width: "100%", textAlign: "center", fontSize: "15px" }}
              severity="info"
              text="Création effectuée"
            ></Message>{" "}
            <br />
            <Dialog
              header="Félicitations !"
              footer={footerSuivant}
              visible={this.state.visibleDialogCreationSuivante}
              modal={true}
              onHide={() =>
                this.setState({ visibleDialogCreationSuivante: false })
              }
            >
              <div className="p-grid">
                <div className="p-col-12">
                  Vous avez créé votre situation, voulez-vous en créer une autre
                  ?
                  <br />
                </div>
              </div>
            </Dialog>
          </div>
        )}
        {this.state.retourModification === "succes" && (
          <div>
            <Message
              style={{ width: "100%", textAlign: "center", fontSize: "15px" }}
              severity="info"
              text="Modification effectuée"
            ></Message>{" "}
            <br />
          </div>
        )}

        <Growl ref={(el) => (this.errorsMessages = el)} />

        <div className="p-grid">
          <div className="p-col-12">
            <div className="p-grid dashboard">
              <div className="p-col-12 p-md-6 p-lg-6">
                <div
                  className="p-col-12 p-md-12 p-lg-12"
                  style={{ marginBottom: "10px" }}
                >
                  <div
                    className={
                      "p-grid card colorbox " + this.state.etatParcelle
                    }
                  >
                    <div className="p-col-4">
                      <i className="material-icons">
                        {this.state.iconEtatParcelle}
                      </i>

                      {/*sentiment-satisfied
                                    sentiment-neutral
                                    sentiment-dissatisfied
                                    check_circle  warning error */}
                    </div>
                    <div className="p-col-8">
                      {!this.state.isModification && (
                        <div
                          onClick={(e) => {
                            this.clickDisabled();
                          }}
                        >
                          <Button
                            disabled={!this.state.isAuthoriseModificaton}
                            className={
                              !this.state.isAuthoriseModificaton
                                ? " deep-orange-btn  p-disabled"
                                : "deep-orange-btn "
                            }
                            icon="pi-md-edit"
                            style={{ float: "right" }}
                            onClick={(e) => {
                              this.onModification();
                            }}
                          />

                          {this.habilitationService.isAuthorize("ADMIN") && (
                            <Button
                              icon="pi-md-delete"
                              className="p-button blue-grey-btn"
                              style={{ float: "right", marginRight: "8px" }}
                              onClick={(e) =>
                                this.setState({
                                  visibleConfirmationDelete: true,
                                })
                              }
                            />
                          )}
                        </div>
                      )}

                      {this.state.isModification && (
                        <div
                          style={{ float: "right", background: "transparent" }}
                        >
                          <Button
                            icon="pi pi-times"
                            className="p-button-secondary"
                            onClick={(e) => {
                              this.annuler();
                              this.setState({ isModification: false });
                            }}
                          />
                          <Button
                            icon="pi-md-check"
                            className="p-button-success"
                            style={{ marginLeft: "5px" }}
                            onClick={(e) => {
                              this.updateParcelle();
                            }}
                          />
                        </div>
                      )}

                      {!this.state.isModification && (
                        <span
                          className="colorbox-count"
                          style={{ lineHeight: "60px" }}
                        >
                          {this.state.parcelle.nom}
                        </span>
                      )}
                      {this.state.isModification && (
                        <span className="colorbox-count">
                          <InputText
                            className={this.state.errorNom ? "p-error" : ""}
                            style={{ width: "100%" }}
                            value={this.state.parcelle.nom}
                            onChange={(e) => {
                              var parc = this.state.parcelle;
                              parc.nom = e.target.value;
                              this.setState({ parcelle: parc });
                            }}
                          />{" "}
                        </span>
                      )}
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-col-12 p-md-6 p-lg-6">
                <div className="p-col-12 p-md-12 p-lg-12 ">
                  <Map
                    ref="mapParcelle"
                    center={this.state.mapCenter}
                    zoom={this.state.mapZoom}
                    onClick={this.addMarker}
                    style={{ height: "350px" }}
                  >
                    <TileLayer
                      attribution='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                      url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
                    />

                    <LayersControl position="topright">
                      <ReactLeafletSearch
                        showMarker={false}
                        position="topright"
                        inputPlaceholder="recherche"
                        popUp={this.popupSearchMarker}
                        providerOptions={{ region: "fr" }}
                        zoom={14}
                        closeResultsOnClick={true}
                      />

                      <LayersControl.BaseLayer name="OpenStreet Map" checked>
                        <TileLayer
                          attribution='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                          url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
                        />
                      </LayersControl.BaseLayer>
                      <LayersControl.BaseLayer name="ArcGIS World Topo Map">
                        <TileLayer
                          attribution="Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community"
                          url="http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}"
                        />
                      </LayersControl.BaseLayer>

                      <LayersControl.BaseLayer name="IGN photos aériennes">
                        <TileLayer
                          attribution="IGN-F/Géoportail"
                          url="https://wxs.ign.fr/pratique/geoportail/wmts?&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}"
                        />
                      </LayersControl.BaseLayer>
                    </LayersControl>

                    {!this.state.isModification && (
                      <Marker
                        position={[
                          this.state.parcelle.latitude,
                          this.state.parcelle.longitude,
                        ]}
                        icon={this.state.iconMarkerParcelle}
                      ></Marker>
                    )}
                    {!this.state.isModification && (
                      <Rectangle
                        bounds={L.latLngBounds(
                          [
                            this.state.parcelle.latitude - 0.005,
                            this.state.parcelle.longitude - 0.005,
                          ],
                          [
                            this.state.parcelle.latitude + 0.005,
                            this.state.parcelle.longitude + 0.005,
                          ]
                        )}
                        color={this.state.couleurCircle}
                      ></Rectangle>
                    )}

                    {this.state.isModification && (
                      <Marker
                        position={[
                          this.state.parcelle.latitude,
                          this.state.parcelle.longitude,
                        ]}
                        icon={this.state.iconMarkerParcelle}
                      ></Marker>
                    )}
                  </Map>
                </div>
              </div>
            </div>

            <div className="p-grid dashboard">
              <div className="p-col-12 p-md-4">
                <div className="card timeline p-fluid">
                  <h1>
                    Précocité{" "}
                    <img
                      style={{ width: "18px", height: "18px", float: "right" }}
                      alt="help"
                      src={"/assets/icons/iconHelp.png"}
                      onClick={(e) => this.helpPrecocite.toggle(e)}
                    />
                  </h1>

                  <div className="p-grid">
                    <div className="p-col">
                      <RadioButton
                        inputId="phenlogie_1"
                        disabled={!this.state.isModification}
                        value={EnumPhenologie.PRECOCE}
                        name="phenlogie"
                        onChange={(e) => {
                          this.setState({ phenologie: e.value });
                          this.changePhenologie(e.value);
                        }}
                        checked={
                          this.state.phenologie === EnumPhenologie.PRECOCE
                        }
                      />
                      <label
                        htmlFor="phenlogie_1"
                        className="p-radiobutton-label"
                      >
                        {EnumPhenologie.PRECOCE.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="phenlogie_2"
                        disabled={!this.state.isModification}
                        value={EnumPhenologie.MOYENNE}
                        name="phenlogie"
                        onChange={(e) => {
                          this.setState({ phenologie: e.value });
                          this.changePhenologie(e.value);
                        }}
                        checked={
                          this.state.phenologie === EnumPhenologie.MOYENNE
                        }
                      />
                      <label
                        htmlFor="phenlogie_2"
                        className="p-radiobutton-label"
                      >
                        {EnumPhenologie.MOYENNE.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="phenlogie_3"
                        disabled={!this.state.isModification}
                        value={EnumPhenologie.TARDIVE}
                        name="phenlogie"
                        onChange={(e) => {
                          this.setState({ phenologie: e.value });
                          this.changePhenologie(e.value);
                        }}
                        checked={
                          this.state.phenologie === EnumPhenologie.TARDIVE
                        }
                      />
                      <label
                        htmlFor="phenlogie_3"
                        className="p-radiobutton-label"
                      >
                        {EnumPhenologie.TARDIVE.label}
                      </label>
                    </div>
                  </div>
                  <br />

                  <div className="p-grid">
                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#009688" }}>
                        {this.state.parcelle.debourrement}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#009688" }}
                      >
                        filter_1
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#009688" }}
                      >
                        Débourrement
                      </span>
                      <span className="event-text"></span>
                      <div className="event-content"></div>
                    </div>

                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#E91E63" }}>
                        {this.state.parcelle.floraison}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#E91E63" }}
                      >
                        filter_2
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#E91E63" }}
                      >
                        Floraison
                      </span>
                      <span className="event-text"></span>
                    </div>

                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#9c27b0" }}>
                        {this.state.parcelle.veraison}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#9c27b0" }}
                      >
                        filter_3
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#9c27b0" }}
                      >
                        Véraison
                      </span>
                      <span className="event-text"></span>
                    </div>

                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#ff9800" }}>
                        {this.state.parcelle.recolte}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#ff9800" }}
                      >
                        filter_4
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#ff9800" }}
                      >
                        Récolte
                      </span>
                      <span className="event-text"></span>
                      <div className="event-content">
                        <img
                          src="/assets/layout/images/recolte_raisin.jpg"
                          alt="recolte"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {!this.state.isModification && (
                <div className="p-col-12 p-md-8 p-lg-8">
                  <div className="card">
                    <img
                      src="/assets/logos/logo_IFV.png"
                      width="45"
                      alt="avatar6"
                      className="logo_ifv_graphe"
                    />
                    <h1 className="centerText">
                      Indicateur de contrainte hydrique{" "}
                    </h1>

                    {this.state.displayResetZoomGraphiqueResults && (
                      <Button
                        icon="pi pi-times"
                        className="p-button-secondary"
                        label="Reset Zoom"
                        onClick={(e) => {
                          this.resetZoom(
                            this.refs.graphiqueResultLine,
                            "graphique_results"
                          );
                        }}
                        style={{
                          right: "70px",
                          position: "absolute",
                          zIndex: "99",
                          fontSize: "10px",
                          marginTop: "10px",
                        }}
                      />
                    )}

                    {/* is mobile faire sa  sinon */}
                    {!this.state.displayMobile && (
                      <Chart
                        id="graphiqueResultLine"
                        ref="graphiqueResultLine"
                        type="bar"
                        data={this.state.lineChartDataResults}
                        options={this.resultsChartOptions}
                      />
                    )}
                    {this.state.displayMobile && (
                      <Chart
                        height="300px"
                        id="graphiqueResultLine"
                        ref="graphiqueResultLine"
                        type="bar"
                        data={this.state.lineChartDataResults}
                        options={this.resultsChartOptions}
                      />
                    )}

                    <br />
                    <Fieldset legend="Information règlementation">
                      Cette application est un outil de conseil à l’irrigation.
                      Les consignes qu’elle délivre sont de nature agronomique.
                      L’utilisateur est invité à s’informer de la règlementation
                      applicable à sa situation.{" "}
                    </Fieldset>
                    <br />
                  </div>
                </div>
              )}
            </div>
          </div>

          {!this.state.isModification && (
            <div className="p-col-12 p-md-6 p-lg-6">
              <div className="card">
                {!this.habilitationService.isAuthorize(
                  "ADMIN2",
                  "NIVEAU_2",
                  "NIVEAU_3"
                ) && (
                  <div>
                    <h1 className="centerText">
                      ETP <span style={{ fontSize: "11px" }}>(mm)</span>{" "}
                    </h1>
                    <div
                      style={{
                        margin: "auto",
                        textAlign: "center",
                        width: "250px",
                        marginBottom: "25px",
                      }}
                    >
                      Votre abonnement ne vous permet pas l'accès à cette donnée
                    </div>
                    <div style={{ margin: "auto", textAlign: "center" }}>
                      <Button
                        id="button_abonner"
                        className="pink-btn "
                        label="Abonnez vous"
                        icon="pi-md-add-shopping-cart"
                        onClick={() => {
                          window.location.href = config.URI_ABONNEZ_VOUS;
                        }}
                        style={{ marginBottom: "0px" }}
                      />
                    </div>
                  </div>
                )}

                {this.habilitationService.isAuthorize(
                  "NIVEAU_2",
                  "NIVEAU_3"
                ) && (
                  <div>
                    <h1 className="centerText">
                      ETP <span style={{ fontSize: "11px" }}>(mm)</span>{" "}
                      {this.state.displayResetZoomETP && (
                        <Button
                          icon="pi pi-times"
                          className="p-button-secondary"
                          label="Reset Zoom"
                          onClick={(e) => {
                            this.resetZoom(this.refs.etpChartLine, "etp");
                          }}
                          style={{
                            float: "right",
                            zIndex: "99",
                            fontSize: "10px",
                            marginTop: "10px",
                          }}
                        />
                      )}
                    </h1>

                    <Chart
                      id="etpChartLine"
                      ref="etpChartLine"
                      type="line"
                      data={this.state.lineChartDataETP}
                      options={this.etpChartOptions}
                    />
                  </div>
                )}
              </div>
            </div>
          )}

          {!this.state.isModification && (
            <div className="p-col-12 p-md-6 p-lg-6">
              <div className="card">
                {!this.habilitationService.isAuthorize(
                  "NIVEAU_2",
                  "NIVEAU_3"
                ) && (
                  <div>
                    <h1 className="centerText">
                      Température <span style={{ fontSize: "11px" }}>(°C)</span>{" "}
                    </h1>

                    <div
                      style={{
                        margin: "auto",
                        textAlign: "center",
                        width: "250px",
                        marginBottom: "25px",
                      }}
                    >
                      Votre abonnement ne vous permet pas l'accès à cette donnée
                    </div>
                    <div style={{ margin: "auto", textAlign: "center" }}>
                      <Button
                        id="button_abonner"
                        className="pink-btn "
                        label="Abonnez vous"
                        icon="pi-md-add-shopping-cart"
                        onClick={() => {
                          window.location.href = config.URI_ABONNEZ_VOUS;
                        }}
                        style={{ marginBottom: "0px" }}
                      />
                    </div>
                  </div>
                )}

                {this.habilitationService.isAuthorize(
                  "ADMIN2",
                  "NIVEAU_2",
                  "NIVEAU_3"
                ) && (
                  <div>
                    <h1 className="centerText">
                      Température <span style={{ fontSize: "11px" }}>(°C)</span>{" "}
                      {this.state.displayResetZoomTemp && (
                        <Button
                          icon="pi pi-times"
                          className="p-button-secondary"
                          label="Reset Zoom"
                          onClick={(e) => {
                            this.resetZoom(
                              this.refTempChart.current,
                              "temperature"
                            );
                          }}
                          style={{
                            float: "right",
                            zIndex: "99",
                            fontSize: "10px",
                            marginTop: "10px",
                          }}
                        />
                      )}
                    </h1>

                    <Chart
                      id="pluieChartLine"
                      ref={this.refTempChart}
                      type="bar"
                      data={this.state.lineChartDataTemp}
                      options={this.tempChartOptions}
                    />
                  </div>
                )}
              </div>
            </div>
          )}

          {!this.state.isModification &&
            this.habilitationService.isAuthorize("ROLE_RESULT_CSV_VIEWER") && (
              <div className="p-col-12 p-md-12 p-lg-12">
                <div className="card">
                  <h1 className="centerText">
                    CSV <span style={{ fontSize: "11px" }}></span>
                  </h1>
                  {this.state.csv}
                </div>
              </div>
            )}

          {!this.state.isModification &&
            this.habilitationService.isAuthorize("EXPORT_RESULT_EXCEL") && (
              <div className="p-col-12 p-md-6 p-lg-6">
                <div className="card">
                  <div>
                    <h1 className="centerText">Export</h1>

                    <div style={{ margin: "auto", textAlign: "center" }}>
                      <Button
                        id="button_abonner"
                        className="pink-btn "
                        label="Export résultats"
                        icon="pi-md-file-download"
                        onClick={() => {
                          this.exportResultModel();
                        }}
                        style={{ marginBottom: "0px" }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            )}

          {/*
          {!this.state.isModification &&
            this.habilitationService.isAuthorize("EXPORT_RESULT_EXCEL") && (
              <div className="p-col-12 p-md-6 p-lg-6">
                <div className="card">
                  <div style={{ margin: "auto", textAlign: "center" }}>
                    <FullCalendar
                      events={this.state.events}
                      options={this.state.options}
                    />
                  </div>
                </div>
              </div>
            )}
            */}
        </div>

        <Dialog
          header="Confirmation de suppression !"
          footer={footerDelete}
          visible={this.state.visibleConfirmationDelete}
          style={{ width: "50vw" }}
          modal={true}
          onHide={() => this.setState({ visibleConfirmationDelete: false })}
        >
          Confirmez la suppression de la situation :{" "}
          {this.state.parcelle ? this.state.parcelle.nom : ""}
        </Dialog>
      </div>
    );
  }
}
