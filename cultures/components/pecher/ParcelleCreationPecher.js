import React, { Component } from "react";
import { Map, TileLayer, Marker, Popup, LayersControl } from "react-leaflet";

import L from "leaflet";
import "leaflet/dist/leaflet.css";

import { Fieldset } from "primereact/fieldset";
import { RadioButton } from "primereact/radiobutton";
import { OverlayPanel } from "primereact/overlaypanel";

import {
  EnumTypeVin,
  EnumTypeSol,
  EnumPhenologie,
  EnumCulture,
  EnumMaterielIrrigation,
} from "../../../enums/enums";

import { InputText } from "primereact/inputtext";
import { URIWSServeur, IconMarkerBlue } from "../../../constants/constants"
import { Button } from "primereact/button";

import { ReactLeafletSearch } from "react-leaflet-search";

import { ParcelleService } from "../../services/ParcelleService";
import axios from "axios";
import { EvenementService } from "../../../services/EvenementService";
import { Growl } from "primereact/growl";
import { RingLoader } from "react-spinners";
import { Spinner } from "primereact/spinner";
import { HabilitationService } from "../../../services/HabilitationService";
import { Calendar } from "primereact/calendar";

export default class ParcelleCreationPecher extends Component {
  constructor() {
    super();

    this.state = {
      errorNom: false,
      errorLocalisation: false,
      mapCenter: [43.6, 3.8],
      mapZoom: 9,

      loading: true,
      loadingSave: false,

      parcelle: {
        nom: "",
        use_custom_ru: true,
      },
    };

    this.saveParcelle = this.saveParcelle.bind(this);

    this.parcelleService = new ParcelleService();
    this.evenementService = new EvenementService();
    this.habilitationService = new HabilitationService();
  }

  componentDidMount() {

    if (      
        this.props.location.state &&
        this.props.location.state.culture !== EnumCulture.PECHER.code.toLocaleLowerCase()
          ) {

        this.props.history.push("/situations");
      return;
    }

    axios
      .get(
        URIWSServeur +
          "/ws/parcelle/find-parcelle?idParcelle=-1&culture=" +
          EnumCulture.PECHER.code
      )
      .then((response) => {
        this.setState({ parcelle: response.data });
      })
      .finally((f) => {
        this.setState({ loading: false });
      });
  }

  changePhenologie(phenologie) {
    var parcelle = this.state.parcelle;

    if (phenologie === EnumPhenologie.PRECOCE) {
      parcelle.debutRecolte = "01/07";
    } else if (phenologie === EnumPhenologie.MOYENNE) {
      parcelle.debutRecolte = "20/07";
    } else if (phenologie === EnumPhenologie.TARDIVE) {
      parcelle.debutRecolte = "20/05";
    }
  }

  addMarker = (e) => {
    var map = this.refs.mapParcelleCreation.leafletElement;

    if (this.state.currentMarker != null) {
      map.removeLayer(this.state.currentMarker);
      map.removeLayer(this.state.currentCircle);
    }

    var marker = new L.Marker(e.latlng, { icon: IconMarkerBlue }).addTo(map);

    //var circle = new L.Circle(e.latlng, { radius: 1000, weight: 1 }).on('click', this.onClickMarker).addTo(map);
    var circle = new L.rectangle(
      L.latLngBounds(
        [e.latlng.lat - 0.005, e.latlng.lng - 0.005],
        [e.latlng.lat + 0.005, e.latlng.lng + 0.005]
      )
    ).addTo(map);

    this.setState({ currentMarker: marker });
    this.setState({ currentCircle: circle });

    // this.setState({mapCenter: e.latlng})
    //map.fitBounds(marker.toBounds())
    map.setView(e.latlng);

    var parcelleMAJ = this.state.parcelle;
    parcelleMAJ.longitude = e.latlng.lng;
    parcelleMAJ.latitude = e.latlng.lat;
    this.setState({ parcelle: parcelleMAJ });
  };

  async saveParcelle() {
    var parcelle = this.state.parcelle;
    var errorsMessagesTexte = [];

    this.setState({ errorNom: false });    
    this.setState({ errorTypeSol: false });
    this.setState({ errorMaterielIrrigation: false });        
    this.setState({ errorPhenologie: false });
    this.setState({ errorLocalisation: false });



    if (!parcelle.nom) {
      this.setState({ errorNom: true });
      errorsMessagesTexte.push("Nom");
    }

    // fair etest si custom et pas de RU select ?

    if (!parcelle.typeSol) {
      this.setState({ errorTypeSol: true });
      errorsMessagesTexte.push("Réserve du sol");
    }

    if (
      parcelle.typeSol &&
      parcelle.typeSol.code === EnumTypeSol.CUSTOM.code &&
      !parcelle.reserveUtileCustom
    ) {
      this.setState({ errorTypeSol: true });
      errorsMessagesTexte.push(
        "La quantitée de la réserve utilie est obligatoire si parmétrable..."
      );
    }

    if (!parcelle.materielIrrigation) {
      this.setState({ errorMaterielIrrigation: true });
      errorsMessagesTexte.push("Matériel d'irrigation");
    }

    if (!parcelle.debutCycle) {
      this.setState({ errorPhenologie: true });
      errorsMessagesTexte.push("Date de début du cycle");
    }

    if (!parcelle.debutRecolte) {
      this.setState({ errorPhenologie: true });
      errorsMessagesTexte.push("Cycle de la culture");
    }

    if (!parcelle.longitude) {
      this.setState({ errorLocalisation: true });
      errorsMessagesTexte.push("Localisation");
    }

    if (errorsMessagesTexte.length > 0) {
      this.errorsMessages.show({
        severity: "error",
        summary: "Champs non renseignés : ",
        detail: errorsMessagesTexte.join(", "),
      });
    } else {
      this.setState({ loadingSave: true });

      var map = this.refs.mapParcelleCreation.leafletElement;
      map.setView([parcelle.latitude, parcelle.longitude], 13);

      // call ws pour save parcelle

      await this.parcelleService.creationParcelle(
        parcelle,
        this.props.location.state.culture,
        map,
        this
      );
    }
  }

  popupSearchMarker(SearchInfo) {
    return (
      <Popup>
        <div>
          <p>{SearchInfo.info}</p>
        </div>
      </Popup>
    );
  }

  render() {
    let fr = {
      firstDayOfWeek: 1,
      dayNames: [
        "dimanche",
        "lundi",
        "mardi",
        "mercredi",
        "jeudi",
        "vendredi",
        "samedi",
      ],
      dayNamesShort: ["dim", "lun", "mar", "mer", "jeu", "ven", "dam"],
      dayNamesMin: ["DI", "LU", "MA", "ME", "JE", "VE", "SA"],
      monthNames: [
        "janvier",
        "février",
        "mars",
        "avril",
        "mai",
        "juin",
        "juillet",
        "août",
        "septembre",
        "octobre",
        "novembre",
        "décembre",
      ],
      monthNamesShort: [
        "jan",
        "fev",
        "mar",
        "avr",
        "mai",
        "juin",
        "juil",
        "août",
        "sept",
        "oct",
        "nov",
        "dec",
      ],
    };

    let loading = (
      <div className="loader">
        <div style={{ width: "50px", margin: "0 auto" }}>
          <RingLoader
            margin={150}
            sizeUnit={"px"}
            size={50}
            color={"#123abc"}
            loading={true}
            style={{ margin: "0 auto" }}
          />
        </div>

        <div style={{ fontSize: "16px", margin: "0 auto", color: "#3F51B5" }}>
          Initialisation de la situation
        </div>
      </div>
    );

    //const wrappedZoomIndicator = withLeaflet(ReactLeafletZoomIndicator);

    if (this.state.loading) {
      return <div className="p-grid dashboard"> {loading}</div>;
    }

    return (
      <div>
        <div style={{ width: "180px" }}></div>

        {/* <Button
          label=""
          className="start_tutorial"
          icon="pi pi-info-circle"
          onClick={(e) => this.beginTutorial()}
          style={{
            display: this.state.aide_tutorial_visible ? "none" : "initial",
          }}
        /> */}

        <div className="loader">
          <div style={{ width: "50px", margin: "0 auto" }}>
            <RingLoader
              margin={150}
              sizeUnit={"px"}
              size={50}
              color={"#123abc"}
              loading={this.state.loadingSave}
              style={{ margin: "0 auto" }}
            />
          </div>

          {this.state.loadingSave && (
            <div
              style={{ fontSize: "16px", margin: "0 auto", color: "#3F51B5" }}
            >
              <br />
              Sauvegarde en cours de la situation
            </div>
          )}
        </div>

        <OverlayPanel
          ref={(el) => (this.helpTypeVin = el)}
          style={{ width: "280px" }}
        >
          <p>
            - Vin Blanc ou Rosé à rendement naturel : il peut s’agir par exemple
            de Vins de Pays, de Cépage ou de Table
          </p>
          <p>
            - Vin Blanc ou Rosé à rendement maîtrisé : concerne les vins Blanc
            ou Rosé en AOP.
          </p>
          <p>
            - Vin Rouge à rendement naturel : il peut s’agir par exemple de Vins
            de Pays, de Cépage ou de Table.
          </p>
          <p>
            - Vin Rouge à rendement maîtrisé : concerne les vins Rouges en AOP.
          </p>
        </OverlayPanel>

        <OverlayPanel
          ref={(el) => (this.helpTypeSol = el)}
          style={{ width: "280px" }}
        >
          <p>
            Il s’agit de caractériser la réserve utile de votre sol, qui est la
            résultante de sa profondeur, sa texture et sa charge en éléments
            grossiers.
          </p>
          <p>
            - Faible réserve en eau ({"<"} 125 mm) : sol peu profond, chargé en
            cailloux et/ou avec texture grossière. La contrainte hydrique est
            généralement forte (défoliation marquée et rendement faible). Elle
            est présente quasiment tous les ans en l’absence d’irrigation.
          </p>
          <p>
            - Réserve en eau moyenne (125 à 175 mm) : situation intermédiaire.
          </p>
          <p>
            - Réserve en eau élevée ('{'>'}' 175 mm) sol profond, peu caillouteux,
            avec texture équilibrée. Les symptômes de stress hydrique sont peu
            fréquents même sans irrigation.
          </p>
        </OverlayPanel>

        <OverlayPanel
          ref={(el) => (this.helpPrecocite = el)}
          style={{ width: "280px" }}
        >
          <p>
            Les gammes de précocité sont établies sur les dates moyennes de
            stades phénologiques suivantes :
          </p>

          <table>
            <thead>
              <tr>
                <th></th>
                <th>Précoce</th>
                <th>Moyen</th>
                <th>Tardif</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>Débourrement</td>
                <td>01/04</td>
                <td>01/04</td>
                <td>01/04</td>
              </tr>

              <tr>
                <td>Floraison</td>
                <td>22/05</td>
                <td>01/06</td>
                <td>08/06</td>
              </tr>
              <tr>
                <td>Véraison</td>
                <td>12/07</td>
                <td>01/08</td>
                <td>16/08</td>
              </tr>

              <tr>
                <td>Vendange</td>
                <td>15/08</td>
                <td>10/09</td>
                <td>30/09</td>
              </tr>
            </tbody>
          </table>
        </OverlayPanel>

        {/*   <div className="action-header-fixed">

            <div className="p-grid">
            <div className="p-col">
                <Button label="Annuler" style={{width: '100%'}} className="p-button-secondary"/>

                </div>
                <div className="p-col">
                <Button label="Sauvegarder" style={{width: '100%'}} className="p-button-success"/>
                </div>
                </div>
            </div>

        */}

        <Growl ref={(el) => (this.errorsMessages = el)} />

        <div className="p-grid">
          <div id="col_informations" className="p-col-12">
            <Fieldset
              id="fielset_information"
              legend="Informations"
              className="fieldset_tutorial"
            >
              <div className="p-grid">
                <div className="p-md-8 p-col-12">
                  <h4 style={{ marginTop: "0px" }}>
                    Ici texte poiur le peche ?
                  </h4>
                  <ul>
                    <li>
                      Un secteur géographique, à localiser en cliquant sur la
                      carte.
                    </li>
                  </ul>
                </div>

                <div
                  className="p-md-4 p-col-12"
                  style={{ textAlign: "center" }}
                >
                  <a
                    href="https://www.youtube.com/watch?v=qDzs_8unkKc"
                    target="_blank"
                  >
                    <img
                      src="/assets/icons/icon_tuto.png"
                      style={{ width: "85px" }}
                      alt="tutos"
                    />
                    <br />
                    Vous pouvez aussi visionner
                    <br />
                    notre tuto en cliquant ici
                  </a>
                </div>
              </div>
            </Fieldset>
          </div>
          <div className="p-col-12">
            <div className="p-grid dashboard">
              <div id="col_panel_1" className="p-col-12 p-md-6 p-lg-6">
                <div id="card_nom" className="card card-w-title">
                  <h1>Nom</h1>
                  <div className="p-grid">
                    <div className="p-col">
                      <InputText
                        value={this.state.parcelle.nom}
                        onChange={(e) => {
                          var parc = this.state.parcelle;
                          parc.nom = e.target.value;
                          this.setState({ parcelle: parc });
                        }}
                        style={{ width: "100%" }}
                        className={this.state.errorNom ? "p-error" : ""}
                      />
                    </div>
                  </div>
                </div>

                <div id="card_reserve_sol" className="card card-w-title">
                  <h1>
                    Réserve du sol{" "}
                    <img
                      style={{ width: "18px", height: "18px", float: "right" }}
                      alt="help"
                      src={"/assets/icons/iconHelp.png"}
                      onClick={(e) => this.helpTypeSol.toggle(e)}
                    />
                  </h1>
                  <div className="p-grid">
                    <div className="p-col">
                      <RadioButton
                        inputId="reserve_sol_faible"
                        value={EnumTypeSol.FAIBLE}
                        name="reservesol"
                        className={this.state.errorTypeSol ? "p-error" : ""}
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.typeSol = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.typeSol &&
                          this.state.parcelle.typeSol.code ===
                            EnumTypeSol.FAIBLE.code
                        }
                      />
                      <label
                        htmlFor="reserve_sol_faible"
                        className="p-radiobutton-label"
                      >
                        {EnumTypeSol.FAIBLE.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="reserve_sol_moyen"
                        value={EnumTypeSol.MOYEN}
                        name="reservesol"
                        className={this.state.errorTypeSol ? "p-error" : ""}
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.typeSol = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.typeSol &&
                          this.state.parcelle.typeSol.code ===
                            EnumTypeSol.MOYEN.code
                        }
                      />
                      <label
                        htmlFor="reserve_sol_moyen"
                        className="p-radiobutton-label"
                      >
                        {EnumTypeSol.MOYEN.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="reserve_sol_profond"
                        value={EnumTypeSol.FORT}
                        name="reservesol"
                        className={this.state.errorTypeSol ? "p-error" : ""}
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.typeSol = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.typeSol &&
                          this.state.parcelle.typeSol.code ===
                            EnumTypeSol.FORT.code
                        }
                      />
                      <label
                        htmlFor="reserve_sol_profond"
                        className="p-radiobutton-label"
                      >
                        {EnumTypeSol.FORT.label}
                      </label>
                    </div>
                  </div>

                  {this.habilitationService.isAuthorize("CUSTOM_RU") && (
                    <div className="p-grid">
                      <div className="p-col">
                        <RadioButton
                          inputId="reserve_sol_custom"
                          value={EnumTypeSol.CUSTOM}
                          name="reservesol"
                          className={this.state.errorTypeSol ? "p-error" : ""}
                          onChange={(e) => {
                            var nObject = this.state.parcelle;
                            nObject.typeSol = e.value;
                            this.setState({ parcelle: nObject });
                          }}
                          checked={
                            this.state.parcelle.typeSol
                              ? this.state.parcelle.typeSol.code ===
                                EnumTypeSol.CUSTOM.code
                              : ""
                          }
                        />
                        <label
                          htmlFor="reserve_sol_custom"
                          className="p-radiobutton-label"
                        >
                          {EnumTypeSol.CUSTOM.label}
                        </label>

                        {this.state.parcelle.typeSol &&
                          this.state.parcelle.typeSol.code ===
                            EnumTypeSol.CUSTOM.code && (
                            <div className="p-col">
                              <Spinner
                                value={this.state.parcelle.reserveUtileCustom}
                                onChange={(e) => {
                                  var nObject = this.state.parcelle;
                                  nObject.reserveUtileCustom = e.value;
                                  this.setState({ parcelle: nObject });
                                }}
                                min={1}
                                max={200}
                                step={1}
                                className={
                                  this.state.errorDebitLH ? "p-error" : ""
                                }
                              />
                            </div>
                          )}
                      </div>
                    </div>
                  )}
                </div>


                <div id="card_reserve_sol" className="card card-w-title">
                  <h1>
                    Matériel d'irrigation{" "}
                    <img
                      style={{ width: "18px", height: "18px", float: "right" }}
                      alt="help"
                      src={"/assets/icons/iconHelp.png"}
                      onClick={(e) => this.helpTypeSol.toggle(e)}
                    />
                  </h1>
                  <div className="p-grid">
                    <div className="p-col">
                      <RadioButton
                        inputId="materiel_gag"
                        value={EnumMaterielIrrigation.GAG}
                        name="materielirrigation"
                        className={this.state.errorMaterielIrrigation ? "p-error" : ""}
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.materielIrrigation = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.materielIrrigation &&
                          this.state.parcelle.materielIrrigation.code ===
                            EnumMaterielIrrigation.GAG.code
                        }
                      />
                      <label
                        htmlFor="materiel_gag"
                        className="p-radiobutton-label"
                      >
                        {EnumMaterielIrrigation.GAG.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="materiel_autre"
                        value={EnumMaterielIrrigation.AUTRE}
                        name="materielirrigation"
                        className={this.state.errorMaterielIrrigation ? "p-error" : ""}
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.materielIrrigation = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.materielIrrigation &&
                          this.state.parcelle.materielIrrigation.code ===
                            EnumMaterielIrrigation.AUTRE.code
                        }
                      />
                      <label
                        htmlFor="materiel_autre"
                        className="p-radiobutton-label"
                      >
                        {EnumMaterielIrrigation.AUTRE.label}
                      </label>
                    </div>
                    
                  </div>
                  
                </div>
              </div>

              <div id="col_panel_2" className="p-col-12 p-md-6 p-lg-6">
                <div id="col_map" className="p-col-12 p-md-12 p-lg-12">
                  <div id="mapexport"></div>

                  <Map
                    id="map_parcelle_creation"
                    ref="mapParcelleCreation"
                    center={this.state.mapCenter}
                    zoom={this.state.mapZoom}
                    onClick={this.addMarker}
                    style={{ height: "400px" }}
                    className={
                      this.state.errorLocalisation ? "p-error-div" : ""
                    }
                  >
                    <TileLayer
                      zIndex={1}
                      attribution='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                      url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
                    />

                    <LayersControl position="topright">
                      <ReactLeafletSearch
                        showMarker={false}
                        position="topright"
                        inputPlaceholder="recherche"
                        popUp={this.popupSearchMarker}
                        providerOptions={{ region: "fr" }}
                        zoom={14}
                        closeResultsOnClick={true}
                      />

                      <LayersControl.BaseLayer name="OpenStreet Map" checked>
                        <TileLayer
                          attribution='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                          url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
                        />
                      </LayersControl.BaseLayer>
                      <LayersControl.BaseLayer name="ArcGIS World Topo Map">
                        <TileLayer
                          attribution="Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community"
                          url="http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}"
                        />
                      </LayersControl.BaseLayer>

                      <LayersControl.BaseLayer name="IGN photos aériennes">
                        <TileLayer
                          attribution="IGN-F/Géoportail"
                          url="https://wxs.ign.fr/pratique/geoportail/wmts?&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}"
                        />
                      </LayersControl.BaseLayer>
                    </LayersControl>
                  </Map>
                </div>
              </div>
            </div>

            <br />

            <div className="p-grid dashboard">
              <div id="col_panel_3" className="p-col-12 p-md-4">
                <div id="card_panel_3" className="card timeline p-fluid">
                  <h1>
                    Cycle de la culture{" "}
                    <img
                      style={{ width: "18px", height: "18px", float: "right" }}
                      alt="help"
                      src={"/assets/icons/iconHelp.png"}
                      onClick={(e) => this.helpPrecocite.toggle(e)}
                    />
                  </h1>
                  saisir date début du cycle végétatif saisir date début récolte
                  <Calendar
                    locale={fr}
                    value={new Date(new Date().getFullYear() ,this.state.parcelle.debutCycle.split("/")[1] - 1,this.state.parcelle.debutCycle.split("/")[0])}
                    onChange={(e) => {
                      if (e.value) {
                        let change = this.state.parcelle;
                        change.debutCycle = e.value.getTime();

                        this.setState({
                          parcelle: change,
                          debutCycle: e.value,
                        });
                      } else {
                        this.setState({ debutCycle: e.value });
                      }
                    }}
                    showIcon={true}
                    dateFormat="dd/mm/yy"
                    /*maxDate={this.state.dateEnd}*/
                    minDate={new Date(new Date().getFullYear(), 4, 1)}
                    maxDate={new Date(new Date().getFullYear(), 4, 15)}
                    style={{ width: "100%" }}
                    className="calendar-option-carte-export"
                  />
                  <div className="p-grid">
                    <div className="p-col">
                      <RadioButton
                        inputId="phenlogie_1"
                        value={EnumPhenologie.PRECOCE}
                        name="phenlogie"
                        onChange={(e) => {
                          var parcelleMAJ = this.state.parcelle;
                          parcelleMAJ.phenologie = e.value;
                          this.setState({ parcelle: parcelleMAJ });
                          this.changePhenologie(e.value);
                        }}
                        checked={
                          this.state.parcelle.phenologie ===
                          EnumPhenologie.PRECOCE
                        }
                        className={this.state.errorPhenologie ? "p-error" : ""}
                      />
                      <label
                        htmlFor="phenlogie_1"
                        className="p-radiobutton-label"
                      >
                        {EnumPhenologie.PRECOCE.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="phenlogie_2"
                        value={EnumPhenologie.MOYENNE}
                        name="phenlogie"
                        onChange={(e) => {
                          var parcelleMAJ = this.state.parcelle;
                          parcelleMAJ.phenologie = e.value;
                          this.setState({ parcelle: parcelleMAJ });
                          this.changePhenologie(e.value);
                        }}
                        checked={
                          this.state.parcelle.phenologie ===
                          EnumPhenologie.MOYENNE
                        }
                        className={this.state.errorPhenologie ? "p-error" : ""}
                      />
                      <label
                        htmlFor="phenlogie_2"
                        className="p-radiobutton-label"
                      >
                        {EnumPhenologie.MOYENNE.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="phenlogie_3"
                        value={EnumPhenologie.TARDIVE}
                        name="phenlogie"
                        onChange={(e) => {
                          var parcelleMAJ = this.state.parcelle;
                          parcelleMAJ.phenologie = e.value;
                          this.setState({ parcelle: parcelleMAJ });
                          this.changePhenologie(e.value);
                        }}
                        checked={
                          this.state.parcelle.phenologie ===
                          EnumPhenologie.TARDIVE
                        }
                        className={this.state.errorPhenologie ? "p-error" : ""}
                      />
                      <label
                        htmlFor="phenlogie_3"
                        className="p-radiobutton-label"
                      >
                        {EnumPhenologie.TARDIVE.label}
                      </label>
                    </div>
                  </div>
                  <br />
                  <div className="p-grid">
                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#009688" }}>
                        {this.state.parcelle.debutCycle}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#009688" }}
                      >
                        filter_1
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#009688" }}
                      >
                        Début du cycle végétatif
                      </span>
                      <span className="event-text"> </span>
                      <div className="event-content"></div>
                    </div>

                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#E91E63" }}>
                        {this.state.parcelle.debutRecolte}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#E91E63" }}
                      >
                        filter_2
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#E91E63" }}
                      >
                        Pleine végétation
                      </span>
                      <span className="event-text"></span>
                    </div>

                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#9c27b0" }}>
                        {this.state.parcelle.veraison}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#9c27b0" }}
                      >
                        filter_3
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#9c27b0" }}
                      >
                        Début de récolte
                      </span>
                      <span className="event-text"></span>
                    </div>

                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#ff9800" }}>
                        {this.state.parcelle.recolte}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#ff9800" }}
                      >
                        filter_4
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#ff9800" }}
                      >
                        Fin de récole
                      </span>
                      <span className="event-text"></span>
                    </div>

                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#ff9800" }}>
                        {this.state.parcelle.recolte}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#ff9800" }}
                      >
                        filter_5
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#ff9800" }}
                      >
                        Fin de dycle
                      </span>
                      <span className="event-text"></span>
                      <div className="event-content">
                        <img
                          src="/assets/cultures/recolte_pechers.jpg"
                          alt="recolte"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-col-12 p-md-4">
                <Button
                  icon="pi pi-times"
                  label="Annuler"
                  onClick={() => (window.location = "/situations")}
                  style={{ width: "100%", height: "80px" }}
                  className="p-button-secondary"
                />
              </div>

              <div id="col_panel_4" className="p-col-12 p-md-4">
                <Button
                  icon="pi-md-check"
                  label="Sauvegarder"
                  onClick={this.saveParcelle}
                  style={{ width: "100%", height: "80px" }}
                  className="p-button-success"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
