import React, { Component } from "react";

import "leaflet/dist/leaflet.css";

import { Chart } from "primereact/chart";

import { isMobile } from "../../../constants/constants";
import { Button } from "primereact/button";
import { ParcelleService } from "../../services/ParcelleService";
import { InputText } from "primereact/inputtext";
import { ModelService } from "../../../services/ModelService";
import axios from "axios";

import { HabilitationService } from "../../../services/HabilitationService";

import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Dropdown } from "primereact/dropdown";
import { RingLoader } from "react-spinners";

// Require utile pour zoom sur les charts
require("hammerjs");
require("chartjs-plugin-zoom");

const CancelToken = axios.CancelToken;

export default class RechargeHivernale extends Component {
  constructor() {
    super();

    this.state = {
      source: CancelToken.source(),

      loading: true,

      mapCenter: [43.6, 3.8],
      mapZoom: 9,

      lineChartDataPhfbEtudeHivernale: {
        datasets: [],
      },

      displayMobile: isMobile(),

      anneeSelectEtudeHivernale: new Date().getFullYear(),
      anneesOptionsForModel: [
        { value: new Date().getFullYear() },
        { value: new Date().getFullYear() - 1 },
        { value: new Date().getFullYear() - 2 },
        { value: new Date().getFullYear() - 3 },
        { value: new Date().getFullYear() - 4 },
        { value: new Date().getFullYear() - 5 },
        { value: new Date().getFullYear() - 6 },
        { value: new Date().getFullYear() - 7 },
        { value: new Date().getFullYear() - 8 },
        { value: new Date().getFullYear() - 9 },
        { value: new Date().getFullYear() - 10 },
      ],
    };

    this.exportResultatRechargeHivernale = this.exportResultatRechargeHivernale.bind(
      this
    );

    this.resetZoom = this.resetZoom.bind(this);

    this.parcelleService = new ParcelleService();
    this.modelService = new ModelService();
    this.habilitationService = new HabilitationService();

    this.simulationOptions = {
      title: {
        //display: true,
        display: false,
        text: "Potentiel de base",
        fontSize: 16,
      },
      legend: {
        display: true,
        position: "bottom",
        labels: {
          usePointStyle: true,
        },
      },
      hover: {
        mode: "single",
      },
      tooltips: {
        mode: "single",
        callbacks: {},
      },
      scales: {
        xAxes: [
          {
            type: "time",
            time: {
              unit: "month",
              displayFormats: {
                month: "MMM YYYY",
              },
            },
          },
        ],

        yAxes: [
          {
            id: "y-axis-left",
            display: true,
            position: "left",
            scaleLabel: {},
            ticks: {
              //fontColor: '#03A9F4',
            },
          },
          {
            id: "y-axis-right",
            display: true,
            position: "right",
            scaleLabel: {
              display: true,
              labelString: "mm",
              fontColor: "#0073cc",
              position: "top",
            },
            ticks: {
              min: 0,
              fontColor: "#0073cc",
            },
          },
        ],
      },

      plugins: {
        filler: {
          propagate: true,
        },

        zoom: {
          // Container for zoom options
          zoom: {
            // Boolean to enable zooming
            enabled: true,

            // Enable drag-to-zoom behavior
            //drag: true,
            drag: {
              borderColor: "black",
              borderWidth: 1,
              backgroundColor: "rgb(225,225,225)",
            },
            mode: "xy",

            rangeMin: {
              x: null,
              y: null,
            },
            rangeMax: {
              x: null,
              y: null,
            },

            speed: 0.1,

            onZoom: this.displayButtonResetZoomPhfb,
          },
        },
      },
    };
  }

  componentDidMount() {
    //si le role
    if (this.habilitationService.isAuthorize("ETUDE_RECHARGE_HIVERNALE")) {
      this.loadSimulationEtudeHivernale(this.state.anneeSelectEtudeHivernale);
    }
  }

  componentWillUnmount() {}

  resetZoom(ref, title) {
    // permet de voir le detail de l'object
    //console.log('Object: ', ref);

    var chart = ref.chart;
    chart.resetZoom();

    if (title === "phfb") {
      this.setState({ displayResetZoomPhfb: false });
    } else if (title === "etp") {
      this.setState({ displayResetZoomETP: false });
    } else if (title === "temperature") {
      this.setState({ displayResetZoomTemp: false });
    }
  }

  exportResultatRechargeHivernale() {
    this.setState({ loadingExport: true });

    this.modelService.exportResultatRechargeHivernale(
      this.props.location.state.idParcelle,
      this.state.anneeSelectEtudeHivernale,
      this,
      this.state.source
    );
  }

  loadSimulationEtudeHivernale(annee) {
    if (!annee) {
      annee = this.state.anneeSelectEtudeHivernale;
    }

    this.modelService.loadEtudeHivernate(
      this.props.location.state.idParcelle,
      annee,
      this,
      this.state.source
    );

    this.modelService.loadDatesChangeConseil(
      this.props.location.state.idParcelle,
      annee,
      this,
      this.state.source
    );
  }

  render() {
    return (
      <div>
        {this.state.loadingExport && (
          <div className="loader-fixed">
            <div style={{ width: "50px", margin: "0 auto" }}>
              <RingLoader
                margin={150}
                sizeUnit={"px"}
                size={50}
                color={"#123abc"}
                loading={true}
                style={{ margin: "0 auto" }}
              />
            </div>

            <div
              style={{ fontSize: "16px", margin: "0 auto", color: "#3F51B5" }}
            >
              Export en cours
            </div>
          </div>
        )}

        {!this.state.isModification &&
          this.habilitationService.isAuthorize("ETUDE_RECHARGE_HIVERNALE") && (
            <div className="p-grid">
              <div className="p-col-12 p-md-8 p-lg-8">
                <div className="card">
                  <div>
                    <h1 className="centerText">
                      Etude recharge hivernale des sols vigne{" "}
                      {/*gerer avec habilitation ??? car si on change l'années on prend les parametreage courantquand memem ... qui peuvent etre different de l'an dernier... */}
                    </h1>

                    <div style={{ margin: "auto", textAlign: "center" }}>
                      <Chart
                        id="etude_hivernale"
                        ref="refEtudeHivernaleChart"
                        type="bar"
                        data={this.state.lineChartDataPhfbEtudeHivernale}
                        options={this.simulationOptions}
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-col-12 p-md-4 p-lg-4">
                <div className="card">
                  <div>
                    <h1 className="centerText">Paramètres</h1>

                    <div className="p-grid">
                      <div
                        className="p-col-12"
                        style={{ textAlign: "right", padding: 0 }}
                      >
                        <h4 className="centerText">Année</h4>
                      </div>

                      <div className="p-col-12">
                        <Dropdown
                          optionLabel="value"
                          optionValue="value"
                          value={this.state.anneeSelectEtudeHivernale}
                          options={this.state.anneesOptionsForModel}
                          onChange={(e) => {
                            this.setState({
                              anneeSelectEtudeHivernale: e.value,
                            });
                            this.loadSimulationEtudeHivernale(e.value);
                          }}
                        />
                      </div>
                    </div>

                    <div className="p-grid">
                      <div
                        className="p-col-12"
                        style={{ textAlign: "right", padding: 0 }}
                      >
                        <h4 className="centerText">
                          Niveau de la réserve au 1er avril (en mm)
                        </h4>
                      </div>

                      <div className="p-col-12">
                        <InputText
                          value={this.state.reserveUtile1Avril}
                          onChange={(e) => {
                            this.setState(
                              { reserveUtile1Avril: e.target.value },
                              () => this.loadSimulationEtudeHivernale()
                            );
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-col-12 p-md-6 p-lg-6">
                <div className="card">
                  <div>
                    <h1 className="centerText">Résultat</h1>

                    <div style={{ margin: "auto", textAlign: "center" }}>
                      <DataTable value={this.state.dataTableDatesChangeSeuil}>
                        <Column field="date" header="Date"></Column>
                      </DataTable>
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-col-12 p-md-6 p-lg-6">
                <div className="card">
                  <div>
                    <h1 className="centerText">Export</h1>

                    <div style={{ margin: "auto", textAlign: "center" }}>
                      <Button
                        id="button_abonner"
                        className="pink-btn "
                        label="Export résultat"
                        icon="pi-md-file-download"
                        onClick={() => {
                          this.exportResultatRechargeHivernale();
                        }}
                        style={{ marginBottom: "0px" }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
      </div>
    );
  }
}
