import React, { Component } from "react";

import {
  Map,
  TileLayer,
  Marker,
  Rectangle,
  LayersControl,
} from "react-leaflet";

import L from "leaflet";
import "leaflet/dist/leaflet.css";

import { RadioButton } from "primereact/radiobutton";
import { Chart } from "primereact/chart";
import { OverlayPanel } from "primereact/overlaypanel";

import { EnumTypeVin, EnumTypeSol, EnumPhenologie } from "../../../enums/enums";

import {
  IconError,
  IconConsigneGreen,
  URIWSServeur,
  IconConsigneRed,
  IconConsigneOrange,
  IconMarkerBlue,
  isMobile,
} from "../../../constants/constants";
import { Button } from "primereact/button";
import { ParcelleService } from "../../services/ParcelleService";
import { InputText } from "primereact/inputtext";
import { ModelService } from "../../../services/ModelService";
import axios from "axios";
import { Message } from "primereact/message";
import { Growl } from "primereact/growl";
import { RingLoader } from "react-spinners";
import { HabilitationService } from "../../../services/HabilitationService";

import { ReactLeafletSearch } from "react-leaflet-search";
import { Dialog } from "primereact/dialog";
import { config } from "../../../../config";
import { Fieldset } from "primereact/fieldset";
import { Spinner } from "primereact/spinner";
import { Dropdown } from "primereact/dropdown";
import RechargeHivernale from "./RechargeHivernale";
import IntegrationIrrigation from "./IntegrationIrrigation";

// Require utile pour zoom sur les charts
require("hammerjs");
require("chartjs-plugin-zoom");

const CancelToken = axios.CancelToken;

export default class ParcelleVigne extends Component {
  constructor() {
    super();

    this.state = {
      source: CancelToken.source(),

      loading: true,

      mapCenter: [43.6, 3.8],
      mapZoom: 9,

      isModification: false,
      isAuthoriseModificaton: false,
      etatParcelle: "colorbox-white",
      iconEtatParcelle: "",
      iconMarkerParcelle: IconError,
      lineChartDataPhfb: {
        //labels: ["Janvier", "February", "March", "April", "May", "June", "July"],
        datasets: [],
      },
      lineChartDataPhfbEtudeHivernale: {
        //labels: ["Janvier", "February", "March", "April", "May", "June", "July"],
        datasets: [],
      },
      lineChartDataSimulation: {
        datasets: [],
      },
      lineChartDataETP: {
        datasets: [],
      },
      lineChartDataPluie: {
        datasets: [],
      },

      lineChartDataTemp: {
        datasets: [],
      },

      simulation: "1",
      uniteSimulation: "phfb",
      declencheSimulation: "min",
      coeffEfficaciteIrrigation: 1,
      dataTableSimulation: [],

      displayResetZoomPhfb: false,
      displayResetZoomETP: false,
      displayResetZoomTemp: false,

      isSurperpositionClasses: false,
      displayMobile: isMobile(),

      anneeSelect: new Date().getFullYear(),
      anneeSelectSimulation: new Date().getFullYear(),
      anneeSelectEtudeHivernale: new Date().getFullYear(),
      anneesOptionsForModel: [
        { value: new Date().getFullYear() },
        { value: new Date().getFullYear() - 1 },
        { value: new Date().getFullYear() - 2 },
        { value: new Date().getFullYear() - 3 },
        { value: new Date().getFullYear() - 4 },
        { value: new Date().getFullYear() - 5 },
        { value: new Date().getFullYear() - 6 },
        { value: new Date().getFullYear() - 7 },
        { value: new Date().getFullYear() - 8 },
        { value: new Date().getFullYear() - 9 },
        { value: new Date().getFullYear() - 10 },
      ],
    };

    /*  events: [{ id: 1, title: "All Day Event", start: "2020-10-15" }],
    options: {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      defaultView: "dayGridMonth",
      locale: frLocale,
      editable: true,
      dateClick: (e) => {
        alert("ici");
      },
    },*/

    this.loadSimulation = this.loadSimulation.bind(this);
    this.loadSimulationEtudeHivernale = this.loadSimulationEtudeHivernale.bind(
      this
    );
    this.exportResultatRechargeHivernale = this.exportResultatRechargeHivernale.bind(
      this
    );

    this.loadData = this.loadData.bind(this);
    this.updateParcelle = this.updateParcelle.bind(this);
    this.deleteParcelle = this.deleteParcelle.bind(this);
    this.annuler = this.annuler.bind(this);

    this.resetZoom = this.resetZoom.bind(this);
    this.reloadClasses = this.reloadClasses.bind(this);

    this.onModification = this.onModification.bind(this);

    this.displayButtonResetZoomPhfb = this.displayButtonResetZoomPhfb.bind(
      this
    );

    this.displayButtonResetZoomETP = this.displayButtonResetZoomETP.bind(this);
    this.displayButtonResetZoomTemp = this.displayButtonResetZoomTemp.bind(
      this
    );

    this.parcelleService = new ParcelleService();
    this.modelService = new ModelService();
    this.habilitationService = new HabilitationService();

    this.refPluieChart = React.createRef();
    this.refTempChart = React.createRef();

    this.phfbChartOptions = {
      title: {
        //display: true,
        display: false,
        text: "Potentiel de base",
        fontSize: 16,
      },
      legend: {
        display: true,
        position: "bottom",
        labels: {
          usePointStyle: true,
        },
        onClick: function (evt, legendItem) {
          //alert('legend onClick: event:' + evt+'item :'+ legendItem.text);
          //alert(JSON.stringify(legendItem))

          if (
            legendItem.text !== "Potentiel de base" &&
            legendItem.text !== "Pluviométrie"
          ) {
            alert("Cette légende ne peut pas être désactivé");
            evt.stopPropagation();
          } else {
            this.chart.getDatasetMeta(
              legendItem.datasetIndex
            ).hidden = !this.chart.getDatasetMeta(legendItem.datasetIndex)
              .hidden;

            this.chart.options.scales.yAxes[
              legendItem.datasetIndex
            ].display = !this.chart.options.scales.yAxes[
              legendItem.datasetIndex
            ].display;

            this.chart.update();
          }
        },
      },
      hover: {
        mode: "single",
      },
      tooltips: {
        mode: "single",
        callbacks: {},
      },
      scales: {
        xAxes: [
          {
            type: "time",
            time: {
              unit: "month",
              displayFormats: {
                month: "MMM YYYY",
              },
            },
          },
        ],

        yAxes: [
          {
            id: "y-axis-left",
            display: true,
            position: "left",
            scaleLabel: {
              display: true,
              labelString: "MPa",
              //fontSize: 10,
              //fontColor: '#03A9F4',
            },
            ticks: {
              min: -1.5,
              max: 0,
              //fontColor: '#03A9F4',
            },
          },
          {
            id: "y-axis-right",
            display: true,
            position: "right",
            scaleLabel: {
              display: true,
              labelString: "mm",
              fontColor: "#0073cc",
              position: "top",
            },
            ticks: {
              min: 0,
              fontColor: "#0073cc",
            },
          },
        ],
      },

      // plugins: {
      //     zoom: {
      //         pan: {
      //             enabled: true,
      //             mode: 'xy'
      //         },
      //         zoom: {
      //             enabled: true,
      //             mode: 'xy'
      //         }
      //     }
      // }

      plugins: {
        filler: {
          propagate: true,
        },

        zoom: {
          // Container for zoom options
          zoom: {
            // Boolean to enable zooming
            enabled: true,

            // Enable drag-to-zoom behavior
            //drag: true,
            drag: {
              borderColor: "black",
              borderWidth: 1,
              backgroundColor: "rgb(225,225,225)",
            },

            // Drag-to-zoom rectangle style can be customized
            // drag: {
            // 	 borderColor: 'rgba(225,225,225,0.3)'
            // 	 borderWidth: 5,
            // 	 backgroundColor: 'rgb(225,225,225)'
            // },

            // Zooming directions. Remove the appropriate direction to disable
            // Eg. 'y' would only allow zooming in the y direction
            mode: "xy",

            rangeMin: {
              // Format of min zoom range depends on scale type
              x: null,
              y: null,
            },
            rangeMax: {
              // Format of max zoom range depends on scale type
              x: null,
              y: null,
            },

            // Speed of zoom via mouse wheel
            // (percentage of zoom on a wheel event)
            speed: 0.1,

            onZoom: this.displayButtonResetZoomPhfb,
            // Function called once zooming is completed
            // Useful for dynamic data loading
            //onZoom: function({chart}) { console.log(`I was zoomed!!!`); }
          },
        },
      },
    };

    this.etpChartOptions = {
      maintainAspectRatio: false,
      title: {
        //display: true,
        display: false,
        text: "ETP",
        fontSize: 16,
      },
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            type: "time",
            time: {
              unit: "month",
              displayFormats: {
                month: "MMM YYYY",
              },
            },
          },
        ],
      },

      plugins: {
        zoom: {
          // Container for zoom options
          zoom: {
            // Boolean to enable zooming
            enabled: true,

            // Enable drag-to-zoom behavior
            //drag: true,
            drag: {
              borderColor: "black",
              borderWidth: 1,
              backgroundColor: "rgb(225,225,225)",
            },

            // Drag-to-zoom rectangle style can be customized
            // drag: {
            // 	 borderColor: 'rgba(225,225,225,0.3)'
            // 	 borderWidth: 5,
            // 	 backgroundColor: 'rgb(225,225,225)'
            // },

            // Zooming directions. Remove the appropriate direction to disable
            // Eg. 'y' would only allow zooming in the y direction
            mode: "xy",

            rangeMin: {
              // Format of min zoom range depends on scale type
              x: null,
              y: null,
            },
            rangeMax: {
              // Format of max zoom range depends on scale type
              x: null,
              y: null,
            },

            // Speed of zoom via mouse wheel
            // (percentage of zoom on a wheel event)
            speed: 0.1,

            // Function called once zooming is completed
            // Useful for dynamic data loading
            //onZoom: function({chart}) { console.log(`I was zoomed!!!`); }
            onZoom: this.displayButtonResetZoomETP,
          },
        },
      },
    };

    this.tempChartOptions = {
      maintainAspectRatio: false,
      title: {
        //display: true,
        display: false,
        text: "Température",
        fontSize: 16,
      },
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            type: "time",
            time: {
              unit: "month",
              displayFormats: {
                month: "MMM YYYY",
              },
            },
          },
        ],
      },
      plugins: {
        zoom: {
          // Container for zoom options
          zoom: {
            // Boolean to enable zooming
            enabled: true,

            drag: {
              borderColor: "black",
              borderWidth: 1,
              backgroundColor: "rgb(225,225,225)",
            },

            mode: "xy",

            rangeMin: {
              // Format of min zoom range depends on scale type
              x: null,
              y: null,
            },
            rangeMax: {
              // Format of max zoom range depends on scale type
              x: null,
              y: null,
            },

            speed: 0.1,

            onZoom: this.displayButtonResetZoomTemp,
          },
        },
      },
    };
  }

  componentDidMount() {
    if (!(this.props.location.state && this.props.location.state.idParcelle)) {
      this.props.history.push("/situations");
      return;
    }

    window.addEventListener("resize", this.updateDimensions.bind(this));

    if (this.props.location.state.creation === "succes") {
      this.setState({
        visibleDialogCreationSuivante:
          parseInt(this.props.location.state.nbrSituations) <
          parseInt(localStorage.getItem("nbr_situation_global")),
      });
    }

    this.loadParcelle(this.props.location.state.idParcelle);
    this.loadData(this.props.location.state.idParcelle, this.state.anneeSelect);
    this.loadClasses(
      this.props.location.state.idParcelle,
      this.state.isSurperpositionClasses,
      this.state.anneeSelect
    );

    //si le role
    if (this.habilitationService.isAuthorize("ETUDE_RECHARGE_HIVERNALE")) {
      this.loadSimulationEtudeHivernale(this.state.anneeSelectEtudeHivernale);
    }

    setTimeout(
      function () {
        //Start the timer
        //alert('delay')

        if (this.props.location.state) {
          this.props.location.state.creation = "";
        }
      }.bind(this),
      5000
    );

    //  var map = this.refs.mapParcelle.leafletElement;
    //  map.invalidateSize(true);
  }

  displayButtonResetZoomPhfb() {
    this.setState({ displayResetZoomPhfb: true });
  }

  displayButtonResetZoomETP() {
    this.setState({ displayResetZoomETP: true });
  }

  displayButtonResetZoomTemp() {
    this.setState({ displayResetZoomTemp: true });
  }

  updateDimensions() {
    this.setState({ displayMobile: isMobile() });
  }

  componentWillUnmount() {
    this.state.source.cancel("Operation canceled situation - change page.");
  }

  annuler() {
    if (this.state.currentMarker != null) {
      var map = this.refs.mapParcelle.leafletElement;
      map.removeLayer(this.state.currentMarker);
      map.removeLayer(this.state.currentCircle);
    }

    this.loadParcelle(this.props.location.state.idParcelle);
    this.reloadClasses(this.state.anneeSelect);
  }

  onModification() {
    if (this.state.isAuthoriseModificaton) {
      this.setState({ isModification: true });
      this.state.source.cancel("Operation canceled situation - modification.");
      this.setState({ csv: "" });
      this.setState({ source: CancelToken.source() });

      var map = this.refs.mapParcelle.leafletElement;
      var marker = new L.Marker([
        this.state.parcelle.latitude,
        this.state.parcelle.longitude,
      ]).addTo(map);

      //var circle = new L.Circle(e.latlng, { radius: 1000, weight: 1 }).on('click', this.onClickMarker).addTo(map);
      var circle = new L.rectangle(
        L.latLngBounds(
          [
            this.state.parcelle.latitude - 0.005,
            this.state.parcelle.longitude - 0.005,
          ],
          [
            this.state.parcelle.latitude + 0.005,
            this.state.parcelle.longitude + 0.005,
          ]
        )
      ).addTo(map);

      this.setState({ currentMarker: marker });
      this.setState({ currentCircle: circle });

      //this.setState({ currentMarker: null });
      this.setState({ currentMarker: marker });
      this.setState({ iconMarkerParcelle: IconMarkerBlue });
    } else {
      this.setState({ displayDialogNonModification: true });
    }
  }

  clickDisabled() {
    if (!this.state.isAuthoriseModificaton) {
      this.setState({ displayDialogNonModification: true });
    }
  }

  addMarker = (e) => {
    if (this.state.isModification) {
      var map = this.refs.mapParcelle.leafletElement;

      if (this.state.currentMarker != null) {
        map.removeLayer(this.state.currentMarker);
        map.removeLayer(this.state.currentCircle);
      }

      var marker = new L.Marker(e.latlng, { icon: IconMarkerBlue }).addTo(map);

      var circle = new L.rectangle(
        L.latLngBounds(
          [e.latlng.lat - 0.005, e.latlng.lng - 0.005],
          [e.latlng.lat + 0.005, e.latlng.lng + 0.005]
        )
      ).addTo(map);

      this.setState({ currentMarker: marker });
      this.setState({ currentCircle: circle });

      var parcelleMAJ = this.state.parcelle;

      parcelleMAJ.longitude = e.latlng.lng;
      parcelleMAJ.latitude = e.latlng.lat;
      this.setState({ parcelle: parcelleMAJ });

      map.setView(e.latlng);
    }
  };

  loadParcelle(idParcelle) {
    this.parcelleService.findParcelle(idParcelle, this, this.state.source);
    this.parcelleService.isAuthoriseModification(this, idParcelle);
    this.parcelleService.checkEtatParcelle(idParcelle, this, this.state.source);
  }

  changeEtatParcelle(couleur) {
    this.setState({ couleurCircle: couleur });

    if (couleur) {
      this.setState({ etatParcelle: "colorbox-" + couleur });

      if (couleur === "green") {
        this.setState({ iconMarkerParcelle: IconConsigneGreen });
        this.setState({ iconEtatParcelle: "insert_emoticon" });
      } else if (couleur === "yellow") {
        this.setState({ iconMarkerParcelle: IconConsigneOrange });
        this.setState({ iconEtatParcelle: "sentiment_dissatisfied" });
      }
      if (couleur === "red") {
        this.setState({ iconMarkerParcelle: IconConsigneRed });
        this.setState({ iconEtatParcelle: "mood_bad" });
      }
    } else {
      this.setState({ etatParcelle: "colorbox-gray" });
      this.setState({ iconEtatParcelle: "warning" });
      this.setState({ iconMarkerParcelle: IconError });
    }
  }

  detectPhenologie(parcelle) {
    if (parcelle.recolte === "15/08") {
      this.setState({ phenologie: EnumPhenologie.PRECOCE });
    } else if (parcelle.recolte === "10/09") {
      this.setState({ phenologie: EnumPhenologie.MOYENNE });
    } else if (parcelle.recolte === "30/09") {
      this.setState({ phenologie: EnumPhenologie.TARDIVE });
    }
  }

  changePhenologie(phenologie) {
    var parcelle = this.state.parcelle;

    if (phenologie === EnumPhenologie.PRECOCE) {
      parcelle.debourrement = "01/04";
      parcelle.floraison = "22/05";
      parcelle.veraison = "12/07";
      parcelle.recolte = "15/08";
    } else if (phenologie === EnumPhenologie.MOYENNE) {
      parcelle.debourrement = "01/04";
      parcelle.floraison = "01/06";
      parcelle.veraison = "01/08";
      parcelle.recolte = "10/09";
    } else if (phenologie === EnumPhenologie.TARDIVE) {
      parcelle.debourrement = "01/04";
      parcelle.floraison = "08/06";
      parcelle.veraison = "16/08";
      parcelle.recolte = "30/09";
    }

    this.setState({ parcelle: parcelle });
  }

  loadData(idParcelle, annee) {
    // LOAD ETP

    if (
      this.habilitationService.isAuthorize("ADMIN2", "NIVEAU_2", "NIVEAU_3")
    ) {
      axios
        .get(
          URIWSServeur +
            "/ws/meteo/data-by-parcelle-param?idParcelle=" +
            idParcelle +
            "&annee=" +
            annee +
            "&param=ETP",
          { cancelToken: this.state.source.token }
        )
        .then((response) => response.data)
        .then((data) => {
          var _lineChartData = {
            datasets: [],
          };
          _lineChartData.datasets.unshift({
            type: "line",
            label: "ETP",
            data: data,
            fill: false,
            pointRadius: 0,
            borderColor: "#0073cc", //#03A9F4
          });

          this.setState({ lineChartDataETP: _lineChartData });
          this.refs.etpChartLine.refresh();
        })
        .catch((err) => console.error(this.props.url, err.toString()));

      this.modelService.loadTemp(
        this.props.location.state.idParcelle,
        this,
        this.state.source
      );
    }
  }

  reloadClasses(annee) {
    //  this.state.lineChartDataPhfb = {
    //      datasets: []
    //  };

    // this.setState({lineChartDataPhfb :  {datasets: []}});
    this.loadClasses(
      this.props.location.state.idParcelle,
      this.state.isSurperpositionClasses,
      annee
    );
  }

  async loadClasses(idParcelle, isSurperpositionClasses, annee) {
    //console.log('loadClasses')

    //this.modelService.loadClasses();

    //let _lineChartData = { ...this.state.lineChartDataPhfb };
    var _lineChartData = {
      datasets: [],
    };

    _lineChartData.datasets.push({
      type: "line",
      label: "Potentiel de base",
      pointStyle: "line",
      data: [],
      fill: false,
      pointRadius: 0,
      borderColor: "#03A9F4", //#03A9F4
    });

    if (this.habilitationService.isAuthorize("NIVEAU_2", "NIVEAU_3")) {
      _lineChartData.datasets.push({
        type: "bar",
        label: "Pluviométrie",
        pointStyle: "rect",
        data: [],
        fill: false,
        pointRadius: 0,
        backgroundColor: "#0073cc",
        yAxisID: "y-axis-right",
      });
    }

    // Courbe cible

    await axios
      .get(
        URIWSServeur +
          "/ws/model/tendance-optimal-phfb-by-parcelle?idParcelle=" +
          idParcelle +
          "&annee=" +
          annee,
        { cancelToken: this.state.source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        // recreation des series avec date ?

        var dataphfbMax = [];
        var dataphfbMin = [];
        var dataphfbWarning = [];

        for (var i = 0; i < data.Date.length; i++) {
          dataphfbMax.push({ x: data.Date[i], y: data.PHFB_Max[i] });
          dataphfbMin.push({ x: data.Date[i], y: data.PHFB_Min[i] });
          dataphfbWarning.push({ x: data.Date[i], y: data.PHFB_Warning[i] });
        }

        //let _lineChartData = { ...this.state.lineChartDataPhfb };

        _lineChartData.datasets.push({
          type: "line",
          label: "Contrainte hydrique forte",
          data: dataphfbMin,
          //borderColor: '#ff6f4e87', //data.colorPhfbMin,
          borderColor: "rgba(255, 111, 78, 0.5294117647058824)", //data.colorPhfbMin,
          borderWidth: 1,
          pointRadius: 0,
          //fill:"-1"
          fill: "start",
          //fill: "none",
          //backgroundColor: '#ff6f4e87', //data.colorPhfbMin,
          backgroundColor: "rgba(255, 111, 78, 0.5294117647058824)", //data.colorPhfbMin,
          tooltip: false,
          yAxisID: "y-axis-left",
        });

        _lineChartData.datasets.push({
          type: "line",
          label: "Contrainte hydrique  se renforçant",
          data: dataphfbMin,
          //borderColor: 'orange', //data.colorPhfbMax,
          borderColor: "rgba(245, 152, 37, 0.56)", //data.colorPhfbMax,
          borderWidth: 1,
          pointRadius: 0,
          fill: "+1",
          //fill: "none",
          //backgroundColor: 'orange', //data.colorPhfbMax,
          backgroundColor: "rgba(245, 152, 37, 0.56)", //data.colorPhfbMax,
          tooltip: false,
          yAxisID: "y-axis-left",
        });

        _lineChartData.datasets.push({
          type: "line",
          label: "Contrainte hydrique optimale",
          data: dataphfbWarning,
          //borderColor: '#4caf50c2', //data.colorPhfbMax,
          borderColor: "rgba(76, 175, 80, 0.7607843137254902)", //data.colorPhfbMax,
          borderWidth: 1,
          pointRadius: 0,
          fill: "+1",
          //fill: "none",
          //backgroundColor: '#4caf50c2', //data.colorPhfbMax,
          backgroundColor: "rgba(76, 175, 80, 0.7607843137254902)", //data.colorPhfbMax,
          tooltip: false,
          yAxisID: "y-axis-left",
        });

        _lineChartData.datasets.push({
          type: "line",
          label: "Excès eau",
          data: dataphfbMax,
          //borderColor: '#007eff94', //data.colorPhfbMax,
          borderColor: "rgba(0, 126, 255, 0.5803921568627451)", //data.colorPhfbMax,
          borderWidth: 1,
          pointRadius: 0,
          fill: "end",
          //fill: "none",
          //backgroundColor: '#007eff94', //data.colorPhfbMax,
          backgroundColor: "rgba(0, 126, 255, 0.5803921568627451)", //data.colorPhfbMax,
          tooltip: false,
          yAxisID: "y-axis-left",
        });

        this.setState({ lineChartDataPhfb: _lineChartData });
        // this.refs.phfbchartLine.refresh();
      })

      .catch((err) => console.error(this.props.url, err.toString()));

    this.loadContrainteHydrique(idParcelle, annee);

    if (isSurperpositionClasses) {
      /* RAjouter type sol*/
      await axios
        .get(
          URIWSServeur +
            "/ws/bhr/phfb-by-classes-by-parcelle?idParcelle=" +
            idParcelle,
          { cancelToken: this.state.source.token }
        )
        .then((response) => response.data)
        .then((data) => {
          // recreation des series avec date ?

          var dataA = [];
          var dataB = [];
          var dataC = [];
          var dataD = [];
          var dataE = [];

          for (var i = 0; i < data.Date.length; i++) {
            dataA.push({ x: data.Date[i], y: data.A[i] });
            dataB.push({ x: data.Date[i], y: data.B[i] });
            dataC.push({ x: data.Date[i], y: data.C[i] });
            dataD.push({ x: data.Date[i], y: data.D[i] });
            dataE.push({ x: data.Date[i], y: data.E[i] });
          }

          // this.state.lineData.refresh
          var _lineChartData = { ...this.state.lineChartDataPhfb };
          //_lineChartData.labels.push(data.Date);

          // ici trouver pour ecraser series et pas rajouter !!!!!
          //_lineChartData.datasets = [];

          //console.log(_lineChartData.datasets);

          _lineChartData.datasets.push({
            type: "line",
            label: "Absence de contrainte",
            data: dataA,
            borderColor: data.colorA,
            borderWidth: 1,
            pointRadius: 0,
            //cubicInterpolationMode: 'monotone', trop de point donc aucun impact
            fill: "end",
            backgroundColor: data.colorA,
            tooltip: false,
          });

          _lineChartData.datasets.push({
            type: "line",
            label: "Contrainte faible",
            data: dataB,
            borderColor: data.colorB,
            borderWidth: 1,
            pointRadius: 0,
            fill: "-1",
            backgroundColor: data.colorB,
            tooltip: false,
          });

          _lineChartData.datasets.push({
            type: "line",
            label: "Contrainte modérée",
            data: dataC,
            borderColor: data.colorC,
            borderWidth: 1,
            pointRadius: 0,
            fill: "-1",
            backgroundColor: data.colorC,
            tooltip: false,
          });

          _lineChartData.datasets.push({
            type: "line",
            label: "Contrainte forte",
            data: dataD,
            borderColor: data.colorD,
            borderWidth: 1,
            pointRadius: 0,
            fill: "-1",
            backgroundColor: data.colorD,
            tooltip: false,
          });
          _lineChartData.datasets.push({
            type: "line",
            label: "Contrainte sévère",
            data: dataE,
            borderColor: data.colorE,
            borderWidth: 1,
            pointRadius: 0,
            fill: "-1",
            backgroundColor: data.colorE,
            tooltip: false,
          });

          if (this.refs.phfbchartLine) {
            this.refs.phfbchartLine.refresh();
          }
        })

        .catch((err) => console.error(this.props.url, err.toString()));
    }
  }

  loadContrainteHydrique(idParcelle, annee) {
    // load Contrainte hydrique

    if (this.habilitationService.isAuthorize("NIVEAU_2", "NIVEAU_3")) {
      axios
        .get(
          URIWSServeur +
            "/ws/meteo/data-by-parcelle-param?idParcelle=" +
            idParcelle +
            "&annee=" +
            annee +
            "&param=PLUIE",
          { cancelToken: this.state.source.token }
        )
        .then((response) => response.data)
        .then((data) => {
          // alert(JSON.stringify(data))
          var _lineChartData = { ...this.state.lineChartDataPhfb };
          _lineChartData.datasets[1] = {
            type: "bar",
            label: "Pluviométrie",
            pointStyle: "rect",
            data: data,
            fill: false,
            pointRadius: 0,
            backgroundColor: "#0073cc",
            yAxisID: "y-axis-right",
          };

          if (this.refs.phfbchartLine) {
            this.refs.phfbchartLine.refresh();
          }
        })
        .catch((err) => console.error(err.toString()));
    }

    // // Model bilan hydrique by rewrite moi : V2
    axios
      .get(
        URIWSServeur +
          "/ws/model/phfb-bilan-hydrique?idParcelle=" +
          idParcelle +
          "&annee=" +
          annee,
        { cancelToken: this.state.source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        var _lineChartData = { ...this.state.lineChartDataPhfb };
        _lineChartData.datasets[0] = {
          type: "line",
          label: "Potentiel de base",
          pointStyle: "line",
          data: data,
          fill: false,
          pointRadius: 0,
          borderColor: "#03A9F4",
          yAxisID: "y-axis-left",
        };

        if (this.refs.phfbchartLine) {
          this.refs.phfbchartLine.refresh();
        }
      });

    if (this.habilitationService.isAuthorize("ROLE_RESULT_CSV_VIEWER")) {
      // CSV all model
      axios
        .get(
          URIWSServeur +
            "/ws/model/all-bilan-hydrique?idParcelle=" +
            idParcelle,
          { cancelToken: this.state.source.token }
        )
        .then((response) => response.data)
        .then((data) => {
          this.setState({ csv: data });
        });
    }
  }

  async updateParcelle() {
    var errorsMessagesTexte = [];

    this.setState({ errorNom: false });

    if (!this.state.parcelle.nom) {
      this.setState({ errorNom: true });
      errorsMessagesTexte.push("Nom");
    }

    if (this.state.parcelle.nom.length > 120) {
      this.setState({ errorNom: true });
      errorsMessagesTexte.push("Nom (120 caractères)");
    }

    if (errorsMessagesTexte.length > 0) {
      this.errorsMessages.show({
        severity: "error",
        summary: "Champs non renseignés : ",
        detail: errorsMessagesTexte.join(", "),
      });
    } else {
      // call ws pour save parcelle
      await this.parcelleService.updateParcelle(this.state.parcelle, this);

      if (this.state.currentMarker != null) {
        var map = this.refs.mapParcelle.leafletElement;
        map.removeLayer(this.state.currentMarker);
        map.removeLayer(this.state.currentCircle);
      }

      this.reloadClasses(this.state.anneeSelect);
      this.loadData(this.state.parcelle.id, this.state.anneeSelect);
      this.loadParcelle(this.state.parcelle.id);

      setTimeout(
        function () {
          //Start the timer

          if (this.state.retourModification) {
            this.setState({ retourModification: "" });
          }
        }.bind(this),
        5000
      );
    }
  }

  deleteParcelle() {
    this.parcelleService.deleteParcelle(this.state.parcelle, this);
    // refreshr topbar
    this.props.callUpdateTopBar();
  }

  resetZoom(ref, title) {
    // permet de voir le detail de l'object
    //console.log('Object: ', ref);

    var chart = ref.chart;
    chart.resetZoom();

    if (title === "phfb") {
      this.setState({ displayResetZoomPhfb: false });
    } else if (title === "etp") {
      this.setState({ displayResetZoomETP: false });
    } else if (title === "temperature") {
      this.setState({ displayResetZoomTemp: false });
    }
  }

  exportResultatRechargeHivernale() {
    this.setState({ loadingExport: true });

    this.modelService.exportResultatRechargeHivernale(
      this.props.location.state.idParcelle,
      this.state.anneeSelectEtudeHivernale,
      this,
      this.state.source
    );
  }

  loadSimulation(annee) {
    if (!annee) {
      annee = this.state.anneeSelectSimulation;
    }

    this.modelService.loadSimulation(
      this.props.location.state.idParcelle,
      annee,
      this,
      this.state.source
    );

    this.modelService.loadSimulationDataTableResult(
      this.props.location.state.idParcelle,
      annee,
      this,
      this.state.source
    );
  }

  loadSimulationEtudeHivernale(annee) {
    if (!annee) {
      annee = this.state.anneeSelectEtudeHivernale;
    }

    this.modelService.loadEtudeHivernate(
      this.props.location.state.idParcelle,
      annee,
      this,
      this.state.source
    );

    this.modelService.loadDatesChangeConseil(
      this.props.location.state.idParcelle,
      annee,
      this,
      this.state.source
    );
  }

  render() {
    const footerDelete = (
      <div>
        <Button
          label="Annuler"
          icon="pi pi-times"
          onClick={(e) => this.setState({ visibleConfirmationDelete: false })}
        />

        <Button
          label="Confirmer"
          icon="pi pi-check"
          onClick={(e) => this.deleteParcelle()}
        />
      </div>
    );

    const footerSuivant = (
      <div>
        <Button
          label="Fermer"
          icon="pi pi-times"
          onClick={(e) =>
            this.setState({ visibleDialogCreationSuivante: false })
          }
        />

        <Button
          label="Nouvelle situation"
          icon="pi pi-plus"
          onClick={(e) => {
            this.props.history.push({
              pathname: "/situation/pecher/création",
              state: { idParcelle: null },
            });
          }}
        />
      </div>
    );

    let loading = (
      <div className="loader">
        <div style={{ width: "50px", margin: "0 auto" }}>
          <RingLoader
            margin={150}
            sizeUnit={"px"}
            size={50}
            color={"#123abc"}
            loading={true}
            style={{ margin: "0 auto" }}
          />
        </div>

        <div style={{ fontSize: "16px", margin: "0 auto", color: "#3F51B5" }}>
          Chargement de la situation
        </div>
      </div>
    );

    if (config.MODULE_VIGNE !== "actif") {
      return <div className="p-grid dashboard"> Module non accessible</div>;
    }

    if (this.state.loading) {
      return <div className="p-grid dashboard"> {loading} </div>;
    }

    if (this.state.loadingExport) {
      return (
        <div className="loader">
          <div style={{ width: "50px", margin: "0 auto" }}>
            <RingLoader
              margin={150}
              sizeUnit={"px"}
              size={50}
              color={"#123abc"}
              loading={true}
              style={{ margin: "0 auto" }}
            />
          </div>

          <div style={{ fontSize: "16px", margin: "0 auto", color: "#3F51B5" }}>
            Export en cours
          </div>
        </div>
      );
    }

    return (
      <div>
        <Dialog
          header="Modification désactivée"
          style={{ width: "30vw" }}
          visible={this.state.displayDialogNonModification}
          modal={true}
          dismissableMask={true}
          onHide={(e) => {
            this.setState({ displayDialogNonModification: false });
          }}
        >
          <div className="p-grid">
            <div className="p-col-12">Contactez-nous</div>
            <div
              className="p-col-12"
              style={{ textAlign: "center", paddingTop: "1.5rem" }}
            >
              <Button
                id="button_abonner"
                className="pink-btn "
                label="Contactez-nous"
                icon="pi-md-email"
                onClick={() => {
                  window.location.href = "/nous-contacter";
                }}
                style={{ marginBottom: "0px" }}
              />
            </div>
          </div>
        </Dialog>

        <OverlayPanel
          ref={(el) => (this.helpTypeVin = el)}
          style={{ width: "280px" }}
        >
          <p>
            - Vin Blanc ou Rosé à rendement naturel : il peut s’agir par exemple
            de Vins de Pays, de Cépage ou de Table
          </p>
          <p>
            - Vin Blanc ou Rosé à rendement maîtrisé : concerne les vins Blanc
            ou Rosé en AOP.
          </p>
          <p>
            - Vin Rouge à rendement naturel : il peut s’agir par exemple de Vins
            de Pays, de Cépage ou de Table.
          </p>
          <p>
            - Vin Rouge à rendement maîtrisé : concerne les vins Rouges en AOP.
          </p>
        </OverlayPanel>

        <OverlayPanel
          ref={(el) => (this.helpTypeSol = el)}
          style={{ width: "280px" }}
        >
          <p>
            Il s’agit de caractériser la réserve utile de votre sol, qui résulte
            de sa profondeur et de sa texture.
          </p>
          <p>
            - Faible réserve en eau : il peut s’agir de sols à texture grossière
            (sableuse, et/ou caillouteuse), ou de sols à texture plus fine, mais
            de faible profondeur (par exemple moins d’un mètre de terre sur la
            roche mère).
          </p>
          <p>
            - Réserve moyenne en eau : sols à texture moyenne, plutôt limoneuse,
            ou sol moyennement profond (entre 1 et 2 m de terre sur la roche
            mère).
          </p>
          <p>
            - Réserve en eau élevée : sols à texture fine, plutôt argileuse, ou
            a texture plus grossière mais très profonds (exemple sols de
            Costières).
          </p>
        </OverlayPanel>

        <OverlayPanel
          ref={(el) => (this.helpPrecocite = el)}
          style={{ width: "280px" }}
        >
          <p>
            Les gammes de précocité sont établies sur les dates moyennes de
            stades phénologiques suivantes :
          </p>

          <table>
            <thead>
              <tr>
                <th></th>
                <th>Précoce</th>
                <th>Moyen</th>
                <th>Tardif</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>Débourrement</td>
                <td>01/04</td>
                <td>01/04</td>
                <td>01/04</td>
              </tr>

              <tr>
                <td>Floraison</td>
                <td>22/05</td>
                <td>01/06</td>
                <td>08/06</td>
              </tr>
              <tr>
                <td>Véraison</td>
                <td>12/07</td>
                <td>01/08</td>
                <td>16/08</td>
              </tr>

              <tr>
                <td>Vendange</td>
                <td>15/08</td>
                <td>10/09</td>
                <td>30/09</td>
              </tr>
            </tbody>
          </table>
        </OverlayPanel>

        {this.props.location.state.creation === "succes" && (
          <div>
            <Message
              style={{ width: "100%", textAlign: "center", fontSize: "15px" }}
              severity="info"
              text="Création effectuée"
            ></Message>{" "}
            <br />
            <Dialog
              header="Félicitations !"
              footer={footerSuivant}
              visible={this.state.visibleDialogCreationSuivante}
              modal={true}
              onHide={() =>
                this.setState({ visibleDialogCreationSuivante: false })
              }
            >
              <div className="p-grid">
                <div className="p-col-12">
                  Vous avez créé votre situation, voulez-vous en créer une autre
                  ?
                  <br />
                </div>
              </div>
            </Dialog>
          </div>
        )}
        {this.state.retourModification === "succes" && (
          <div>
            <Message
              style={{ width: "100%", textAlign: "center", fontSize: "15px" }}
              severity="info"
              text="Modification effectuée"
            ></Message>{" "}
            <br />
          </div>
        )}

        <Growl ref={(el) => (this.errorsMessages = el)} />

        <div className="p-grid">
          <div className="p-col-12">
            <div className="p-grid dashboard">
              <div className="p-col-12 p-md-6 p-lg-6">
                <div
                  className="p-col-12 p-md-12 p-lg-12"
                  style={{ marginBottom: "10px" }}
                >
                  <div
                    className={
                      "p-grid card colorbox " + this.state.etatParcelle
                    }
                  >
                    <div className="p-col-4">
                      <i className="material-icons">
                        {this.state.iconEtatParcelle}
                      </i>

                      {/*sentiment-satisfied
                                    sentiment-neutral
                                    sentiment-dissatisfied
                                    check_circle  warning error */}
                    </div>
                    <div className="p-col-8">
                      {!this.state.isModification && (
                        <div
                          onClick={(e) => {
                            this.clickDisabled();
                          }}
                        >
                          <Button
                            disabled={!this.state.isAuthoriseModificaton}
                            className={
                              !this.state.isAuthoriseModificaton
                                ? " deep-orange-btn  p-disabled"
                                : "deep-orange-btn "
                            }
                            icon="pi-md-edit"
                            style={{ float: "right" }}
                            onClick={(e) => {
                              this.onModification();
                            }}
                          />

                          {this.habilitationService.isAuthorize("ADMIN") && (
                            <Button
                              icon="pi-md-delete"
                              className="p-button blue-grey-btn"
                              style={{ float: "right", marginRight: "8px" }}
                              onClick={(e) =>
                                this.setState({
                                  visibleConfirmationDelete: true,
                                })
                              }
                            />
                          )}
                        </div>
                      )}

                      {this.state.isModification && (
                        <div
                          style={{ float: "right", background: "transparent" }}
                        >
                          <Button
                            icon="pi pi-times"
                            className="p-button-secondary"
                            onClick={(e) => {
                              this.annuler();
                              this.setState({ isModification: false });
                            }}
                          />
                          <Button
                            icon="pi-md-check"
                            className="p-button-success"
                            style={{ marginLeft: "5px" }}
                            onClick={(e) => {
                              this.updateParcelle();
                            }}
                          />
                        </div>
                      )}

                      {!this.state.isModification && (
                        <span
                          className="colorbox-count"
                          style={{ lineHeight: "60px" }}
                        >
                          {this.state.parcelle.nom}
                        </span>
                      )}
                      {this.state.isModification && (
                        <span className="colorbox-count">
                          <InputText
                            className={this.state.errorNom ? "p-error" : ""}
                            style={{ width: "100%" }}
                            value={this.state.parcelle.nom}
                            onChange={(e) => {
                              var parc = this.state.parcelle;
                              parc.nom = e.target.value;
                              this.setState({ parcelle: parc });
                            }}
                          />{" "}
                        </span>
                      )}
                    </div>
                  </div>
                </div>

                <div className="card card-w-title">
                  <h1>
                    Type de vin{" "}
                    <img
                      style={{ width: "18px", height: "18px", float: "right" }}
                      alt="help"
                      src={"/assets/icons/iconHelp.png"}
                      onClick={(e) => this.helpTypeVin.toggle(e)}
                    />
                  </h1>
                  <div className="p-grid">
                    <div className="p-col">
                      <RadioButton
                        inputId="type_vin_1"
                        disabled={!this.state.isModification}
                        value={EnumTypeVin.VIN1}
                        name="typevin"
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.typeVin = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.typeVin.code ===
                          EnumTypeVin.VIN1.code
                        }
                      />
                      <label
                        htmlFor="type_vin_1"
                        className="p-radiobutton-label"
                      >
                        {EnumTypeVin.VIN1.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="type_vin_2"
                        disabled={!this.state.isModification}
                        value={EnumTypeVin.VIN2}
                        name="typevin"
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.typeVin = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.typeVin.code ===
                          EnumTypeVin.VIN2.code
                        }
                      />
                      <label
                        htmlFor="type_vin_2"
                        className="p-radiobutton-label"
                      >
                        {EnumTypeVin.VIN2.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="type_vin_3"
                        disabled={!this.state.isModification}
                        value={EnumTypeVin.VIN3}
                        name="typevin"
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.typeVin = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.typeVin.code ===
                          EnumTypeVin.VIN3.code
                        }
                      />
                      <label
                        htmlFor="type_vin_3"
                        className="p-radiobutton-label"
                      >
                        {EnumTypeVin.VIN3.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="type_vin_4"
                        disabled={!this.state.isModification}
                        value={EnumTypeVin.VIN4}
                        name="typevin"
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.typeVin = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.typeVin.code ===
                          EnumTypeVin.VIN4.code
                        }
                      />
                      <label
                        htmlFor="type_vin_4"
                        className="p-radiobutton-label"
                      >
                        {EnumTypeVin.VIN4.label}
                      </label>
                    </div>
                  </div>
                </div>

                <div className="card card-w-title">
                  <h1>
                    Réserve du sol{" "}
                    <img
                      style={{ width: "18px", height: "18px", float: "right" }}
                      alt="help"
                      src={"/assets/icons/iconHelp.png"}
                      onClick={(e) => this.helpTypeSol.toggle(e)}
                    />
                  </h1>
                  <div className="p-grid">
                    <div className="p-col">
                      <RadioButton
                        inputId="reserve_sol_faible"
                        disabled={!this.state.isModification}
                        value={EnumTypeSol.FAIBLE}
                        name="reservesol"
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.typeSol = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.typeSol.code ===
                          EnumTypeSol.FAIBLE.code
                        }
                      />
                      <label
                        htmlFor="reserve_sol_faible"
                        className="p-radiobutton-label"
                      >
                        {EnumTypeSol.FAIBLE.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="reserve_sol_moyen"
                        disabled={!this.state.isModification}
                        value={EnumTypeSol.MOYEN}
                        name="reservesol"
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.typeSol = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.typeSol.code ===
                          EnumTypeSol.MOYEN.code
                        }
                      />
                      <label
                        htmlFor="reserve_sol_moyen"
                        className="p-radiobutton-label"
                      >
                        {EnumTypeSol.MOYEN.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="reserve_sol_profond"
                        disabled={!this.state.isModification}
                        value={EnumTypeSol.FORT}
                        name="reservesol"
                        onChange={(e) => {
                          var nObject = this.state.parcelle;
                          nObject.typeSol = e.value;
                          this.setState({ parcelle: nObject });
                        }}
                        checked={
                          this.state.parcelle.typeSol.code ===
                          EnumTypeSol.FORT.code
                        }
                      />
                      <label
                        htmlFor="reserve_sol_profond"
                        className="p-radiobutton-label"
                      >
                        {EnumTypeSol.FORT.label}
                      </label>
                    </div>
                  </div>

                  {this.habilitationService.isAuthorize("CUSTOM_RU") && (
                    <div className="p-grid">
                      <div className="p-col">
                        <RadioButton
                          inputId="reserve_sol_custom"
                          disabled={!this.state.isModification}
                          value={EnumTypeSol.CUSTOM}
                          name="reservesol"
                          onChange={(e) => {
                            var nObject = this.state.parcelle;
                            nObject.typeSol = e.value;
                            this.setState({ parcelle: nObject });
                          }}
                          checked={
                            this.state.parcelle.typeSol.code ===
                            EnumTypeSol.CUSTOM.code
                          }
                        />
                        <label
                          htmlFor="reserve_sol_custom"
                          className="p-radiobutton-label"
                        >
                          {EnumTypeSol.CUSTOM.label}
                        </label>

                        {this.state.parcelle.typeSol.code ===
                          EnumTypeSol.CUSTOM.code && (
                          <div className="p-col">
                            <Spinner
                              disabled={!this.state.isModification}
                              value={this.state.parcelle.reserveUtileCustom}
                              onChange={(e) => {
                                var nObject = this.state.parcelle;
                                nObject.reserveUtileCustom = e.value;
                                this.setState({ parcelle: nObject });
                              }}
                              min={1}
                              max={200}
                              step={1}
                              className={
                                this.state.errorDebitLH ? "p-error" : ""
                              }
                            />
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="p-col-12 p-md-6 p-lg-6">
                <div className="p-col-12 p-md-12 p-lg-12 ">
                  <Map
                    ref="mapParcelle"
                    center={this.state.mapCenter}
                    zoom={this.state.mapZoom}
                    onClick={this.addMarker}
                    style={{ height: "350px" }}
                  >
                    <TileLayer
                      attribution='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                      url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
                    />

                    <LayersControl position="topright">
                      <ReactLeafletSearch
                        showMarker={false}
                        position="topright"
                        inputPlaceholder="recherche"
                        popUp={this.popupSearchMarker}
                        providerOptions={{ region: "fr" }}
                        zoom={14}
                        closeResultsOnClick={true}
                      />

                      <LayersControl.BaseLayer name="OpenStreet Map" checked>
                        <TileLayer
                          attribution='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                          url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
                        />
                      </LayersControl.BaseLayer>
                      <LayersControl.BaseLayer name="ArcGIS World Topo Map">
                        <TileLayer
                          attribution="Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community"
                          url="http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}"
                        />
                      </LayersControl.BaseLayer>

                      <LayersControl.BaseLayer name="IGN photos aériennes">
                        <TileLayer
                          attribution="IGN-F/Géoportail"
                          url="https://wxs.ign.fr/pratique/geoportail/wmts?&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}"
                        />
                      </LayersControl.BaseLayer>
                    </LayersControl>

                    {!this.state.isModification && (
                      <Marker
                        position={[
                          this.state.parcelle.latitude,
                          this.state.parcelle.longitude,
                        ]}
                        icon={this.state.iconMarkerParcelle}
                      ></Marker>
                    )}
                    {!this.state.isModification && (
                      <Rectangle
                        bounds={L.latLngBounds(
                          [
                            this.state.parcelle.latitude - 0.005,
                            this.state.parcelle.longitude - 0.005,
                          ],
                          [
                            this.state.parcelle.latitude + 0.005,
                            this.state.parcelle.longitude + 0.005,
                          ]
                        )}
                        color={this.state.couleurCircle}
                      ></Rectangle>
                    )}

                    {this.state.isModification && (
                      <Marker
                        position={[
                          this.state.parcelle.latitude,
                          this.state.parcelle.longitude,
                        ]}
                        icon={this.state.iconMarkerParcelle}
                      ></Marker>
                    )}
                  </Map>
                </div>
              </div>
            </div>

            <div className="p-grid dashboard">
              <div className="p-col-12 p-md-4">
                <div className="card timeline p-fluid">
                  <h1>
                    Précocité{" "}
                    <img
                      style={{ width: "18px", height: "18px", float: "right" }}
                      alt="help"
                      src={"/assets/icons/iconHelp.png"}
                      onClick={(e) => this.helpPrecocite.toggle(e)}
                    />
                  </h1>

                  <div className="p-grid">
                    <div className="p-col">
                      <RadioButton
                        inputId="phenlogie_1"
                        disabled={!this.state.isModification}
                        value={EnumPhenologie.PRECOCE}
                        name="phenlogie"
                        onChange={(e) => {
                          this.setState({ phenologie: e.value });
                          this.changePhenologie(e.value);
                        }}
                        checked={
                          this.state.phenologie === EnumPhenologie.PRECOCE
                        }
                      />
                      <label
                        htmlFor="phenlogie_1"
                        className="p-radiobutton-label"
                      >
                        {EnumPhenologie.PRECOCE.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="phenlogie_2"
                        disabled={!this.state.isModification}
                        value={EnumPhenologie.MOYENNE}
                        name="phenlogie"
                        onChange={(e) => {
                          this.setState({ phenologie: e.value });
                          this.changePhenologie(e.value);
                        }}
                        checked={
                          this.state.phenologie === EnumPhenologie.MOYENNE
                        }
                      />
                      <label
                        htmlFor="phenlogie_2"
                        className="p-radiobutton-label"
                      >
                        {EnumPhenologie.MOYENNE.label}
                      </label>
                    </div>
                    <div className="p-col">
                      <RadioButton
                        inputId="phenlogie_3"
                        disabled={!this.state.isModification}
                        value={EnumPhenologie.TARDIVE}
                        name="phenlogie"
                        onChange={(e) => {
                          this.setState({ phenologie: e.value });
                          this.changePhenologie(e.value);
                        }}
                        checked={
                          this.state.phenologie === EnumPhenologie.TARDIVE
                        }
                      />
                      <label
                        htmlFor="phenlogie_3"
                        className="p-radiobutton-label"
                      >
                        {EnumPhenologie.TARDIVE.label}
                      </label>
                    </div>
                  </div>
                  <br />

                  <div className="p-grid">
                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#009688" }}>
                        {this.state.parcelle.debourrement}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#009688" }}
                      >
                        filter_1
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#009688" }}
                      >
                        Débourrement
                      </span>
                      <span className="event-text"></span>
                      <div className="event-content"></div>
                    </div>

                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#E91E63" }}>
                        {this.state.parcelle.floraison}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#E91E63" }}
                      >
                        filter_2
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#E91E63" }}
                      >
                        Floraison
                      </span>
                      <span className="event-text"></span>
                    </div>

                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#9c27b0" }}>
                        {this.state.parcelle.veraison}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#9c27b0" }}
                      >
                        filter_3
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#9c27b0" }}
                      >
                        Véraison
                      </span>
                      <span className="event-text"></span>
                    </div>

                    <div className="p-col-3">
                      <span className="event-time" style={{ color: "#ff9800" }}>
                        {this.state.parcelle.recolte}
                      </span>
                      <i
                        className="material-icons"
                        style={{ color: "#ff9800" }}
                      >
                        filter_4
                      </i>
                    </div>
                    <div className="p-col-9">
                      <span
                        className="event-owner"
                        style={{ color: "#ff9800" }}
                      >
                        Récolte
                      </span>
                      <span className="event-text"></span>
                      <div className="event-content">
                        <img
                          src="/assets/layout/images/recolte_raisin.jpg"
                          alt="recolte"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {!this.state.isModification && (
                <div className="p-col-12 p-md-8 p-lg-8">
                  <div className="card">
                    <img
                      src="/assets/logos/logo_IFV.png"
                      width="45"
                      alt="avatar6"
                      className="logo_ifv_graphe"
                    />
                    <h1 className="centerText">
                      Indicateur de contrainte hydrique{" "}
                      {/*gerer avec habilitation ??? car si on change l'années on prend les parametreage courantquand memem ... qui peuvent etre different de l'an dernier... */}
                      <Dropdown
                        optionLabel="value"
                        optionValue="value"
                        value={this.state.anneeSelect}
                        options={this.state.anneesOptionsForModel}
                        onChange={(e) => {
                          this.setState({ anneeSelect: e.value });
                          this.reloadClasses(e.value);
                        }}
                      />
                    </h1>

                    {this.state.displayResetZoomPhfb && (
                      <Button
                        icon="pi pi-times"
                        className="p-button-secondary"
                        label="Reset Zoom"
                        onClick={(e) => {
                          this.resetZoom(this.refs.phfbchartLine, "phfb");
                        }}
                        style={{
                          right: "70px",
                          position: "absolute",
                          zIndex: "99",
                          fontSize: "10px",
                          marginTop: "10px",
                        }}
                      />
                    )}

                    {/* is mobile faire sa  sinon */}
                    {!this.state.displayMobile && (
                      <Chart
                        id="phfbchartLine"
                        ref="phfbchartLine"
                        type="bar"
                        data={this.state.lineChartDataPhfb}
                        options={this.phfbChartOptions}
                      />
                    )}
                    {this.state.displayMobile && (
                      <Chart
                        height="300px"
                        id="phfbchartLine"
                        ref="phfbchartLine"
                        type="bar"
                        data={this.state.lineChartDataPhfb}
                        options={this.phfbChartOptions}
                      />
                    )}

                    <br />
                    <Fieldset legend="Information règlementation">
                      Cette application est un outil de conseil à l’irrigation.
                      Les consignes qu’elle délivre sont de nature agronomique.
                      L’utilisateur est invité à s’informer de la règlementation
                      applicable à sa situation.{" "}
                    </Fieldset>
                    <br />
                  </div>
                </div>
              )}
            </div>
          </div>

          {!this.state.isModification && (
            <div className="p-col-12 p-md-6 p-lg-6">
              <div className="card">
                {!this.habilitationService.isAuthorize(
                  "ADMIN2",
                  "NIVEAU_2",
                  "NIVEAU_3"
                ) && (
                  <div>
                    <h1 className="centerText">
                      ETP <span style={{ fontSize: "11px" }}>(mm)</span>{" "}
                    </h1>
                    <div
                      style={{
                        margin: "auto",
                        textAlign: "center",
                        width: "250px",
                        marginBottom: "25px",
                      }}
                    >
                      Votre abonnement ne vous permet pas l'accès à cette donnée
                    </div>
                    <div style={{ margin: "auto", textAlign: "center" }}>
                      <Button
                        id="button_abonner"
                        className="pink-btn "
                        label="Abonnez vous"
                        icon="pi-md-add-shopping-cart"
                        onClick={() => {
                          window.location.href = config.URI_ABONNEZ_VOUS;
                        }}
                        style={{ marginBottom: "0px" }}
                      />
                    </div>
                  </div>
                )}

                {this.habilitationService.isAuthorize(
                  "NIVEAU_2",
                  "NIVEAU_3"
                ) && (
                  <div>
                    <h1 className="centerText">
                      ETP <span style={{ fontSize: "11px" }}>(mm)</span>{" "}
                      {this.state.displayResetZoomETP && (
                        <Button
                          icon="pi pi-times"
                          className="p-button-secondary"
                          label="Reset Zoom"
                          onClick={(e) => {
                            this.resetZoom(this.refs.etpChartLine, "etp");
                          }}
                          style={{
                            float: "right",
                            zIndex: "99",
                            fontSize: "10px",
                            marginTop: "10px",
                          }}
                        />
                      )}
                    </h1>

                    <Chart
                      id="etpChartLine"
                      ref="etpChartLine"
                      type="line"
                      data={this.state.lineChartDataETP}
                      options={this.etpChartOptions}
                    />
                  </div>
                )}
              </div>
            </div>
          )}

          {!this.state.isModification && (
            <div className="p-col-12 p-md-6 p-lg-6">
              <div className="card">
                {!this.habilitationService.isAuthorize(
                  "NIVEAU_2",
                  "NIVEAU_3"
                ) && (
                  <div>
                    <h1 className="centerText">
                      Température <span style={{ fontSize: "11px" }}>(°C)</span>{" "}
                    </h1>

                    <div
                      style={{
                        margin: "auto",
                        textAlign: "center",
                        width: "250px",
                        marginBottom: "25px",
                      }}
                    >
                      Votre abonnement ne vous permet pas l'accès à cette donnée
                    </div>
                    <div style={{ margin: "auto", textAlign: "center" }}>
                      <Button
                        id="button_abonner"
                        className="pink-btn "
                        label="Abonnez vous"
                        icon="pi-md-add-shopping-cart"
                        onClick={() => {
                          window.location.href = config.URI_ABONNEZ_VOUS;
                        }}
                        style={{ marginBottom: "0px" }}
                      />
                    </div>
                  </div>
                )}

                {this.habilitationService.isAuthorize(
                  "ADMIN2",
                  "NIVEAU_2",
                  "NIVEAU_3"
                ) && (
                  <div>
                    <h1 className="centerText">
                      Température <span style={{ fontSize: "11px" }}>(°C)</span>{" "}
                      {this.state.displayResetZoomTemp && (
                        <Button
                          icon="pi pi-times"
                          className="p-button-secondary"
                          label="Reset Zoom"
                          onClick={(e) => {
                            this.resetZoom(
                              this.refTempChart.current,
                              "temperature"
                            );
                          }}
                          style={{
                            float: "right",
                            zIndex: "99",
                            fontSize: "10px",
                            marginTop: "10px",
                          }}
                        />
                      )}
                    </h1>

                    <Chart
                      id="pluieChartLine"
                      ref={this.refTempChart}
                      type="bar"
                      data={this.state.lineChartDataTemp}
                      options={this.tempChartOptions}
                    />
                  </div>
                )}
              </div>
            </div>
          )}

          {!this.state.isModification &&
            this.habilitationService.isAuthorize("ROLE_RESULT_CSV_VIEWER") && (
              <div className="p-col-12 p-md-12 p-lg-12">
                <div className="card">
                  <h1 className="centerText">
                    CSV <span style={{ fontSize: "11px" }}></span>
                  </h1>
                  {this.state.csv}
                </div>
              </div>
            )}

          <IntegrationIrrigation {...this.props} />

          <RechargeHivernale {...this.props} />
        </div>

        <Dialog
          header="Confirmation de suppression !"
          footer={footerDelete}
          visible={this.state.visibleConfirmationDelete}
          style={{ width: "50vw" }}
          modal={true}
          onHide={() => this.setState({ visibleConfirmationDelete: false })}
        >
          Confirmez la suppression de la situation :{" "}
          {this.state.parcelle ? this.state.parcelle.nom : ""}
        </Dialog>
      </div>
    );
  }
}
