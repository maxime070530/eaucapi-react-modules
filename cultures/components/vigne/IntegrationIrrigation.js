import React, { Component } from "react";

import "leaflet/dist/leaflet.css";

import { Chart } from "primereact/chart";

import { isMobile } from "../../../constants/constants";

import { ParcelleService } from "../../services/ParcelleService";
import { InputText } from "primereact/inputtext";
import { ModelService } from "../../../services/ModelService";
import axios from "axios";

import { HabilitationService } from "../../../services/HabilitationService";

import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Dropdown } from "primereact/dropdown";
import { RadioButton } from "primereact/radiobutton";

import { ColumnGroup } from "primereact/columngroup";
import { Row } from "primereact/row";
import { Button } from "primereact/button";
import { RingLoader } from "react-spinners";

// Require utile pour zoom sur les charts
require("hammerjs");
require("chartjs-plugin-zoom");

const CancelToken = axios.CancelToken;

export default class IntegrationIrrigation extends Component {
  constructor() {
    super();

    this.state = {
      source: CancelToken.source(),

      loading: true,

      mapCenter: [43.6, 3.8],
      mapZoom: 9,

      lineChartDataSimulation: {
        datasets: [],
      },

      simulation: "1",
      uniteSimulation: "phfb",
      declencheSimulation: "min",
      coeffEfficaciteIrrigation: 1,
      dataTableSimulation: [],

      displayMobile: isMobile(),

      anneeSelectSimulation: new Date().getFullYear(),
      anneesOptionsForModel: [
        { value: new Date().getFullYear() },
        { value: new Date().getFullYear() - 1 },
        { value: new Date().getFullYear() - 2 },
        { value: new Date().getFullYear() - 3 },
        { value: new Date().getFullYear() - 4 },
        { value: new Date().getFullYear() - 5 },
        { value: new Date().getFullYear() - 6 },
        { value: new Date().getFullYear() - 7 },
        { value: new Date().getFullYear() - 8 },
        { value: new Date().getFullYear() - 9 },
        { value: new Date().getFullYear() - 10 },
      ],
    };

    this.loadSimulation = this.loadSimulation.bind(this);

    this.resetZoom = this.resetZoom.bind(this);

    this.parcelleService = new ParcelleService();
    this.modelService = new ModelService();
    this.habilitationService = new HabilitationService();

    this.calculTotalDose = this.calculTotalDose.bind(this);
    this.exportEcartType = this.exportEcartType.bind(this);

    this.simulationOptions = {
      title: {
        //display: true,
        display: false,
        text: "Potentiel de base",
        fontSize: 16,
      },
      legend: {
        display: true,
        position: "bottom",
        labels: {
          usePointStyle: true,
        },
      },
      hover: {
        mode: "single",
      },
      tooltips: {
        mode: "single",
        callbacks: {},
      },
      scales: {
        xAxes: [
          {
            type: "time",
            time: {
              unit: "month",
              displayFormats: {
                month: "MMM YYYY",
              },
            },
          },
        ],

        yAxes: [
          {
            id: "y-axis-left",
            display: true,
            position: "left",
            scaleLabel: {},
            ticks: {
              //fontColor: '#03A9F4',
            },
          },
          {
            id: "y-axis-right",
            display: true,
            position: "right",
            scaleLabel: {
              display: true,
              labelString: "mm",
              fontColor: "#0073cc",
              position: "top",
            },
            ticks: {
              min: 0,
              fontColor: "#0073cc",
            },
          },
        ],
      },

      plugins: {
        filler: {
          propagate: true,
        },

        zoom: {
          // Container for zoom options
          zoom: {
            // Boolean to enable zooming
            enabled: true,

            // Enable drag-to-zoom behavior
            //drag: true,
            drag: {
              borderColor: "black",
              borderWidth: 1,
              backgroundColor: "rgb(225,225,225)",
            },
            mode: "xy",

            rangeMin: {
              x: null,
              y: null,
            },
            rangeMax: {
              x: null,
              y: null,
            },

            speed: 0.1,

            onZoom: this.displayButtonResetZoomPhfb,
          },
        },
      },
    };
  }

  componentDidMount() {
    //si le role a a faire
    this.loadSimulation();

    if (this.habilitationService.isAuthorize("ETUDE_RECHARGE_HIVERNALE")) {
    }
  }

  componentWillUnmount() {}

  calculTotalDose() {
    var total = 0;

    for (let current of this.state.dataTableSimulation) {
      total += current.dose;
    }

    return total;
  }

  loadSimulation(annee) {
    if (!annee) {
      annee = this.state.anneeSelectSimulation;
    }

    this.modelService.loadSimulation(
      this.props.location.state.idParcelle,
      annee,
      this,
      this.state.source
    );

    this.modelService.loadSimulationDataTableResult(
      this.props.location.state.idParcelle,
      annee,
      this,
      this.state.source
    );
  }

  exportEcartType() {
    this.setState({ loadingExport: true });

    this.modelService.exportEcarType(
      this.props.location.state.idParcelle,
      2020,
      this,
      this.state.source
    );
  }

  resetZoom(ref, title) {
    // permet de voir le detail de l'object
    //console.log('Object: ', ref);

    var chart = ref.chart;
    chart.resetZoom();

    if (title === "phfb") {
      this.setState({ displayResetZoomPhfb: false });
    } else if (title === "etp") {
      this.setState({ displayResetZoomETP: false });
    } else if (title === "temperature") {
      this.setState({ displayResetZoomTemp: false });
    }
  }

  render() {
    let footerGroup = (
      <ColumnGroup>
        <Row>
          <Column
            footer="Total (mm):"
            colSpan={1}
            footerStyle={{ textAlign: "right" }}
          />
          <Column footer={this.calculTotalDose()} />
        </Row>
      </ColumnGroup>
    );

    return (
      <div>
        {this.state.loadingExport && (
          <div className="loader-fixed">
            <div style={{ width: "50px", margin: "0 auto" }}>
              <RingLoader
                margin={150}
                sizeUnit={"px"}
                size={50}
                color={"#123abc"}
                loading={true}
                style={{ margin: "0 auto" }}
              />
            </div>

            <div
              style={{ fontSize: "16px", margin: "0 auto", color: "#3F51B5" }}
            >
              Export en cours
            </div>
          </div>
        )}

        {!this.state.isModification &&
          this.habilitationService.isAuthorize("EXPORT_RESULT_EXCEL") && (
            <div className="p-grid">
              <div className="p-col-12 p-md-8 p-lg-8">
                <div className="card">
                  <div>
                    <h1 className="centerText">
                      Simulation {this.state.simulation}{" "}
                      {/*gerer avec habilitation ??? car si on change l'années on prend les parametreage courantquand memem ... qui peuvent etre different de l'an dernier... */}
                      <Dropdown
                        optionLabel="value"
                        optionValue="value"
                        value={this.state.anneeSelectSimulation}
                        options={this.state.anneesOptionsForModel}
                        onChange={(e) => {
                          this.setState({ anneeSelectSimulation: e.value });
                          this.loadSimulation(e.value);
                        }}
                      />
                    </h1>

                    <div style={{ margin: "auto", textAlign: "center" }}>
                      {!this.state.displayMobile && (
                        <Chart
                          id="simulation"
                          ref="refSimulationChart"
                          type="bar"
                          data={this.state.lineChartDataSimulation}
                          options={this.simulationOptions}
                        />
                      )}
                      {this.state.displayMobile && (
                        <Chart
                          height="300px"
                          id="simulation"
                          ref="refSimulationChart"
                          type="line"
                          data={this.state.lineChartDataSimulation}
                          options={this.simulationOptions}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-col-12 p-md-4 p-lg-4">
                <div className="card">
                  <div>
                    <h1 className="centerText">Paramètres</h1>

                    <div className="p-grid">
                      <div
                        className="p-col-12"
                        style={{ textAlign: "right", padding: 0 }}
                      >
                        <h4 className="centerText">Simulation</h4>
                      </div>

                      <div className="p-col-12">
                        <RadioButton
                          inputId="simulation_1"
                          value="1"
                          name="simulation"
                          onChange={(e) => {
                            this.setState({ simulation: e.value }, () =>
                              this.loadSimulation()
                            );
                          }}
                          checked={this.state.simulation === "1"}
                        />
                        <label
                          htmlFor="typecarte_mp"
                          className="p-radiobutton-label"
                        >
                          1 (Ecart pour remonter à la cible , inject dans IEFF)
                        </label>
                      </div>

                      <div className="p-col-12">
                        <RadioButton
                          inputId="simulation_2"
                          value="2"
                          name="simulation"
                          onChange={(e) => {
                            this.setState({ simulation: e.value }, () =>
                              this.loadSimulation()
                            );
                          }}
                          checked={this.state.simulation === "2"}
                        />
                        <label
                          htmlFor="typecarte_t"
                          className="p-radiobutton-label"
                        >
                          2 (Ajout 2 mm * coef dans ATSW dès qu'on est inferieur
                          à declenche)
                        </label>
                      </div>

                      <div className="p-col-12">
                        <RadioButton
                          inputId="simulation_4"
                          value="4"
                          name="simulation"
                          onChange={(e) => {
                            this.setState({ simulation: e.value }, () =>
                              this.loadSimulation()
                            );
                          }}
                          checked={this.state.simulation === "4"}
                        />
                        <label
                          htmlFor="typecarte_t"
                          className="p-radiobutton-label"
                        >
                          4 (Recalcul en ajoutant 2mm * coef / jours jusqu'à
                          atteindre la cible)
                        </label>
                      </div>
                    </div>

                    <div className="p-grid">
                      <div
                        className="p-col-12"
                        style={{ textAlign: "right", padding: 0 }}
                      >
                        <h4 className="centerText">Unitée</h4>
                      </div>

                      <div className="p-col-6 p-md-6 p-lg-6">
                        <RadioButton
                          inputId="unite_simulation_phfb"
                          value="phfb"
                          name="unite_simulation"
                          onChange={(e) => {
                            this.setState({ uniteSimulation: e.value }, () =>
                              this.loadSimulation()
                            );
                          }}
                          checked={this.state.uniteSimulation === "phfb"}
                        />
                        <label
                          htmlFor="typecarte_mp"
                          className="p-radiobutton-label"
                        >
                          Phfb
                        </label>
                      </div>

                      <div className="p-col-6 p-md-6 p-lg-6">
                        <RadioButton
                          inputId="unite_simulation_mm"
                          value="mm"
                          name="unite_simulation"
                          onChange={(e) => {
                            this.setState({ uniteSimulation: e.value }, () =>
                              this.loadSimulation()
                            );
                          }}
                          checked={this.state.uniteSimulation === "mm"}
                        />
                        <label
                          htmlFor="typecarte_t"
                          className="p-radiobutton-label"
                        >
                          mm
                        </label>
                      </div>
                    </div>

                    <div className="p-grid">
                      <div
                        className="p-col-12"
                        style={{ textAlign: "right", padding: 0 }}
                      >
                        <h4 className="centerText">Limite déclenche</h4>
                      </div>

                      <div className="p-col-6 p-md-6 p-lg-6">
                        <RadioButton
                          inputId="declenche_simulation_min"
                          value="min"
                          name="declenche_simulation"
                          onChange={(e) => {
                            this.setState(
                              { declencheSimulation: e.value },
                              () => this.loadSimulation()
                            );
                          }}
                          checked={this.state.declencheSimulation === "min"}
                        />
                        <label
                          htmlFor="typecarte_mp"
                          className="p-radiobutton-label"
                        >
                          Min
                        </label>
                      </div>

                      <div className="p-col-6 p-md-6 p-lg-6">
                        <RadioButton
                          inputId="declenche_simulation_mid"
                          value="mid"
                          name="declenche_simulation"
                          onChange={(e) => {
                            this.setState(
                              { declencheSimulation: e.value },
                              () => this.loadSimulation()
                            );
                          }}
                          checked={this.state.declencheSimulation === "mid"}
                        />
                        <label
                          htmlFor="typecarte_t"
                          className="p-radiobutton-label"
                        >
                          Mid
                        </label>
                      </div>
                    </div>

                    <div className="p-grid">
                      <div
                        className="p-col-12"
                        style={{ textAlign: "right", padding: 0 }}
                      >
                        <h4 className="centerText">
                          Coef efficacité irrigation
                        </h4>
                      </div>

                      <div className="p-col-12">
                        <InputText
                          value={this.state.coeffEfficaciteIrrigation}
                          onChange={(e) => {
                            this.setState(
                              { coeffEfficaciteIrrigation: e.target.value },
                              () => this.loadSimulation()
                            );
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="p-col-12 p-md-4 p-lg-4">
                <div className="card">
                  <div>
                    <h1 className="centerText">Résultat</h1>

                    <div style={{ margin: "auto", textAlign: "center" }}>
                      <DataTable
                        value={this.state.dataTableSimulation}
                        footerColumnGroup={footerGroup}
                      >
                        <Column field="date" header="Date"></Column>
                        <Column field="dose" header="Dose (mm)"></Column>
                      </DataTable>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}

        {!this.state.isModification &&
          this.habilitationService.isAuthorize("EXPORT_RESULT_EXCEL") && (
            <div className="p-col-12 p-md-6 p-lg-6">
              <div className="card">
                <div>
                  <h1 className="centerText">Export</h1>

                  <div style={{ margin: "auto", textAlign: "center" }}>
                    <Button
                      id="button_abonner"
                      className="pink-btn "
                      label="Export écart type"
                      icon="pi-md-file-download"
                      onClick={() => {
                        this.exportEcartType();
                      }}
                      style={{ marginBottom: "0px" }}
                    />
                  </div>
                </div>
              </div>
            </div>
          )}
      </div>
    );
  }
}
