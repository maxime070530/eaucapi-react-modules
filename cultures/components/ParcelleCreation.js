import React, { Component } from "react";

import { Growl } from "primereact/growl";
import { Fieldset } from "primereact/fieldset";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { UtilisateurService } from "../../services/UtilisateurService";
import { EnumCulture } from "../../enums/enums";

export default class ParcelleCreation extends Component {
  constructor() {
    super();

    this.state = {};

    this.redirectCreationParcelle = this.redirectCreationParcelle.bind(this);

    this.utilisateurService = new UtilisateurService();
  }

  componentDidMount() {}

  redirectCreationParcelle(culture) {
    this.utilisateurService
      .isAuthorizeAddParcelle()
      .then((res) => res.data)
      .then((responseJson) => {
        // console.log(responseJson);

        if (responseJson) {
          // retour liste des situations + message confirmation !

          this.props.history.push({
            pathname: "/situation/" + culture + "/création",
            state: { idParcelle: null, culture: culture },
          });
        } else {
          // dialog pour s'abonner
          this.setState({ visibleDialog: true });
        }
      });
  }

  render() {
    return (
      <div>
        <div style={{ width: "180px" }}></div>

        <Growl ref={(el) => (this.errorsMessages = el)} />

        <div className="p-grid">
          <div id="col_informations" className="p-col-12">
            <Fieldset
              id="fielset_information"
              legend="Informations"
              className="fieldset_tutorial"
            >
              <div className="p-grid">
                <div className="p-md-8 p-col-12">
                  <h4 style={{ marginTop: "0px" }}>
                    Ici texte poiur le peche ?
                  </h4>
                  <ul>
                    <li>JE choisit une culture</li>
                  </ul>
                </div>

                <div
                  className="p-md-4 p-col-12"
                  style={{ textAlign: "center" }}
                >
                  <a
                    href="https://www.youtube.com/watch?v=qDzs_8unkKc"
                    target="_blank"
                  >
                    <img
                      src="/assets/icons/icon_tuto.png"
                      style={{ width: "85px" }}
                      alt="tutos"
                    />
                    <br />
                    Vous pouvez aussi visionner
                    <br />
                    notre tuto en cliquant ici
                  </a>
                </div>
              </div>
            </Fieldset>

            <div className="p-grid dashboard" style={{ marginTop: "25px" }}>
              
              <div className="p-col-12 p-md-4">
                <Link
                  onClick={() => {
                    this.redirectCreationParcelle(EnumCulture.VIGNE.code.toLocaleLowerCase());
                  }}
                  class="card-effect-hover card-vignes"
                >
                  <div class="overlay"></div>
                  <div class="circle">
                    <img
                      src="/assets/cultures/recolte_raisin.jpg"
                      alt="Vignes"
                      style={{
                        width: "100%",
                        height: "100%",
                        zIndex: "1",
                        borderRadius: "50%",
                      }}
                    />
                  </div>
                  <p>Vignes</p>
                </Link>
              </div>

              <div className="p-col-12 p-md-4">
                <Link
                  class="card-effect-hover card-pechers"
                  onClick={() => {
                    this.redirectCreationParcelle(EnumCulture.PECHER.code.toLocaleLowerCase());
                  }}
                >
                  <div class="overlay"></div>
                  <div class="circle">
                    <img
                      src="/assets/cultures/pecher.jpg"
                      alt="Vignes"
                      style={{
                        width: "100%",
                        height: "100%",
                        zIndex: "1",
                        borderRadius: "50%",
                      }}
                    />
                  </div>
                  <p>Pêchers</p>
                </Link>
              </div>

              <div className="p-col-12 p-md-4">
                <a href="#" class="card-effect-hover card-others">
                  <div class="overlay"></div>
                  <div class="circle">
                    <img
                      src="/assets/cultures/others_culture.svg"
                      alt="Vignes"
                      style={{ width: "100%", height: "100%", zIndex: "1" }}
                    />
                  </div>
                  <p>Autres</p>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
