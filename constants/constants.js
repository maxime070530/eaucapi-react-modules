import L from 'leaflet';
import {config} from '../../config'


export var URIWSServeur = config.URIWSServeur; 
export var URIWSServeur_EspaceClient = config.URIWSServeur_EspaceClient;


export const MAX_MILLI_EXPIRED = 1200000; // 20 min


export const MES_PARCELLES = 'mes_parcelles';
export const TENDANCE = 'tendance';
export const PLUVIOMETRIE = 'pluviometrie';

export const TYPE_MES_PARCELLES = 'MP';
export const TYPE_TENDANCE = 'T';
export const TYPE_PLUVIOMETRIE = 'P';


export var AddParcelleIcon = L.icon({
    iconUrl: '/assets/icons/icon_marker_add_parcelle_v2.png',
    iconSize: [64, 64], // size of the icon
    iconAnchor: [32, 64]
});

export var IconClassA = L.icon({
    iconUrl: '/assets/markers/map_marker_classe_A.png',
    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [20, 40]
});

export var IconClassB = L.icon({
    iconUrl: '/assets/markers/map_marker_classe_B.png',
    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [20, 40]
});

export var IconClassC = L.icon({
    iconUrl: '/assets/markers/map_marker_classe_C.png',
    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [20, 40]
});


export var IconClassD = L.icon({
    iconUrl: '/assets/markers/map_marker_classe_D.png',
    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [20, 40]
});


export var IconClassE = L.icon({
    iconUrl: '/assets/markers/map_marker_classe_E.png',
    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [20, 40]
});



export var IconError = L.icon({
    iconUrl: '/assets/markers/map_marker_disabled.png',
    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [20, 40]
});

export var IconConsigneGreen = L.icon({
    iconUrl: '/assets/markers/map_marker_green.png',
    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [20, 40]
});

//export var IconConsigneGreen = L.icon({
//    iconUrl: 'assets/markers/round_green_check.png',
//    iconSize: [40, 40], // size of the icon
//    shadowSize: [50, 64], // size of the shadow
//    iconAnchor: [20, 40]
//});


export var IconMarkerBlue = L.icon({
    iconUrl: '/assets/markers/map_marker_blue.png',
    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [20, 40]
});


export var IconConsigneOrange = L.icon({
    iconUrl: '/assets/markers/map_marker_fourchette_moyenne.png',
    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [20, 40]
});


export var IconConsigneRed = L.icon({
    iconUrl: '/assets/markers/map_marker_fourchette_haute.png',
    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [20, 40]
});





export function isDesktop() {
    return window.innerWidth > 1024;
}

export function isMobile() {
    return window.innerWidth <= 640;
}

export function isInf750() {
    return window.innerWidth <= 750;
}

export function  isTablet() {
    let width = window.innerWidth;
    return width <= 1024 && width > 640;
}