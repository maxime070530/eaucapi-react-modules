export var EnumCulture = {
    VIGNE: {code:'VIGNE',label:'Vignes'},
    PECHER: {code:'PECHER',label:'Pêchers'}
}



export var EnumTypeVin = {
    VIN1: {code:'VIN1',label:'Vin blanc ou rosé rendement naturel'},
    VIN2: {code:'VIN2',label:'Vin blanc ou rosé rendement maîtrisé'},
    VIN3: {code:'VIN3',label:'Vin rouge rendement naturel'},
    VIN4: {code:'VIN4',label:'Vin rouge rendement maîtrisé'}
}


export var EnumTypeSol = {
    FAIBLE: {code:'FAIBLE',label:'Faible réserve en eau'},
    MOYEN: {code:'MOYEN',label:'Réserve moyenne en eau'},
    FORT: {code:'FORT',label:'Réserve en eau élevée'},
    CUSTOM : {code:'CUSTOM',label:'Réserve en eau paramétrable (mm)'},
}


export var EnumMaterielIrrigation = {
    GAG: {code:'GAG',label:'Goutte-à-Goutte'},
    AUTRE: {code:'AUTRE',label:'Autre'}   
}


export var EnumPhenologie = {
    PRECOCE: {code:'PRECOCE',label:'Précoce'},
    MOYENNE: {code:'MOYENNE',label:'Moyenne'},
    TARDIVE: {code:'TARDIVE',label:'Tardive'}
}


export var EnumConseilIrrigation = {
    INUTILE: {code:'INUTILE',label:'Aucune irrigation nécessaire', couleur:'green'},
    AVERTISSEMENT: {code:'AVERTISSEMENT',label:'Contrainte se renforçant', couleur:'yellow'},
    IRRIGATION: {code:'IRRIGATION',label:'Irrigation souhaitable', couleur:'red'},
    NAN: {code:'NAN',label:'Aucun conseil disponible', couleur:'gray'}
}


