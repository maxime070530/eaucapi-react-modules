import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { ScrollPanel } from "primereact/components/scrollpanel/ScrollPanel";

import {
  MES_PARCELLES,
  TENDANCE,
  PLUVIOMETRIE,
  TYPE_MES_PARCELLES,
  TYPE_TENDANCE,
  TYPE_PLUVIOMETRIE,
} from "../../constants/constants";
import { Button } from "primereact/button";
import { RadioButton } from "primereact/radiobutton";
import { Calendar } from "primereact/calendar";
import { EnumTypeSol, EnumPhenologie } from "../../enums/enums";
import { OverlayPanel } from "primereact/overlaypanel";
import { HabilitationService } from "../../services/HabilitationService";


export class CarteTendanceOptionsRightPanel extends Component {
  static defaultProps = {
    onContentClick: null,
    expanded: false,
  };

  static propTypes = {
    onContentClick: PropTypes.func.isRequired,
    expanded: PropTypes.bool,
  };

  constructor() {
    super();

    this.habilitationService = new HabilitationService();
  }

  componentDidMount() {
    setTimeout(() => {
      this.rightPanelMenuScroller && this.rightPanelMenuScroller.moveBar();
    }, 100);
  }

  render() {
    let className = classNames("layout-rightpanel-options-carte", {
      "layout-rightpanel-active": this.props.expanded,
    });

    const DATE_OPTIONS = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
    };

    let fr = {
      firstDayOfWeek: 1,
      dayNames: [
        "dimanche",
        "lundi",
        "mardi",
        "mercredi",
        "jeudi",
        "vendredi",
        "samedi",
      ],
      dayNamesShort: ["dim", "lun", "mar", "mer", "jeu", "ven", "dam"],
      dayNamesMin: ["DI", "LU", "MA", "ME", "JE", "VE", "SA"],
      monthNames: [
        "janvier",
        "février",
        "mars",
        "avril",
        "mai",
        "juin",
        "juillet",
        "août",
        "septembre",
        "octobre",
        "novembre",
        "décembre",
      ],
      monthNamesShort: [
        "jan",
        "fev",
        "mar",
        "avr",
        "mai",
        "juin",
        "juil",
        "août",
        "sept",
        "oct",
        "nov",
        "dec",
      ],
    };

    return (
      <div className={className} onClick={this.props.onContentClick}>
        <OverlayPanel
          ref={(el) => (this.helpCarte = el)}
          style={{ right: "10px;" }}
          className="overlaypanel_right_panel"
        >
          <p>Vous avez accès à deux types de représentation cartographiques :</p>
          <p>
            - « Mes situations » qui reprend le positionnement géographique et
            l’état hydrique des situations que vous avez créées sur votre
            exploitation, à la date sélectionnée.
          </p>
          <p>
            - « Contrainte hydrique » il s’agit de l’état de la contrainte
            hydrique calculée en tous points de la zone pour laquelle des
            données météorologiques sont disponibles, à la date sélectionnée,
            pour un type de sol à faible réserve et des cépages tardifs.
          </p>

          {this.habilitationService.isAuthorize("CARTE_PLUVIOMETRIE") &&
          <p>
            - « Pluviométrie » : il s’agit de la carte de la pluviométrie en
            tous points de la zone pour laquelle des données météorologiques
            sont disponibles, à la date sélectionnée, et sur les dernières 24 h.
          </p>
  }
         
        </OverlayPanel>

        <OverlayPanel
          ref={(el) => (this.helpTypeSol = el)}
          style={{ right: "10px;" }}
          className="overlaypanel_right_panel"
        >
          <p>
            Il s’agit de caractériser la réserve utile de votre sol, qui est la
            résultante de sa profondeur, sa texture et sa charge en éléments
            grossiers.
          </p>
          <p>
            - Faible réserve en eau ({"<"} 125 mm) : sol peu profond, chargé en
            cailloux et/ou avec texture grossière. La contrainte hydrique est
            généralement forte (défoliation marquée et rendement faible). Elle
            est présente quasiment tous les ans en l’absence d’irrigation.
          </p>
          <p>
            - Réserve en eau moyenne (125 à 175 mm) : situation intermédiaire.
          </p>
          <p>
            - Réserve en eau élevée ('{'>'}' 175 mm) sol profond, peu caillouteux,
            avec texture équilibrée. Les symptômes de stress hydrique sont peu
            fréquents même sans irrigation.
          </p>
        </OverlayPanel>

        <OverlayPanel
          ref={(el) => (this.helpPrecocite = el)}
          style={{ right: "10px;" }}
          className="overlaypanel_right_panel"
        >
          <p>
            Les gammes de précocité sont établies sur les dates moyennes de
            stades phénologiques suivantes :
          </p>

          <table>
            <thead>
              <tr>
                <th></th>
                <th>Précoce</th>
                <th>Moyen</th>
                <th>Tardif</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>Débourrement</td>
                <td>01/04</td>
                <td>01/04</td>
                <td>01/04</td>
              </tr>

              <tr>
                <td>Floraison</td>
                <td>22/05</td>
                <td>01/06</td>
                <td>08/06</td>
              </tr>
              <tr>
                <td>Véraison</td>
                <td>12/07</td>
                <td>01/08</td>
                <td>16/08</td>
              </tr>

              <tr>
                <td>Vendange</td>
                <td>15/08</td>
                <td>10/09</td>
                <td>30/09</td>
              </tr>
            </tbody>
          </table>
        </OverlayPanel>

        <ScrollPanel
          ref={(el) => (this.rightPanelMenuScroller = el)}
          style={{ height: "100%" }}
        >
          <Button
            icon="pi-md-chevron-right"
            onClick={this.props.onClickClosePanel}
            style={{ float: "right", marginTop: "5px", marginRight: "5px" }}
          ></Button>

          <div className="right-panel-scroll-content">
            <div className="layout-rightpanel-header">
              <div className="weather-day">Tendance</div>
              <div className="weather-date">
                {this.props.options.dateCarte &&
                  this.props.options.dateCarte.toLocaleDateString(
                    "fr",
                    DATE_OPTIONS
                  )}
              </div>
            </div>

            <div className="card card-w-title">
              <h1>
                Données{" "}
                <img
                  style={{
                    width: "18px",
                    height: "18px",
                    float: "right",
                    marginRight: "10px",
                    cursor: "pointer",
                  }}
                  alt="help"
                  src={"assets/icons/iconHelp.png"}
                  onClick={(e) => this.helpCarte.toggle(e)}
                />
              </h1>
              <div className="p-col">
                <RadioButton
                  inputId="typecarte_mp"
                  value={TYPE_MES_PARCELLES}
                  name="typecarte"
                  onChange={(e) =>
                    this.props.reloadMap(e.value, this.props.options.dateCarte)
                  }
                  checked={this.props.layoutSelect === MES_PARCELLES}
                />
                <label htmlFor="typecarte_mp" className="p-radiobutton-label">
                  Mes situations
                </label>
              </div>
              <div className="p-col">
                <RadioButton
                  inputId="typecarte_t"
                  value={TYPE_TENDANCE}
                  name="typecarte"
                  onChange={(e) =>
                    this.props.reloadMap(e.value, this.props.options.dateCarte)
                  }
                  checked={this.props.layoutSelect === TENDANCE}
                />
                <label htmlFor="typecarte_t" className="p-radiobutton-label">
                  Contrainte hydrique
                </label>
              </div>

              {this.habilitationService.isAuthorize("CARTE_PLUVIOMETRIE") && (
                <div className="p-col">
                  <RadioButton
                    inputId="typecarte_p"
                    value={TYPE_PLUVIOMETRIE}
                    name="typecarte"
                    onChange={(e) =>
                      this.props.reloadMap(
                        e.value,
                        this.props.options.dateCarte
                      )
                    }
                    checked={this.props.layoutSelect === PLUVIOMETRIE}
                  />
                  <label htmlFor="typecarte_p" className="p-radiobutton-label">
                    Pluviométrie
                  </label>
                </div>
              )}
            </div>

            <div className="card card-w-title">
              <div
                style={{
                  margin: "auto",
                  textAlign: "center",
                  width: "250px",
                  marginBottom: "25px",
                }}
              >
               Votre abonnement ne vous permet pas le paramétrage de la carte de contrainte hydrique.
              </div>
              <div style={{ margin: "auto", textAlign: "center" }}>
                <Button
                  id="button_abonner"
                  className="pink-btn "
                  label="Contactez-nous"
                  icon="pi-md-add-shopping-cart"
                  onClick={() => {
                    window.location.href = '/nous-contacter';
                  }}
                  style={{ marginBottom: "0px" }}
                />
              </div>
            </div>

            <div className="card card-w-title">
              <h1>Date</h1>
              <Calendar
                locale={fr}
                value={this.props.options.dateCarte}
                onChange={(e) => {
                  this.props.reloadMap(this.props.typeLayoutSelect, e.value);
                }}
                showIcon={true}
                dateFormat="dd/mm/yy"
                maxDate={new Date(new Date() - 24*3600*1000 )}
                style={{ width: "100%" }}
              />
            </div>

            {this.props.layoutSelect === TENDANCE && (
              <div>
                <div className="card card-w-title">
                  <h1>
                    Réserve du sol{" "}
                    <img
                      style={{
                        width: "18px",
                        height: "18px",
                        float: "right",
                        marginRight: "10px",
                        cursor: "pointer",
                      }}
                      alt="help"
                      src={"assets/icons/iconHelp.png"}
                      onClick={(e) => this.helpTypeSol.toggle(e)}
                    />
                  </h1>
                  <div className="p-col">
                    <RadioButton
                      inputId="reserve_sol_faible"
                      disabled={
                        !this.habilitationService.isAuthorize(
                          "CARTE_CONTRAINTE"
                        )
                      }
                      value={EnumTypeSol.FAIBLE}
                      name="reservesol"
                      onChange={(e) =>
                        this.props.reloadTendanceMap(
                          e.value,
                          this.props.options.phenologie
                        )
                      }
                      checked={
                        this.props.options.reserveSol === EnumTypeSol.FAIBLE
                      }
                    />
                    <label
                      htmlFor="reserve_sol_faible"
                      className="p-radiobutton-label"
                    >
                      {EnumTypeSol.FAIBLE.label}
                    </label>
                  </div>
                  <div className="p-col">
                    <RadioButton
                      inputId="reserve_sol_moyen"
                      value={EnumTypeSol.MOYEN}
                      name="reservesol"
                      disabled={
                        !this.habilitationService.isAuthorize(
                          "CARTE_CONTRAINTE"
                        )
                      }
                      onChange={(e) =>
                        this.props.reloadTendanceMap(
                          e.value,
                          this.props.options.phenologie
                        )
                      }
                      checked={
                        this.props.options.reserveSol === EnumTypeSol.MOYEN
                      }
                    />
                    <label
                      htmlFor="reserve_sol_moyen"
                      className="p-radiobutton-label"
                    >
                      {EnumTypeSol.MOYEN.label}
                    </label>
                  </div>
                  <div className="p-col">
                    <RadioButton
                      inputId="reserve_sol_profond"
                      disabled={
                        !this.habilitationService.isAuthorize(
                          "CARTE_CONTRAINTE"
                        )
                      }
                      value={EnumTypeSol.FORT}
                      name="reservesol"
                      onChange={(e) =>
                        this.props.reloadTendanceMap(
                          e.value,
                          this.props.options.phenologie
                        )
                      }
                      checked={
                        this.props.options.reserveSol === EnumTypeSol.FORT
                      }
                    />
                    <label
                      htmlFor="reserve_sol_profond"
                      className="p-radiobutton-label"
                    >
                      {EnumTypeSol.FORT.label}
                    </label>
                  </div>
                </div>

                <div className="card card-w-title">
                  <h1>
                    Précocité{" "}
                    <img
                      style={{
                        width: "18px",
                        height: "18px",
                        float: "right",
                        marginRight: "10px",
                        cursor: "pointer",
                      }}
                      alt="help"
                      src={"assets/icons/iconHelp.png"}
                      onClick={(e) => this.helpPrecocite.toggle(e)}
                    />
                  </h1>
                  <div className="p-col">
                    <RadioButton
                      inputId="phenologie_faible"
                      disabled={
                        !this.habilitationService.isAuthorize(
                          "CARTE_CONTRAINTE"
                        )
                      }
                      value={EnumPhenologie.PRECOCE}
                      name="phenologie"
                      onChange={(e) =>
                        this.props.reloadTendanceMap(
                          this.props.options.reserveSol,
                          e.value
                        )
                      }
                      checked={
                        this.props.options.phenologie === EnumPhenologie.PRECOCE
                      }
                    />
                    <label
                      htmlFor="phenologie_faible"
                      className="p-radiobutton-label"
                    >
                      {EnumPhenologie.PRECOCE.label}
                    </label>
                  </div>
                  <div className="p-col">
                    <RadioButton
                      inputId="phenologie_moyenne"
                      value={EnumPhenologie.MOYENNE}
                      name="phenologie"
                      disabled={
                        !this.habilitationService.isAuthorize(
                          "CARTE_CONTRAINTE"
                        )
                      }
                      onChange={(e) =>
                        this.props.reloadTendanceMap(
                          this.props.options.reserveSol,
                          e.value
                        )
                      }
                      checked={
                        this.props.options.phenologie === EnumPhenologie.MOYENNE
                      }
                    />
                    <label
                      htmlFor="phenologie_moyenne"
                      className="p-radiobutton-label"
                    >
                      {EnumPhenologie.MOYENNE.label}
                    </label>
                  </div>
                  <div className="p-col">
                    <RadioButton
                      inputId="phenologie_tardive"
                      disabled={
                        !this.habilitationService.isAuthorize(
                          "CARTE_CONTRAINTE"
                        )
                      }
                      value={EnumPhenologie.TARDIVE}
                      name="phenologie"
                      onChange={(e) =>
                        this.props.reloadTendanceMap(
                          this.props.options.reserveSol,
                          e.value
                        )
                      }
                      checked={
                        this.props.options.phenologie === EnumPhenologie.TARDIVE
                      }
                    />
                    <label
                      htmlFor="phenologie_tardive"
                      className="p-radiobutton-label"
                    >
                      {EnumPhenologie.TARDIVE.label}
                    </label>
                  </div>
                </div>
              </div>
            )}
          </div>
        </ScrollPanel>
      </div>
    );
  }
}
