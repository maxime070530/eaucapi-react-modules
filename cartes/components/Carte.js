import React, { Component } from "react";

import axios from "axios";

import {
  Map,
  TileLayer,
  LayersControl
} from "react-leaflet";
import { ReactLeafletSearch } from "react-leaflet-search";

import Control from "react-leaflet-control";
import "leaflet/dist/leaflet.css";

import { ParcelleService } from "../../cultures/services/ParcelleService";
import { ModelService } from "../../services/ModelService";
import { EnumTypeSol, EnumTypeVin, EnumPhenologie } from "../../enums/enums";
import { CarteTendanceOptionsRightPanel } from "./CarteTendanceOptionsRightPanel";

import {
  MES_PARCELLES,
  TENDANCE,
  PLUVIOMETRIE,
  TYPE_MES_PARCELLES,
  TYPE_TENDANCE,
  TYPE_PLUVIOMETRIE,
  isMobile,
} from "../../constants/constants";
import { Button } from "primereact/button";
import { RingLoader } from "react-spinners";
import { Dialog } from "primereact/dialog";
import { JournalService } from "../../services/JournalService";
import { HabilitationService } from "../../services/HabilitationService";

const CancelToken = axios.CancelToken;

export class Carte extends Component {
  constructor() {
    super();
    this.state = {
      mapCenter: [43.6, 3.8],
      zoom: 8,
      leafletContainerHeight: window.innerHeight - 235 + "px",
      typeCarteSelect: TYPE_TENDANCE,
      layoutSelect: TENDANCE,
      typeLayoutSelect: null,

      mapSurfaces: [],
      mapParcelles: [],
      visibleSideBarOptions: true,

      visibleTendanceOptionRightPanel: true,

      lastsource: CancelToken.source(),
      sources: [],
      isLoadingPixels: false,

      options: {
        typeVin: EnumTypeVin.VIN1,
        reserveSol: EnumTypeSol.FAIBLE,
        phenologie: EnumPhenologie.MOYENNE,
        dateCarte: new Date(),
      },

      visibleDialog: false,
      bodyDialog: "",
    };

    this.displayLayout = this.displayLayout.bind(this);
    this.reloadMap = this.reloadMap.bind(this);
    this.reloadTendanceMap = this.reloadTendanceMap.bind(this);

    this.onClickClosePanel = this.onClickClosePanel.bind(this);

    this.onHide = this.onHide.bind(this);

    this.parcelleService = new ParcelleService();
    this.modelService = new ModelService();
    this.journalService = new JournalService();
    this.habilitationService = new HabilitationService();
  }

  componentDidMount() {
    //this.displayLayout(MES_PARCELLES);
    //this.displayLayout(TENDANCE);

    // ici detexter si dateCarte a null on l'initialise a dern valeur selon type : mes parcelle new date, pluvio ... dern date meteo
    // rechercher date
    this.init();

    // en version mobile ne pas afficher en 1er le right panel
    if (isMobile()) {
      this.setState({ visibleTendanceOptionRightPanel: false });
    }
  }

  componentWillUnmount() {
    //alert('ici')

    //if (this.state.isLoadingPixels){
    this.state.lastsource.cancel("Operation canceled - change page.");
    this.setState({ lastsource: CancelToken.source() });
    //}
  }

  async init() {
    this.setState({ isLoadingPixels: true });

    var date = await this.journalService.searchDateDatas();

    if (this.props.location.state && this.props.location.state.tendance) {
      this.setState({ typeCarteSelect: this.props.location.state.tendance });
      this.reloadMap(this.props.location.state.tendance, date);
    } else {
      this.reloadMap(this.state.typeCarteSelect, date);
    }
  }

  reloadMap(type, date) {
    var options = this.state.options;
    options.dateCarte = date;

    this.setState({ options: options });

    if (type === TYPE_MES_PARCELLES) {
      this.setState({ typeLayoutSelect: TYPE_MES_PARCELLES });
      this.displayLayout(MES_PARCELLES, date);
    } else if (type === TYPE_TENDANCE) {
      this.setState({ typeLayoutSelect: TYPE_TENDANCE });
      this.displayLayout(TENDANCE, date);
    } else if (type === TYPE_PLUVIOMETRIE) {
      this.setState({ typeLayoutSelect: TYPE_PLUVIOMETRIE });
      this.displayLayout(PLUVIOMETRIE, date);
    }

    if (isMobile()) {
      this.setState({ visibleTendanceOptionRightPanel: false });
    }
  }

  reloadTendanceMap(typeSol, phenologie) {
    var mOptions = this.state.options;
    mOptions.reserveSol = typeSol;
    mOptions.phenologie = phenologie;
    this.setState({ options: mOptions });

    this.reloadMap(TYPE_TENDANCE, this.state.options.dateCarte);
  }

  displayLayout(layout, date) {
    this.setState({ isLoadingPixels: true });

    var map = this.refs.mapdcarte.leafletElement;
    this.setState({ layoutSelect: layout });

    if (this.state.sources) {
      this.state.sources.map((source) => {
        source.cancel("Operation canceled - change layout ref.");
      });
    }

    var source = CancelToken.source();
    this.state.sources.push(source);
    this.setState({ lastsource: source });

    // supprimer tous les markers de la carte !
    this.clearMyAllLayer(map);

    if (layout === TENDANCE) {
      this.modelService.loadCarteClasses(
        date,
        this,
        this.state.options.reserveSol,
        this.state.options.typeVin,
        this.state.options.phenologie,
        map,
        source,
        this.state.sources
      );
    }

    if (layout === MES_PARCELLES) {
      this.parcelleService.displayMesMesParcellesMap(this, date, map, source);
      // this.displayMarkersParcelles(this.state.markers, date);
    }

    if (layout === PLUVIOMETRIE) {
      this.modelService.loadCartePluviometrie(
        date,
        this,
        map,
        source,
        this.state.sources
      );
    }
  }

  clearMyAllLayer(map) {
    for (var i = 0; i < this.state.mapSurfaces.length; i++) {
      map.removeLayer(this.state.mapSurfaces[i]);
    }

    for (var i1 = 0; i1 < this.state.mapParcelles.length; i1++) {
      map.removeLayer(this.state.mapParcelles[i1]);
    }
  }

  onHide(event) {
    this.setState({ visibleDialog: false });
    this.setState({ bodyDialog: "" });
  }

  onClickClosePanel() {
    this.setState({ visibleTendanceOptionRightPanel: false });
  }

  render() {
    // const typeCarteItems = [
    //     { label: 'Mes situations', value: 'MP' },
    //     { label: 'Tendance', value: 'T' },
    //     { label: 'Pluviométrie', value: 'P' }
    // ];

    return (
      <div>
        <div className="div-center-window">
          {" "}
          <RingLoader
            margin={150}
            sizeUnit={"px"}
            size={50}
            color={"#123abc"}
            loading={this.state.isLoadingPixels}
            style={{ margin: "0 auto" }}
          />
        </div>

        {/* !this.state.visibleTendanceOptionRightPanel &&
<Button icon="pi-md-chevron-left" label="Options" onClick={() => this.setState({visibleTendanceOptionRightPanel: true})} style={{position: 'absolute', right: 0, zIndex: 1001,  marginTop: '5px',  marginRight: '5px'}}></Button> */}

        <div className="p-grid">
          <div className="p-col-12">
            <div className="card">
              <Dialog
                header="Information"
                visible={this.state.visibleDialog}
                style={{ width: "50vw" }}
                modal={true}
                onHide={this.onHide}
                closeOnEscape={true}
                dismissableMask={true}
              >
                {this.state.bodyDialog}
              </Dialog>
              {!this.habilitationService.isAuthorize("CARTE_CONTRAINTE") && this.state.layoutSelect === TENDANCE && (
                <div>
                  <h2>
                    Simulation de la contrainte hydrique pour des sols à réserve
                    faible
                    <br /> et des cépages de précocité moyenne
                  </h2>
                  <br />
                </div>
              )}
              
              <Map
                ref="mapdcarte"
                center={this.state.mapCenter}
                zoom={this.state.zoom}
                minZoom={8}
                style={{ height: this.state.leafletContainerHeight }}
              >
                <TileLayer
                  attribution='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
                />

                <Control position="topright">
                  <Button
                    icon="pi-md-chevron-left"
                    label="Ouvrir les options"
                    onClick={() =>
                      this.setState({ visibleTendanceOptionRightPanel: true })
                    }
                  ></Button>

                  {/*this.state.isLoadingPixels && <div style={{ fontSize: '16px', margin: '0 auto', color: '#3F51B5' }}>chargement <br /> {this.state.countLoadingParcelle} / {this.state.nbrParcelles}</div> */}
                </Control>

                <Control position="bottomleft">

                  
                  {this.state.layoutSelect === PLUVIOMETRIE && (
                    <div style={{ backgroundColor: "white" }}>
                      <table>
                        {/* <tr>
                                    <td><div style={{width: "15px", height: "15px",backgroundColor: '#11e2f9'  }}></div></td>
                                    <td>0</td>
                                </tr> */}
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#11e2f9",
                              }}
                            ></div>
                          </td>
                          <td>0-10 (mm)</td>
                        </tr>
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#245bfd",
                              }}
                            ></div>
                          </td>
                          <td>10-20 (mm)</td>
                        </tr>
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#36c708",
                              }}
                            ></div>
                          </td>
                          <td>20-40 (mm)</td>
                        </tr>
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#ded126",
                              }}
                            ></div>
                          </td>
                          <td>40-60 (mm)</td>
                        </tr>
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#ecb229",
                              }}
                            ></div>
                          </td>
                          <td>60-100 (mm)</td>
                        </tr>
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#e60505",
                              }}
                            ></div>
                          </td>
                          <td>'{'>'}'100 (mm)</td>
                        </tr>
                        
                      </table>
                    </div>
                  )}

                  {this.state.layoutSelect === TENDANCE && (
                    <div style={{ backgroundColor: "white" }}>
                      <table>
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#007eff94",
                              }}
                            ></div>
                          </td>
                          <td>Absence de contrainte</td>
                        </tr>
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#81e8ff94",
                              }}
                            ></div>
                          </td>
                          <td>Contrainte faible</td>
                        </tr>
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#4caf509e",
                              }}
                            ></div>
                          </td>
                          <td>Contrainte modérée</td>
                        </tr>
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#ff780094",
                              }}
                            ></div>
                          </td>
                          <td>Contrainte forte</td>
                        </tr>
                        <tr>
                          <td>
                            <div
                              style={{
                                width: "30px",
                                height: "15px",
                                backgroundColor: "#ff6f4e87",
                              }}
                            ></div>
                          </td>
                          <td>Contrainte sévère</td>
                        </tr>
                      </table>
                    </div>
                  )}
                </Control>

                <LayersControl position="topleft">
                  <ReactLeafletSearch
                    showMarker={false}
                    position="topleft"
                    inputPlaceholder="recherche"
                    popUp={this.popupSearchMarker}
                    providerOptions={{ region: "fr" }}
                    zoom={14}
                    closeResultsOnClick={true}
                  />

                  <LayersControl.BaseLayer name="OpenStreet Map" checked>
                    <TileLayer
                      attribution='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                      url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
                    />
                  </LayersControl.BaseLayer>
                  <LayersControl.BaseLayer name="ArcGIS">
                    <TileLayer
                      attribution="Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community"
                      url="http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}"
                    />
                  </LayersControl.BaseLayer>

                  <LayersControl.BaseLayer name="IGN photos aériennes">
                    <TileLayer
                      attribution="IGN-F/Géoportail"
                      url="https://wxs.ign.fr/pratique/geoportail/wmts?&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}"
                    />
                  </LayersControl.BaseLayer>
                </LayersControl>
              </Map>
              <CarteTendanceOptionsRightPanel
                expanded={this.state.visibleTendanceOptionRightPanel}
                onContentClick={this.onRightPanelClick}
                onClickClosePanel={this.onClickClosePanel}
                layoutSelect={this.state.layoutSelect}
                typeLayoutSelect={this.state.typeLayoutSelect}
                reloadMap={this.reloadMap}
                reloadTendanceMap={this.reloadTendanceMap}
                options={this.state.options}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
