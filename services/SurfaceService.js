import axios from 'axios';
import {URIWSServeur} from '../constants/constants'

export class SurfaceService {



    findSurfaceByCoordinate(location , vue){

        axios.get(URIWSServeur + '/ws/without-account/find-parcelle-coordinate?lng=' + location.lng + '&lat=' + location.lat)
        .then(response =>   {  vue.setState({ surface: response.data });} );

    }

    checkEtatSurfaceByCoordinate(location, vue){
        // check etat de la parcelle
        axios.get(URIWSServeur + '/ws/without-account/detect-etat-coordinate?lng=' + location.lng + '&lat=' + location.lat + '&typesol=' + vue.state.options.reserveSol.code + '&typevin='+ vue.state.options.typeVin.code + "&datereference=" +  vue.state.options.dateCarte.getTime())
        .then(response => response.data)
        .then(data => { vue.changeEtatSurface(data.conseil.couleur) })
        .catch(err => { vue.changeEtatSurface(null); console.log(err) });
    }


}