import React from "react";

import axios from "axios";
import { URIWSServeur } from "../constants/constants";

import L from "leaflet";


export class MeteoService {

  loadDataByLocation(location, vue) {
    vue.setState({ isLoadingData: true });
 
    axios
      .get(
        URIWSServeur +
          "/ws/meteo/data-by-coordonate?longitude=" +
          location.lng +
          "&latitude=" +
          location.lat
      )
      .then((response) => {
        if (response.status === 200) {
          vue.setState({ data: response.data });
        }
      })
      .catch((err) => {
        console.error(err.toString());
      })
      .finally(() => {
        vue.setState({ isLoadingPluviometrie: false });
      });
  }

  exportDatas(exportDataFilter, vue) {
    vue.setState({ isLoadingData: true });


    axios
      .post(
        URIWSServeur + "/ws/meteo/export-datas-by-coordonate",
        exportDataFilter,
        { responseType: "blob" }
      )
      .then((response) => {
        if (response.status === 200) {
          // fin loading ???
          
          let fileName = exportDataFilter.pasDeTemps + ".xlsx";

          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", fileName);
          document.body.appendChild(link);
          link.click();
        }

        vue.setState({ isLoading: false });
      })
      .catch((err) => {
        console.error(err.toString());
        vue.setState({ isLoading: false });
      })
      .finally(() => {});
  }

  loadDatasDisplay(exportDataFilter, map, source,   vue) {

    for (var i = 0; i < vue.state.mapSurfaces.length; i++) {
        map.removeLayer(vue.state.mapSurfaces[i]);
    }

    vue.state.mapSurfaces = [];

    axios
      .post(URIWSServeur + "/ws/meteo/datas-export-display", exportDataFilter, {
        cancelToken: source.token,
      })
      .then((response) => response.data)
      .then((data) => {
        var i;


        for (i = 0; i < data.length; i++) {
          var current = data[i];

          var coordHG = [current.coordHG.y, current.coordHG.x];
          var coordBD = [current.coordBD.y, current.coordBD.x];

          var rectangle = L.rectangle([coordHG, coordBD], {
            color: current.colorLegend,
            stroke: false,
            weight: 3,
            fillColor: current.colorLegend,
            fillOpacity: 0.6,
            opacity: 1,
          }).addTo(map);
          //.bindPopup('Pluviométrie : ' + current.pluviometrie )

         vue.state.mapSurfaces.push(rectangle);

          
        }

        vue.setState({ isLoading: false });
      })
      .catch((err) => {
        console.error(vue.props.url, err.toString());
        vue.setState({ isLoading: false });

        console.log(err.response.data);
        alert("error : " +  err.response.data.message);

        if (!axios.isCancel(err)) {
          
        }
      })
      .finally((f) => {
       
      });
  }


 
  displayLegendes(param, pasDeTemps ,  map,  vue){

   

    if (vue.state.legendControl){
        map.removeControl(vue.state.legendControl);
    }
    
    axios
      .get(
        URIWSServeur +
          "/ws/meteo/display-legend?param=" +
          param +
          "&pasDeTemps=" +
          pasDeTemps
      )
      .then((response) => {
        if (response.status === 200) {
            console.log(response.data)


           

           L.Control.Watermark = L.Control.extend({
                onAdd: function(map) {

                    var div = L.DomUtil.create('div');
                    div.style = 'background-color: white';


                    var table = L.DomUtil.create('table');
                   
                  

                    response.data.map((legend, i) => {  
                        // Return the element. Also pass key
                        var divDetail = L.DomUtil.create('div');
                        divDetail.style = 'width: 30px; height: 15px; background-color: ' + legend.couleur;

                        table.innerHTML +=  " <tr><td>" + divDetail.outerHTML  +"</td><td>" + legend.label + "</td></tr>";                       
                      
                        
                      });


                      div.innerHTML = table.outerHTML;

            
                    return div;
                },
            
                onRemove: function(map) {
                    // Nothing to do here
                }
            });
            
            L.control.watermark = function(opts) {
                return new L.Control.Watermark(opts);
            }


            var myLegendControl = L.control.watermark({ position: 'bottomleft' }).addTo(map);

            vue.setState({legendControl : myLegendControl });
                      
            
        }
      })
      .catch((err) => {
       
        console.error(err.toString());
      })
      .finally(() => {
      
      });
    }
  
}
