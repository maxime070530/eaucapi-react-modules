import axios from "axios";

import { URIWSServeur } from "../constants/constants";

export class SupervisorService {
  countUtilisateurs(vue, source) {
    axios
      .get(URIWSServeur + "/ws/supervisor/count-utilisateurs", {
        cancelToken: source.token,
      })
      .then((res) => res.data)
      .then((responseJson) => {
        //alert(JSON.stringify(responseJson))
        vue.setState({ nbrUtilisateurs: responseJson });
      });
  }

  lastDateDataMeteo(vue) {
    axios
      .get(URIWSServeur + "/ws/supervisor/last-data-meteo", {
        cancelToken: vue.state.source.token,
      })
      .then((res) => res.data)
      .then((responseJson) => {
        vue.setState({ lastDateDataMeteo: responseJson });
      });
  }

  integrateDatasManquante(vue) {

    vue.setState({ integrateEnCours: true });


    axios
      .get(URIWSServeur + "/ws/weather-mesures/insert-datas-empty/7", {
        cancelToken: vue.state.source.token,
      })
      .then((res) => res.data)
      .then((responseJson) => {
       
        this.lastDateDataMeteo(vue);
        vue.setState({ integrateEnCours: false });
        window.location = "/supervision";

      })
      .catch((error) => {
        alert(error);
        vue.setState({ integrateEnCours: false });

      });
  }

  enCoursPooling(interval, enCours) {
    if (!enCours) {
      clearInterval(interval);
    }
  }

  countSitutations(vue, source) {
    axios
      .get(URIWSServeur + "/ws/supervisor/count-situation", {
        cancelToken: source.token,
      })
      .then((res) => res.data)
      .then((responseJson) => {
        vue.setState({ nbrTotalSituation: responseJson });
      });
  }

  countConseilByconseil(vue, source) {
    axios
      .get(URIWSServeur + "/ws/supervisor/count-conseil-by-conseil", {
        cancelToken: source.token,
      })
      .then((res) => res.data)
      .then((responseJson) => {
        vue.setState({ nbrTotalAucuneIrrigation: responseJson.inutile });
        vue.setState({ nbrTotalContrainteRenfrocant: responseJson.vigilance });
        vue.setState({ nbrTotalIrrigationSouhaite: responseJson.irrigation });
        vue.setState({ nbrTotalNan: responseJson.nan });
      });
  }
}
