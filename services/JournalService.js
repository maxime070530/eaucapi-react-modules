import { axiosWithoutToken } from '../../AppWrapper';

export class JournalService {


    async searchDateDatas(){


        var date = new Date();
       // recherche date pluvio dispo si - de x jours nes date ??

      await axiosWithoutToken.get('/ws/without-account/last-date-datas-meteo-disponible' )
        .then(response => response.data)
        .then(data => {                  

// si date est > 2 jours mettre date jours
            //alert(new Date(Date.parse(data)))

            var dateMin = new Date(Date.parse(data));
            dateMin.setDate(date.getDate() - 2);
            
               date = new Date(Date.parse(data));

               if (date < dateMin){
                    date = new Date();
               }
         });
                
        return date;

    }



    async searchLastDateDatasMeteo(){


        var date = null;
       // recherche date pluvio dispo si - de x jours nes date ??

      await axiosWithoutToken.get('/ws/without-account/last-date-datas-meteo-disponible' )
        .then(response => response.data)
        .then(data => {                  
            
               date = new Date(Date.parse(data));

         });
               
     
        return date;

    }


    async searchLastDateDatasContraintes(){


        var date = null;
       // recherche date pluvio dispo si - de x jours nes date ??

      await axiosWithoutToken.get('/ws/without-account/last-date-datas-contrainte-disponible' )
        .then(response => response.data)
        .then(data => {                  
            
               date = new Date(Date.parse(data));

         });
               
     
        return date;

    }

  


}