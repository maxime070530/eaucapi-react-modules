import Axios from "axios";



export const axiosGouv = Axios.create({
	baseURL: 'https://geo.api.gouv.fr/',
	//timeout: 1000,
	//	headers: {'X-Custom-Header': 'foobar'}
});

export class GouvService {






    findMetaDataTerritoire(location , vue) {       
       
      

        axiosGouv.get('communes?lat=' + location.lat + '&lon= ' + location.lng)
            .then(response => response.data)
            .then(data => {

              


                if (data.length) {


                    var modifyZone = vue.state.zone;
                    modifyZone.locationCommuneGouv = data[0].nom;
                    vue.setState({ zone: modifyZone });

                    axiosGouv.get('regions/' + data[0].codeRegion)
                        .then(response => response.data)
                        .then(data => {

                            var modifyZone = vue.state.zone;
                            modifyZone.locationRegionGouv = data.nom;
                            vue.setState({ zone: modifyZone });


                        });


                }


            }).catch(function () {

                // 
            });
    }
}