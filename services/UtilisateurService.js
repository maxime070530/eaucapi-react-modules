import axios from "axios";
import { URIWSServeur, MAX_MILLI_EXPIRED } from "../constants/constants";
import { axiosWithoutToken } from "../../AppWrapper";
import { axiosEspaceClient } from "../../AppWrapper";

import { HabilitationService } from "./HabilitationService";
import { config } from "../../config";

export class UtilisateurService {
  constructor() {
    this.habilitationService = new HabilitationService();
  }

  async creationCompte(utilisateur, vue) {
    await axiosWithoutToken
      .post("/ws/authenticate/creation-compte", utilisateur)
      .then((res) => {
        return res.data;
      })
      .then((responseJson) => {
        if (responseJson) {
          vue.setState({ confirmCreationCompte: true });
          return true;
        }
      })
      .catch((error) => {
        if (error.response && error.response.data) {
          var errorJson = error.response.data;

          if (errorJson.severity === "error") {
            if (errorJson.fields && errorJson.fields.includes("email")) {
              vue.setState({ errorEmail: true });
            }

            vue.state.errorsMessages.show({
              severity: "error",
              summary: errorJson.message,
            });
            return false;
          } else {
            vue.state.errorsMessages.show({
              severity: "error",
              summary: "Erreur interne pendant la création du compte",
            });
            return false;
          }
        }
      })

      .finally((fin) => {
        vue.setState({ loading: false });
      });
  }

  async inscriptionCompte(inscription, vue) {
    await axiosWithoutToken
      .post("/ws/authenticate/inscription-compte", inscription)
      .then((res) => {
        return res.data;
      })
      .then((responseJson) => {
        if (responseJson) {
          vue.setState({ confirmCreationCompte: true });
          vue.setState({
            activeIndex: 2,
          });
          window.scrollTo(0, 0);

          return true;
        }
      })
      .catch((error) => {
        if (error.response && error.response.data) {
          var errorJson = error.response.data;

          if (errorJson.severity === "error") {
            if (errorJson.fields && errorJson.fields.includes("email")) {
              vue.setState({ errorEmail: true });
            }

            vue.state.errorsMessages.show({
              severity: "error",
              summary: errorJson.message,
            });
            return false;
          } else {
            vue.state.errorsMessages.show({
              severity: "error",
              summary: "Erreur interne pendant la création du compte",
            });
            return false;
          }
        }
      })

      .finally((fin) => {
        vue.setState({ loading: false });
      });
  }


  async inscriptionCompteAutres(inscription, vue) {
    await axiosWithoutToken
      .post("/ws/authenticate/inscription-compte-autres", inscription)
      .then((res) => {
        return res.data;
      })
      .then((responseJson) => {
        if (responseJson) {
          vue.setState({ confirmCreationCompte: true });
          vue.setState({
            activeIndex: 2,
          });
          window.scrollTo(0, 0);

          return true;
        }
      })
      .catch((error) => {
        if (error.response && error.response.data) {
          var errorJson = error.response.data;

          if (errorJson.severity === "error") {
            if (errorJson.fields && errorJson.fields.includes("email")) {
              vue.setState({ errorEmail: true });
            }

            vue.state.errorsMessages.show({
              severity: "error",
              summary: errorJson.message,
            });
            return false;
          } else {
            vue.state.errorsMessages.show({
              severity: "error",
              summary: "Erreur interne pendant la création du compte",
            });
            return false;
          }
        }
      })

      .finally((fin) => {
        vue.setState({ loading: false });
      });
  }

  async updateUtilisateur(utilisateur, vue) {
    await axios
      .post(URIWSServeur + "/ws/authenticate/update-utilisateur", utilisateur)
      .then((res) => {
        return res.data;
      })
      .then((responseJson) => {
        if (responseJson) {
          vue.setState({ confirmUpdate: true });
          vue.setState({ isModification: false });
          return true;
        }
      })
      .catch((error) => {
        if (error.response && error.response.data) {
          var errorJson = error.response.data;

          if (errorJson.severity === "error") {
            if (errorJson.fields && errorJson.fields.includes("email")) {
              vue.setState({ errorEmail: true });
            }

            vue.state.errorsMessages.show({
              severity: "error",
              summary: errorJson.message,
            });
            return false;
          } else {
            vue.state.errorsMessages.show({
              severity: "error",
              summary: "Erreur interne pendant la création du compte",
            });
            return false;
          }
        }
      })

      .finally((fin) => {
        vue.setState({ loading: false });
      });
  }

  async sendChannelId(idChannel) {
    await axios
      .post(URIWSServeur + "/ws/authenticate/update-id-channel", {
        id_channel: idChannel,
      })
      .then((res) => {
        return res.data;
      })
      .then((responseJson) => {})
      .catch((error) => {})

      .finally((fin) => {});
  }

  async searchUtilisateur(utilisateur, vue) {
    await axiosWithoutToken
      .get("/ws/authenticate/existe_email?email=" + utilisateur.username)
      .then((res) => res.data)
      .then((responseJson) => {
        //                alert(JSON.stringify(responseJson))

  
        if (responseJson) {
          // this.affectToken(responseJson.token.access_token);
          // window.location = "/";
          vue.setState({ displayInputPassword: true });
        } else {
          // check si existe espace client ??? non on propose de s'abonner

          vue.setState({ errorEmail: true });
          vue.state.errorsMessages.show({
            severity: "error",
            summary:
              "Adresse e-mail inconnue. Vérifiez votre saisie ou Abonnez-vous",
          });
        }
      })
      .catch((error) => {
        console.log(error);
      })

      .finally((fin) => {
        vue.setState({ loading: false });
      });
  }

  existeUtilisateur(username) {
    return axiosWithoutToken
      .get("/ws/authenticate/existe_email?email=" + username)
      .then((res) => res.data);
  }

  checkEmailEspaceClient(email) {
    return axiosEspaceClient
      .get(config.URIWSServeur_EspaceClient + "/api/authenticate/check-email/all?email=" + email)
      .then((res) => res.data)
      .catch(function (e) {
        //alert("Une erreur s'est produite");
        throw e;
      });
      
  }

  checkEmailEspaceClientEAG(email) {
    return axiosEspaceClient
      .get(config.URIWSServeur_EspaceClient + "/api/authenticate/check-email/eag?email=" + email)
      .then((res) => res.data)
      .catch(function (e) {
        //alert("Une erreur s'est produite");
        throw e;
      });
      
  }

  checkEmailAuthoriseInscription(email) {
    return axiosWithoutToken
      .get( "/ws/authenticate/autorise-email-inscription?email=" + email)
      .then((res) => res.data)
      .catch(function (e) {
        alert("Une erreur s'est produite");
        throw e;
      });
      
  }
  

  connectToEspaceClient(email, password) {
    let auth = { email: email, password: password };

    return axiosEspaceClient
      .post("/api/authenticate/connect-info", auth)
      .then((res) => res.data);
  }

  async connect(utilisateur, vue) {

    // fix si pb quand on change de compte
    localStorage.removeItem("access_token");
    localStorage.removeItem("is_authenticate");
    localStorage.removeItem("granted_authorities");
    localStorage.removeItem("consentement_analytics");

    await axiosWithoutToken
      .post("/ws/authenticate/login_ws", utilisateur)
      .then((res) => res.data)
      .then((responseJson) => {
        //                alert(JSON.stringify(responseJson))

        if (responseJson) {
          // this.affectToken(responseJson.token.access_token);
          // window.location = "/";

          this.affectTokenAndRedirect(responseJson.tokenValue);
        }
      })
      .catch((error) => {
        //alert(JSON.stringify(error))

        console.log(error);
        vue.setState({ loading: false });

        if (
          error.response &&
          error.response.data &&
          error.response.data.severity === "error"
        ) {
          vue.setState({ errorEmail: true });
          vue.setState({ errorPassword: true });
          vue.state.errorsMessages.show({
            severity: "error",
            summary: error.response.data.message,
          });
          return false;
        }
      });
  }

  async loadProfile(vue) {
    await axios
      .get(URIWSServeur + "/ws/authenticate/profile")
      .then((data) => data.data)
      .then((data) => {
        //localStorage.setItem('name', data.name);
        vue.setState({ name: data.nom + " " + data.prenom });
        vue.setState({ prenom: data.prenom });
        vue.setState({ id: data.id });

        if (data.image) {
          vue.setState({ image: data.image });
        }

        if (data.idChanel) {
          vue.setState({ id_chanel: data.idChanel });
        }
      })
      .catch((err) => {});
  }

  async affectTokenAndRedirect(token) {
    localStorage.setItem("is_authenticate", false);
    localStorage.setItem("access_token", token);
    localStorage.setItem(
      "expires_at",
      JSON.stringify(MAX_MILLI_EXPIRED + new Date().getTime())
    );

    //
    await this.loadAuthorities();

    await this.loadConsentementAnalytics();

    // ici verifier si bien valide le token
    await this.checkAuthenticate();
    window.location = "/";

    return true;
  }

  affectToken(token) {
    localStorage.setItem("is_authenticate", false);
    localStorage.setItem("access_token", token);
    localStorage.setItem(
      "expires_at",
      JSON.stringify(MAX_MILLI_EXPIRED + new Date().getTime())
    );

    // ici verifier si bien valide le token
    this.checkAuthenticate();

    return true;
  }

  affectGrant(grantedAuthorities) {
    //alert(JSON.stringify(grantedAuthorities))

    var roles = [];

    grantedAuthorities.map((value, idx) => {
      //alert(JSON.stringify(value))
      roles.push(value.authority);

      return value;
    });

    //alert(roles)
    localStorage.setItem("granted_authorities", roles);
  }

  async loadAuthorities() {
    await axios
      .get(URIWSServeur + "/ws/authenticate/authorities")
      .then((data) => data.data)
      .then((data) => {
        //alert(JSON.stringify(data))
        this.affectGrant(data);
      });
  }


  async loadConsentementAnalytics() {
    await axios
      .get(URIWSServeur + "/ws/authenticate/is-analytics")
      .then((data) => data.data)
      .then((data) => {
       
        if (data){
          localStorage.setItem("consentement_analytics", true);
        }else{
          localStorage.setItem("consentement_analytics", false);
        }

      });
  }

  async checkAuthenticate() {
    await axios
      .get(URIWSServeur + "/ws/authenticate/is-authenticate")
      .then((data) => data.data)
      .then((data) => {
        localStorage.setItem("is_authenticate", true);
        localStorage.setItem(
          "expires_at",
          JSON.stringify(MAX_MILLI_EXPIRED + new Date().getTime())
        );
      })
      .catch((err) => {
        // alert(JSON.stringify(err))
        // ici maintenance !
        //alert('error ' + JSON.stringify(err))
        localStorage.setItem("acces_token", false);
        localStorage.removeItem("acces_token");
        window.location = "/login";
      });
  }

  checkValidateAuthentication() {
    let expiresAt = JSON.parse(localStorage.getItem("expires_at"));

    if (new Date().getTime() > expiresAt) {
      //alert('expired')
      window.location = "/login";
      return false;
    }

    if (!localStorage.getItem("is_authenticate")) {
      //alert('non authenticate')
      window.location = "/login";
      return false;
    }

    return true;
  }

  getUtilisateurs() {
    return axios.get(URIWSServeur + "/ws/authenticate/all-utilisateurs");
  }

  loadUtilisateur(idUtilisateur, vue, source) {
    axios
      .get(
        URIWSServeur +
          "/ws/authenticate/utilisateur?idUtilisateur=" +
          idUtilisateur,
        { cancelToken: source.token }
      )
      .then((data) => data.data)
      .then((data) => {
        
        vue.setState({ utilisateur: data });
        vue.setState({ selectedRoles: data.roles });

        this.checkEmailEspaceClient(data.email).then((responseJson) => {
          if (responseJson.exist === "true") {
            vue.setState({ buttonSendAccessible: false });
          } else {
            vue.setState({ buttonSendAccessible: true });
          }
        });
      });
  }

  loadAllRoles(vue, source) {
    axios
      .get(URIWSServeur + "/ws/authenticate/all-roles", {
        cancelToken: source.token,
      })
      .then((data) => data.data)
      .then((data) => {
        vue.setState({ listesAllRoles: data });
      });
  }

  isAuthorizeAddParcelle() {
    // console.log(roles);

    return axios.get(
      URIWSServeur + "/ws/authenticate/is-authorize-add-parcelle"
    );
  }

  initNbrSituation(vue) {
    axios
      .get(URIWSServeur + "/ws/authenticate/nbr-situation-total", {
        cancelToken: vue.state.source.token,
      })
      .then((res) => res.data)
      .then((responseJson) => {
        if (vue) {
          vue.setState({ nbrTotalSituation: responseJson });
        }

        localStorage.setItem("nbr_situation_global", responseJson);
      });

    axios
      .get(URIWSServeur + "/ws/authenticate/nbr-situation", {
        cancelToken: vue.state.source.token,
      })
      .then((res) => res.data)
      .then((responseJson) => {
        vue.setState({ nbrSituation: responseJson });
      });
  }

  initNbrSituationGlobaleLocalStorage() {
    axios
      .get(URIWSServeur + "/ws/authenticate/nbr-situation-total")
      .then((res) => res.data)
      .then((responseJson) => {
        localStorage.setItem("nbr_situation_global", responseJson);
      });
  }

  sendMDPForUtilisateur(control, auth, vue) {
    vue.setState({ loading: true });

    axiosWithoutToken
      .post("/ws/authenticate/update-password?control=" + control, auth)
      .then((res) => res.data)
      .then((responseJson) => {
        if (responseJson) {
          //alert(responseJson.access_token)
          window.location = "/callback-oauth2/" + responseJson.tokenValue;
        }
      })
      .catch((responseError) => {
        vue.state.errorsMessages.show({
          severity: "error",
          summary: "Une erreur s'est produite ",
        });
      })
      .finally((f) => {
        vue.setState({ loading: false });
      });
  }

  verificationConfirmationCompte(controlvalue, vue) {
    vue.setState({ loading: true });
  
    let controljson = {control : controlvalue}

    axiosWithoutToken
      .post("/ws/authenticate/verification-confirmation-compte", controljson)
      .then((res) => res.data)
      .then((responseJson) => {
        if (responseJson) {
          //alert(responseJson.tokenValue)
          window.location = "/callback-oauth2/" + responseJson.tokenValue;
        }
      })
      .catch((error) => {
        
       
        if (
          error.response &&
          error.response.data &&
          error.response.data.severity === "error"
        ) {         
          vue.state.errorsMessages.show({
            severity: "error",
            summary: error.response.data.message,
          });

          vue.setState({messagedisplay : error.response.data.message});

        }else{

          vue.state.errorsMessages.show({
            severity: "error",
            summary: "Une erreur s'est produite ",
          });
          vue.setState({messagedisplay : 'Une erreur s\'est produite, veuillez nous contacter'});

        }



      })
      .finally((f) => {
        vue.setState({ loading: false });
        // afficher message une erreur ses produite
       
      });
  }

  generateEmailLinkMDP(vue, idUtilisateur) {
    axios
      .get(
        URIWSServeur +
          "/ws/authenticate/regenerate-mail-link-mdp?idUtilisateur=" +
          idUtilisateur
      )
      .then((data) => data.data)
      .then((data) => {
        vue.state.errorsMessages.show({
          severity: "info",
          summary: "message envoyé",
        });
        return false;
      })
      .catch((error) => {
        // alert(JSON.stringify(error))

        if (error.response && error.response.data) {
          var errorJson = error.response.data;

          if (errorJson.severity === "error") {
            vue.state.errorsMessages.show({
              severity: "error",
              summary: errorJson.message,
            });
            return false;
          } else {
            vue.state.errorsMessages.show({
              severity: "error",
              summary: "Erreur interne pendant la création du compte",
            });
            return false;
          }
        }
      });
  }

  loadInfoUtilisateur(vue, source) {
    axios
      .get(URIWSServeur + "/ws/authenticate/info-utilisateur", {
        cancelToken: source.token,
      })
      .then((data) => data.data)
      .then((data) => {
        vue.setState({ utilisateur: data });
      });
  }



  updateConsentementRGPD(type , value, source) {


    let body = {'value' : value};

    axios
      .put(URIWSServeur + "/ws/authenticate/update-rgpd/" + type , body  ,{
        cancelToken: source.token,
      })
      .then((data) => data.data)
      .then((data) => {
       
      });
  }

  logout(state) {
    // ws/authenticate/logout
    axios
      .get(URIWSServeur + "/ws/authenticate/logout")
      .then((data) => {
        localStorage.removeItem("is_authenticate");
        localStorage.removeItem("access_token");
        localStorage.removeItem("granted_authorities");
        localStorage.removeItem("consentement_analytics");
        

        state.props.history.push("/login");
      })
      .catch((err) => false);

    //logout
  }
}
