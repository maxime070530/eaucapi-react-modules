import React from "react";

import axios from "axios";
import { URIWSServeur }  from "../constants/constants";
import { axiosWithoutToken } from "../../AppWrapper";

import L from "leaflet";
import { JournalService } from "./JournalService";
import { EnumConseilIrrigation } from "../enums/enums"; 

export class ModelService {
  constructor() {
    this.journalService = new JournalService();
  }

  loadPhfbByLocation(location, options, vue) {
    vue.setState({ lineChartDataPhfb: { datasets: [] } });
    vue.setState({ isLoadingPhfb: true });

    axiosWithoutToken
      .get(
        "/ws/without-account/phfb-by-parcelle?lng=" +
          location.lng +
          "&lat=" +
          location.lat +
          "&typesol=" +
          options.reserveSol.code +
          "&typevin=" +
          options.typeVin.code
      )
      .then((response) => response.data)
      .then((data) => {
        var datasets = [];
        datasets.push({
          type: "line",
          label: "Potentiel de base",
          data: data,
          fill: false,
          pointRadius: 0,
          backgroundColor: "#0073cc", //#03A9F4
        });

        vue.setState({ lineChartDataPhfb: { datasets } });

        vue.refPhfbChart.current.refresh();
      })
      .catch((err) => {
        console.error(err.toString());
        vue.refPhfbChart.current.refresh();
      })
      .finally(() => {
        vue.setState({ isLoadingPhfb: false });
      });
  }

  loadPluviometrieByLocation(location, vue) {
    vue.setState({ lineChartDataPluie: { datasets: [] } });
    vue.setState({ isLoadingPluviometrie: true });

    axiosWithoutToken
      .get(
        "/ws/without-account/data-pluviometries-by-location?lng=" +
          location.lng +
          "&lat=" +
          location.lat
      )
      .then((response) => response.data)
      .then((data) => {
        var datasets = [];
        datasets.push({
          type: "bar",
          label: "Pluviométrie",
          data: data,
          fill: false,
          pointRadius: 0,
          backgroundColor: "#0073cc", //#03A9F4
        });

        vue.setState({ lineChartDataPluie: { datasets } });

        vue.refPluieChart.current.refresh();
      })
      .catch((err) => {
        console.error(err.toString());
        vue.refPluieChart.current.refresh();
      })
      .finally(() => {
        vue.setState({ isLoadingPluviometrie: false });
      });
  }

  loadCarteClasses(
    date,
    vue,
    typeSol,
    typeVin,
    phenologie,
    map,
    source,
    sources
  ) {
    vue.setState({ isLoadingPixels: true });
    map.setView([43.37, 4.55], 8);

    axios
      .get(
        URIWSServeur +
          "/ws/bhr/bhr-date?date=" +
          date.getTime() +
          "&typesol=" +
          typeSol.code +
          "&typevin=" +
          typeVin.code +
          "&phenologie=" +
          phenologie.code,
        { cancelToken: source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        //console.log(data)
        // console.log(data.coordHG)

        if (data && data.length > 0) {
          var i;
          for (i = 0; i < data.length; i++) {
            var current = data[i];

            var coordHG = [current.coordHG.y, current.coordHG.x];
            var coordBD = [current.coordBD.y, current.coordBD.x];

            var rectangle = L.rectangle([coordHG, coordBD], {
              color: current.classe.couleurPolygon,
              stroke: false,
              fillOpacity: 0.8,
            }).addTo(map);
            //.bindPopup(current.classe.libelle + ' <br />  Potentiel Hydrique : ' + current.potentielHydrique + " : HG " + current.coordHG.y + " " + current.coordHG.x + " , " + "BD " + current.coordBD.y + " " + current.coordBD.x)

            vue.state.mapSurfaces.push(rectangle);
          }
        } else {
          this.journalService
            .searchLastDateDatasContraintes()
            .then((data) => {
              //alert('ici')

              const bodyMessage = (
                <div>
                  Aucune donnée de contrainte disponible pour cette date,
                  veuillez sélectionner une autre date.
                  <br />
                  <span>
                    (Dernière date disponible : le{" "}
                    {new Date(Date.parse(data)).toLocaleDateString("fr")} )
                  </span>
                </div>
              );

              vue.setState({ visibleDialog: true });
              vue.setState({ bodyDialog: bodyMessage });
            })
            .catch((err) => {
              const bodyMessage = (
                <div>
                  Aucune donnée de contrainte disponible pour cette date,
                  veuillez sélectionner une autre date.
                </div>
              );

              vue.setState({ visibleDialog: true });
              vue.setState({ bodyDialog: bodyMessage });
            });
        }
      })
      .catch((err) => console.error(vue.props.url, err.toString()))
      .finally((f) => {
        //vue.setState({ isLoadingPixels: false });

        sources.splice(1, sources.indexOf(source));

        if (sources && sources.length === 1) {
          vue.setState({ isLoadingPixels: false });
        }
      });
  }

  loadCarteClassesWithoutAccount(
    date,
    vue,
    typeSol,
    typeVin,
    phenologie,
    map,
    source
  ) {
    vue.setState({ isLoadingPixels: true });

    axiosWithoutToken
      .get(
        URIWSServeur +
          "/ws/without-account/bhr-date?date=" +
          date.getTime() +
          "&typesol=" +
          typeSol.code +
          "&typevin=" +
          typeVin.code +
          "&phenologie=" +
          phenologie.code,
        { cancelToken: source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        //console.log(data)
        // console.log(data.coordHG)

        // fusionner dale ?

        if (data && data.length > 0) {
          var i;
          for (i = 0; i < data.length; i++) {
            var current = data[i];

            var coordHG = [current.coordHG.y, current.coordHG.x];
            var coordBD = [current.coordBD.y, current.coordBD.x];

            var color = current.classe.couleur;

            var j;
            //data.length
            for (j = i; j < data.length; j++) {
              if (
                data[j].classe.couleur === color &&
                j !== data.length - 1 &&
                coordHG[0] === data[j].coordHG.y
              ) {
                coordBD = [data[j].coordBD.y, data[j].coordBD.x];
                i++;
              } else {
                var rectangle = L.rectangle([coordHG, coordBD], {
                  color: color,
                  stroke: false,
                }).addTo(map);
                // pas de popup : gerer quand click appel ws pour refaire calcul et afficher le potention
                //.bindPopup(current.classe.libelle + ' <br />  Potentiel Hydrique : ' + current.potentielHydrique + " : HG " + current.coordHG.y + " " + current.coordHG.x + " , " + "BD " + current.coordBD.y + " " + current.coordBD.x)

                vue.state.mapSurfaces.push(rectangle);

                break;
              }
            }
          }
        }

        // data.map(current => {

        //     // voir pour pop up information a mettre ?
        //     var rectangle = L.rectangle([[current.coordHG.y, current.coordHG.x], [current.coordBD.y, current.coordBD.x]], { color: current.classe.couleur, stroke: false })
        //         .addTo(map)
        //         // pas de popup : gerer quand click appel ws pour refaire calcul et afficher le potention
        //         //.bindPopup(current.classe.libelle + ' <br />  Potentiel Hydrique : ' + current.potentielHydrique + " : HG " + current.coordHG.y + " " + current.coordHG.x + " , " + "BD " + current.coordBD.y + " " + current.coordBD.x)

        //         vue.state.mapSurfaces.push(rectangle);

        // });
      })
      .catch((err) => console.error(vue.props.url, err.toString()))
      .finally((f) => {
        vue.setState({ isLoadingPixels: false });
      });
  }

  loadCartePluviometrie(date, vue, map, source, sources) {
    vue.setState({ isLoadingPixels: true });

    //vue.setState({ mapCenter: [43.6, 3.8] });
    //vue.setState({ zoom: 7 });
    map.setView([43.37, 4.55], 8);

    axios
      .get(URIWSServeur + "/ws/bhr/pluviometrie?date=" + date.getTime(), {
        cancelToken: source.token,
      })
      .then((response) => response.data)
      .then((data) => {
        var i;
        for (i = 0; i < data.length; i++) {
          var current = data[i];

          var coordHG = [current.coordHG.y, current.coordHG.x];
          var coordBD = [current.coordBD.y, current.coordBD.x];

          var couleur = this.determineColor(current);

          var rectangle = L.rectangle([coordHG, coordBD], {
            color: couleur,
            stroke: false,
            fillOpacity: 0.6,
          }).addTo(map);
          //.bindPopup('Pluviométrie : ' + current.pluviometrie )

          vue.state.mapSurfaces.push(rectangle);
        }
      })
      .catch((err) => {
        console.error(vue.props.url, err.toString());

        if (!axios.isCancel(err)) {
          this.journalService
            .searchLastDateDatasMeteo()
            .then((data) => {
              //alert('ici')

              const bodyMessage = (
                <div>
                  Aucune donnée météo disponible pour cette date, veuillez
                  sélectionner une autre date.
                  <br />
                  <span>
                    (Dernière date disponible : le{" "}
                    {new Date(Date.parse(data)).toLocaleDateString("fr")} )
                  </span>
                </div>
              );

              vue.setState({ visibleDialog: true });
              vue.setState({ bodyDialog: bodyMessage });
            })
            .catch((err) => {
              const bodyMessage = (
                <div>
                  Aucune donnée météo disponible pour cette date, veuillez
                  sélectionner une autre date.
                </div>
              );

              vue.setState({ visibleDialog: true });
              vue.setState({ bodyDialog: bodyMessage });
            });
        }
      })
      .finally((f) => {
        //enfet ici detecter si pas ailleur loading

        sources.splice(1, sources.indexOf(source));

        if (sources && sources.length === 1) {
          vue.setState({ isLoadingPixels: false });
        }
      });
  }

  determineColor(data) {
    // if (data.pluviometrie == 0 ){
    //     return '#000000'   // rgb(0,0,0)
    // }
    //     else

    //mettre couleur en rgb ?
    if (data.pluviometrie > 0 && data.pluviometrie < 10) {
      return "#11e2f9";
    } else if (data.pluviometrie >= 10 && data.pluviometrie < 20) {
      return "#245bfd";
    } else if (data.pluviometrie >= 20 && data.pluviometrie < 40) {
      return "#36c708";
    } else if (data.pluviometrie >= 40 && data.pluviometrie < 60) {
      return "#ded126";
    } else if (data.pluviometrie >= 60 && data.pluviometrie < 100) {
      return "#ecb229";
    } else if (data.pluviometrie >= 100) {
      return "#e60505";
    }
  }

  loadPluviometrie(idParcelle, vue, datasChart, source) {
    axios
      .get(
        URIWSServeur +
          "/ws/meteo/data-by-parcelle-param?idParcelle=" +
          idParcelle +
          "&param=PLUIE",
        { cancelToken: source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        // alert(JSON.stringify(data))

        datasChart.datasets.push({
          type: "bar",
          label: "Pluie",
          data: data,
          fill: false,
          pointRadius: 0,
          backgroundColor: "#0073cc", //#03A9F4
        });
        vue.refPluieChart.current.refresh();
      })
      .catch((err) => console.error(err.toString()));
  }

  loadTemp(idParcelle, vue, source) {
    axios
      .get(
        URIWSServeur +
          "/ws/meteo/data-by-parcelle-param?idParcelle=" +
          idParcelle +
          "&param=TEMP",
        { cancelToken: source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        // alert(JSON.stringify(data)).

        var datasChart = {
          datasets: [],
        };

        datasChart.datasets.push({
          type: "bar",
          label: "Température",
          data: data,
          fill: false,
          pointRadius: 0,
          backgroundColor: "#0073cc", //#03A9F4
        });

        vue.setState({ lineChartDataTemp: datasChart });
        vue.refTempChart.current.refresh();
      })
      .catch((err) => console.error(err.toString()));
  }

  loadSimulation(idParcelle, annee, vue, source) {
    axios
      .get(
        URIWSServeur +
          "/ws/model/simulation-result?idParcelle=" +
          idParcelle +
          "&annee=" +
          annee +
          "&simulation=" +
          vue.state.simulation +
          "&coef_eff_irrigation=" +
          vue.state.coeffEfficaciteIrrigation +
          "&declenche=" +
          vue.state.declencheSimulation,
        { cancelToken: source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        var datasChart = {
          datasets: [],
        };

        // min #ffb3a1

        if (vue.state.uniteSimulation === "phfb") {
          // simulation 1
          datasChart.datasets.push({
            type: "line",
            label: "Simulation",
            pointStyle: "line",
            data: data.PHFBNew,
            fill: false,
            pointRadius: 0,
            borderColor: "#03A9F4", //#03A9F4,
            yAxisID: "y-axis-left",
          });

          // fct declenche irri
          datasChart.datasets.push({
            type: "line",
            label: "Déclenche",
            pointStyle: "line",
            data: data.PHFB_Declenche,
            fill: false,
            pointRadius: 0,
            borderColor: "#f9c585", //#03A9F4
            yAxisID: "y-axis-left",
          });

          // fct cible
          datasChart.datasets.push({
            type: "line",
            label: "Cible",
            pointStyle: "line",
            data: data.PHFB_Cible,
            fill: false,
            pointRadius: 0,
            borderColor: "#77c27a", //#03A9F4
            yAxisID: "y-axis-left",
          });

          // fct min
          datasChart.datasets.push({
            type: "line",
            label: "Min",
            pointStyle: "line",
            data: data.PHFB_Min,
            fill: false,
            pointRadius: 0,
            borderColor: "#c7c7c794", //#03A9F4
            yAxisID: "y-axis-left",
          });

          // fct max
          datasChart.datasets.push({
            type: "line",
            label: "Max",
            pointStyle: "line",
            data: data.PHFB_Max,
            fill: false,
            pointRadius: 0,
            borderColor: "#c7c7c794", //#03A9F4
            yAxisID: "y-axis-left",
          });
        } else {
          // simulation 1
          datasChart.datasets.push({
            type: "line",
            label: "Simulation",
            pointStyle: "line",
            data: data.PHFBNew_mm,
            fill: false,
            pointRadius: 0,
            borderColor: "#03A9F4", //#03A9F4,
            yAxisID: "y-axis-left",
          });

          // fct declenche irri
          datasChart.datasets.push({
            type: "line",
            label: "Déclenche",
            pointStyle: "line",
            data: data.PHFB_Declenche_mm,
            fill: false,
            pointRadius: 0,
            borderColor: "#f9c585", //#03A9F4
            yAxisID: "y-axis-left",
          });

          // fct cible
          datasChart.datasets.push({
            type: "line",
            label: "Cible",
            pointStyle: "line",
            data: data.PHFB_Cible_mm,
            fill: false,
            pointRadius: 0,
            borderColor: "#77c27a", //#03A9F4
            yAxisID: "y-axis-left",
          });

          // fct min
          datasChart.datasets.push({
            type: "line",
            label: "Min",
            pointStyle: "line",
            data: data.PHFB_Min_mm,
            fill: false,
            pointRadius: 0,
            borderColor: "#c7c7c794", //#03A9F4
            yAxisID: "y-axis-left",
          });

          // fct max
          datasChart.datasets.push({
            type: "line",
            label: "Max",
            pointStyle: "line",
            data: data.PHFB_Max_mm,
            fill: false,
            pointRadius: 0,
            borderColor: "#c7c7c794", //#03A9F4
            yAxisID: "y-axis-left",
          });
        }

        // fct PLUIE
        datasChart.datasets.push({
          type: "bar",
          label: "Pluie",
          pointStyle: "rect",
          data: data.PLUIE,
          fill: false,
          pointRadius: 0,
          backgroundColor: "#7db8e56e", //#03A9F4: "#03A9F4", //#03A9F4
          yAxisID: "y-axis-right",
        });

        vue.setState({ lineChartDataSimulation: datasChart });

        //vue.refSimulationChart.refresh();
      })
      .catch((err) => console.error(err.toString()));
  }

  loadSimulationDataTableResult(idParcelle, annee, vue, source) {
    axios
      .get(
        URIWSServeur +
          "/ws/model/simulation-result-tableau?idParcelle=" +
          idParcelle +
          "&annee=" +
          annee +
          "&simulation=" +
          vue.state.simulation +
          "&coef_eff_irrigation=" +
          vue.state.coeffEfficaciteIrrigation +
          "&declenche=" +
          vue.state.declencheSimulation,
        { cancelToken: source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        vue.setState({ dataTableSimulation: data });
      });
  }

  async loadEtudeHivernate(idParcelle, annee, vue, source) {
    var _lineChartData = {
      datasets: [],
    };

    await axios
      .get(
        URIWSServeur +
          "/ws/model/tendance-optimal-phfb-by-parcelle?idParcelle=" +
          idParcelle +
          "&annee=" +
          annee,
        { cancelToken: source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        // recreation des series avec date ?

        var dataphfbMax = [];
        var dataphfbMin = [];
        var dataphfbWarning = [];

        for (var i = 0; i < data.Date.length; i++) {
          dataphfbMax.push({ x: data.Date[i], y: data.PHFB_Max[i] });
          dataphfbMin.push({ x: data.Date[i], y: data.PHFB_Min[i] });
          dataphfbWarning.push({ x: data.Date[i], y: data.PHFB_Warning[i] });
        }

       
        _lineChartData.datasets.push({
          type: "line",
          label: "Contrainte hydrique forte",
          data: dataphfbMin,
          //borderColor: '#ff6f4e87', //data.colorPhfbMin,
          borderColor: "rgba(255, 111, 78, 0.5294117647058824)", //data.colorPhfbMin,
          borderWidth: 1,
          pointRadius: 0,
          //fill:"-1"
          fill: "start",
          //fill: "none",
          //backgroundColor: '#ff6f4e87', //data.colorPhfbMin,
          backgroundColor: "rgba(255, 111, 78, 0.5294117647058824)", //data.colorPhfbMin,
          tooltip: false,
          yAxisID: "y-axis-left",
        });

        _lineChartData.datasets.push({
          type: "line",
          label: "Contrainte hydrique  se renforçant",
          data: dataphfbMin,
          //borderColor: 'orange', //data.colorPhfbMax,
          borderColor: "rgba(245, 152, 37, 0.56)", //data.colorPhfbMax,
          borderWidth: 1,
          pointRadius: 0,
          fill: "+1",
          //fill: "none",
          //backgroundColor: 'orange', //data.colorPhfbMax,
          backgroundColor: "rgba(245, 152, 37, 0.56)", //data.colorPhfbMax,
          tooltip: false,
          yAxisID: "y-axis-left",
        });

        _lineChartData.datasets.push({
          type: "line",
          label: "Contrainte hydrique optimale",
          data: dataphfbWarning,
          //borderColor: '#4caf50c2', //data.colorPhfbMax,
          borderColor: "rgba(76, 175, 80, 0.7607843137254902)", //data.colorPhfbMax,
          borderWidth: 1,
          pointRadius: 0,
          fill: "+1",
          //fill: "none",
          //backgroundColor: '#4caf50c2', //data.colorPhfbMax,
          backgroundColor: "rgba(76, 175, 80, 0.7607843137254902)", //data.colorPhfbMax,
          tooltip: false,
          yAxisID: "y-axis-left",
        });

        _lineChartData.datasets.push({
          type: "line",
          label: "Excès eau",
          data: dataphfbMax,
          //borderColor: '#007eff94', //data.colorPhfbMax,
          borderColor: "rgba(0, 126, 255, 0.5803921568627451)", //data.colorPhfbMax,
          borderWidth: 1,
          pointRadius: 0,
          fill: "end",
          //fill: "none",
          //backgroundColor: '#007eff94', //data.colorPhfbMax,
          backgroundColor: "rgba(0, 126, 255, 0.5803921568627451)", //data.colorPhfbMax,
          tooltip: false,
          yAxisID: "y-axis-left",
        });
        
      })
      .catch((err) => console.error( err.toString()));



      axios
      .get(
        URIWSServeur +
          "/ws/model/etude-hivernale-result?idParcelle=" +
          idParcelle +
          "&annee=" +
          annee +
          (vue.state.reserveUtile1Avril ? ("&reserveUtile1Avril=" +  vue.state.reserveUtile1Avril) : "")
          ,
        { cancelToken: source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        
        // min #ffb3a1

  
          // etude
          _lineChartData.datasets.unshift({
            type: "line",
            label: "Etude",
            pointStyle: "line",
            data: data.PHFBNew,
            fill: false,
            pointRadius: 0,
            borderColor: "#03A9F4", //#03A9F4,
            yAxisID: "y-axis-left",
          });

          console.log(_lineChartData)
          vue.setState({ lineChartDataPhfbEtudeHivernale: _lineChartData });
          
         

        })
        .catch((err) => console.error( err.toString()));      
    
    
  }

  loadDatesChangeConseil(idParcelle, annee, vue, source) {
    axios
      .get(
        URIWSServeur +
          "/ws/model/date-change-conseil-result-tableau?idParcelle=" +
          idParcelle +
          "&annee=" +
          annee +
          (vue.state.reserveUtile1Avril ? ("&reserveUtile1Avril=" +  vue.state.reserveUtile1Avril) : "") 
          + "&seuil=" +
          EnumConseilIrrigation.IRRIGATION.code,
        { cancelToken: source.token }
      )
      .then((response) => response.data)
      .then((data) => {
        vue.setState({ dataTableDatesChangeSeuil: data });
      });
  }

  exportEcarType(idParcelle, year, vue, source) {
    axios
      .get(
        URIWSServeur +
          "/ws/model/export-datas-ecart-type-ATSW?idParcelle=" +
          idParcelle +
          "&year=" +
          year,
        { cancelToken: source.token, responseType: "blob" }
      )
      .then((response) => {
        if (response.status === 200) {
          let fileName = "export_ecart_type.xlsx";

          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", fileName);
          document.body.appendChild(link);
          link.click();
        }

        vue.setState({ loadingExport: false });
      })

      .catch((err) => {
        console.error(err.toString());
        vue.setState({ loadingExport: false });
      });
  }


  exportResultatRechargeHivernale(idParcelle, year, vue, source) {
      axios
        .get(
          URIWSServeur +
            "/ws/model/export-resultat-recharge-hivernale?idParcelle=" +
            idParcelle +
            "&year=" +
            year +  (vue.state.reserveUtile1Avril ? ("&reserveUtile1Avril=" +  vue.state.reserveUtile1Avril) : ""),
          { cancelToken: source.token, responseType: "blob" }
        )
        .then((response) => {
          if (response.status === 200) {
            let fileName = "resultat_recharge_hivernale.xlsx";
  
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement("a");
            link.href = url;
            link.setAttribute("download", fileName);
            document.body.appendChild(link);
            link.click();
          }
  
          vue.setState({ loadingExport: false });
        })
  
        .catch((err) => {
          console.error(err.toString());
          vue.setState({ loadingExport: false });
        });
    
  }

  exportResultModel(idParcelle, year, vue, source) {
    axios
      .get(
        URIWSServeur +
          "/ws/model/export-datas-result-model?idParcelle=" +
          idParcelle +
          "&year=" +
          year,
        { cancelToken: source.token, responseType: "blob" }
      )
      .then((response) => {
        if (response.status === 200) {
          let fileName = "export_ecart_type.xlsx";

          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", fileName);
          document.body.appendChild(link);
          link.click();
        }

        vue.setState({ loadingExport: false });
      })

      .catch((err) => {
        console.error(err.toString());
        vue.setState({ loadingExport: false });
      });
  }
}
