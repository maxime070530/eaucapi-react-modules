import axios from "axios";

import { URIWSServeur } from "../constants/constants";

export class FormulaireService {
  sendFormulaire(vue, withoutauth) {
    var formulaire = { objet: vue.state.objet, message: vue.state.message };

    if (withoutauth) {
      formulaire.nom = vue.state.nom;
      formulaire.prenom = vue.state.prenom;
      formulaire.email = vue.state.email;
      formulaire.telephone = vue.state.telephone;
    }

    axios
      .post(URIWSServeur + "/ws/formulaire/send-formulaire", formulaire)
      .then((response) => response.data)
      .then((responseJson) => {
        // message succes
        vue.state.errorsMessages.show({
          severity: "success",
          summary: "Votre message a bien été envoyé : ",
          detail: "il sera traité dans les meilleurs délais",
        });

        if (withoutauth) {
          vue.setState({ nom: "" });
          vue.setState({ prenom: "" });
          vue.setState({ email: "" });
          vue.setState({ telephone: "" });
        }
        vue.setState({ objet: "" });
        vue.setState({ message: "" });

        vue.setState({ errorNom: "" });
        vue.setState({ errorPrenom: "" });
        vue.setState({ errorEmail: "" });
        vue.setState({ errorTelephone: "" });

        vue.setState({ errorObjet: "" });
        vue.setState({ errorMessage: "" });
      })
      .catch(() => {
        vue.state.errorsMessages.show({
          severity: "error",
          summary: "Une erreur s'est produite : ",
          detail: "veuillez réessayer ultérieurement",
        });
      })

      .finally(() => {
        vue.setState({ loading: false });
      });
  }
}
