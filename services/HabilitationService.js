export class HabilitationService {
  isAuthorize(...roles) {
   // console.log(roles);

    if (localStorage.getItem("granted_authorities")) {
      for (let i = 0; i < roles.length; i++) {
        if (localStorage.getItem("granted_authorities").includes(roles[i])){
            return true;
        }
      }
    }

    return false;
  }
 

}
