import React, { Component } from "react";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";

import { RingLoader } from "react-spinners";
import { UtilisateurService } from "../../services/UtilisateurService";

import { Redirect } from 'react-router-dom'
import { HabilitationService } from "../../services/HabilitationService";

export class Utilisateurs extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,

      dataTableValue: [],
    };

    this.utilisateurService = new UtilisateurService();
    this.habilitationService = new HabilitationService();
 

    this.actionTemplate = this.actionTemplate.bind(this);
    this.actifTemplate = this.actifTemplate.bind(this);

    this.consulDetailUtilisateur = this.consulDetailUtilisateur.bind(this);
    this.redirectCreationUtilisateur = this.redirectCreationUtilisateur.bind(
      this
    );
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    this.utilisateurService.getUtilisateurs().then((data) => {
      this.setState({ dataTableValue: data.data });
      this.setState({ isLoading: false });
    });
  }

  actionTemplate(rowData, column) {
    return (
      <div>
        <Button
          type="button"
          icon="pi pi-search"
          className="p-button-success"
          onClick={(e) => this.consulDetailUtilisateur(rowData.id)}
        ></Button>
      </div>
    );
  }

  actifTemplate(rowData, column) {
    return (
      <div>
        {rowData.actif && "Actif"}
        {!rowData.actif && "Désactivé"}
      </div>
    );
  }

  consulDetailUtilisateur(idUtilisateur) {
    this.props.history.push({
      pathname: "/utilisateur",
      state: { idUtilisateur: idUtilisateur },
    });
  }

  redirectCreationUtilisateur() {
    this.props.history.push({
      pathname: "/utilisateur/création",
      state: { idUtilisateur: null },
    });
  }

  render() {

    if (!this.habilitationService.isAuthorize("ADMIN")) {
      return <Redirect to="/" />;
    }

    let loading = (
      <div>
        <div style={{ width: "50px", margin: "0 auto" }}>
          <RingLoader
            margin={150}
            sizeUnit={"px"}
            size={50}
            color={"#123abc"}
            loading={true}
            style={{ margin: "0 auto" }}
          />
        </div>
      </div>
    );

    return (
      <div>
        <div className="p-grid">
          <div className="p-col-12">
            <div className="card card-w-title">
              <h1>
                Liste des utilisateurs{" "}
                <Button
                  icon="pi-md-add"
                  className="pink-btn"
                  tooltip="Ajouter un utilisateur"
                  onClick={this.redirectCreationUtilisateur}
                  style={{ fontSize: "0.5em", marginLeft: "10px" }}
                />
              </h1>

              <DataTable
                ref={(el) => (this.dt = el)}
                value={this.state.dataTableValue}
                paginatorPosition="top"
                paginator={true}
                rows={10}
                emptyMessage={
                  this.state.isLoading ? loading : "Aucun utilisateur"
                }
                responsive={true}
              >
                <Column field="nom" header="Nom" sortable={true} />
                <Column field="prenom" header="Prénom" sortable={true} />
                <Column field="email" header="Email" sortable={true} />
                <Column body={this.actifTemplate} header="Actif" />
                <Column
                  body={this.actionTemplate}
                  style={{ textAlign: "center", width: "8em" }}
                />
              </DataTable>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
