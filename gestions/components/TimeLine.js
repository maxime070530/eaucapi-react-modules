import React, {Component} from 'react';


export class TimeLine extends Component {
   

    render() {

     
        return  <React.Fragment>


                <div className="p-col-3">
                                <span className="event-time"> {this.props.eventDate} <br/><span style={{fontSize: '11px' , color: 'gray'}}>{this.props.eventTime}</span></span>
                                <i className="material-icons" style={{ color: this.props.color }}>{this.props.icon}</i>
                            </div>
                            <div className="p-col-9">
                                <span className="event-owner" style={{ color: this.props.color }}>{this.props.libelle}</span>
                                <span className="event-text">{this.props.description}</span>

                                {this.props.image&&  <div className="event-content">
                                    <img src={this.props.image} alt="md" />                                 
                                </div>
                                }
                            </div>
                            </React.Fragment>
                
    }
}