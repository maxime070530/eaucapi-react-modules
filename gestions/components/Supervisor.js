import React, { Component } from "react";

import axios from "axios";

import { Button } from "primereact/components/button/Button";
import { Dropdown } from "primereact/components/dropdown/Dropdown";
import { DataTable } from "primereact/components/datatable/DataTable";
import { Column } from "primereact/column";
import { ColumnGroup } from "primereact/columngroup";
import { Row } from "primereact/row";

import { Dialog } from "primereact/dialog";

import { Redirect } from 'react-router-dom'

import {
  EnumConseilIrrigation,
  EnumTypeVin,
  EnumTypeSol,
  EnumPhenologie,
} from "../../enums/enums";

import { Map, TileLayer, LayersControl } from "react-leaflet";

import Control from "react-leaflet-control";
import "leaflet/dist/leaflet.css";

import { ParcelleService } from "../../cultures/services/ParcelleService";
import { RingLoader } from "react-spinners";
import { TimeLine } from "./TimeLine";
import { EvenementService } from "../../services/EvenementService";

import { UtilisateurService } from "../../services/UtilisateurService";
import { SupervisorService } from "../../services/SupervisorService";
import { HabilitationService } from "../../services/HabilitationService";

const CancelToken = axios.CancelToken;

export class Supervisor extends Component {
  constructor() {
    super();
    this.state = {
      selectedUtilisateur: null,
      utilisateursItems: [],

      nbrUtilisateurs: "...",
      lastDateDataMeteo: "...",
      nbrTotalSituation: "...",
      nbrTotalNan: "...",

      nbrTotalAucuneIrrigation: "...",
      nbrTotalContrainteRenfrocant: "...",
      nbrTotalIrrigationSouhaite: "...",

      mapCenter: [43.6, 3.8],
      mapParcelles: [],
      loadingParcelles: false,
      countLoadingParcelle: 0,
      mapDatasJournalieresParcelle: [],
      isErrorLoading: false,
      visibleDetailDialogMarker: false,

      integrateEnCours: false,

      source: CancelToken.source(),

      slack: {
        username: "",
        token: "",
      },

      evenements: [],

      mailMessage: "Votre message : ",

      nbrError: 0,
      //nbrAucuneIrrigation: 0,
      //nbrVigilanceIrrigation:0,
      // nbrIrrigation:0,
      //
      // nbrParcelles:0,

      filterVin: null,
      filterSol: null,
      filterPrecocite: null,
      filterEtat: null,

      chartData: {
        labels: [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
        ],
        datasets: [
          {
            label: "First Dataset",
            data: [65, 59, 80, 81, 56, 55, 40],
            fill: false,
            borderColor: "#4bc0c0",
          },
          {
            label: "Second Dataset",
            data: [28, 48, 40, 19, 86, 27, 90],
            fill: false,
            borderColor: "#565656",
          },
        ],
      },
    };

    this.onClickMarker = this.onClickMarker.bind(this);
    this.onClickPolygon = this.onClickPolygon.bind(this);

    this.integrateDatasManquante = this.integrateDatasManquante.bind(this);

    this.consulDetailParcelle = this.consulDetailParcelle.bind(this);
    this.redirectCarteMesParcelles = this.redirectCarteMesParcelles.bind(this);
    this.changeZoomMap = this.changeZoomMap.bind(this);

    this.actionTemplate = this.actionTemplate.bind(this);
    this.displaySmiley = this.displaySmiley.bind(this);
    this.changeSelectUtilisateur = this.changeSelectUtilisateur.bind(this);

    this.onTypeVinFilterChange = this.onTypeVinFilterChange.bind(this);
    this.onTypeSolFilterChange = this.onTypeSolFilterChange.bind(this);
    this.onPrecociteFilterChange = this.onPrecociteFilterChange.bind(this);
    this.onEtatFilterChange = this.onEtatFilterChange.bind(this);

    this.parcelleService = new ParcelleService();
    this.evenementService = new EvenementService();
    this.utilisateurService = new UtilisateurService();
    this.supervisorService = new SupervisorService();
    this.habilitationService = new HabilitationService();
  }

  onClickMarker(e) {
    this.setState({ visibleDetailDialogMarker: true });
    this.setState({ parcelleMarkerSelect: e.target.options.parcelle });
  }

  onClickPolygon(e) {
    this.setState({ visibleDetailDialogMarker: true });
    this.setState({ parcelleMarkerSelect: e.target.options.parcelle });
  }

  displayMarkersParcelles(idUtilisateur) {
    var map = this.refs.mapsupervisor.leafletElement;

    // creer marker ici plus porpre que set icon fait un affichage bizare quand change

    this.setState({ loadingParcelles: true });
    this.setState({ countLoadingParcelle: 0 });

    this.parcelleService.displayDashboardMesMesParcellesMap(
      this,
      map,
      this.state.source,
      idUtilisateur
    );

    this.evenementService.findAllEvenement(
      this,
      this.state.evenements.length + 10
    );
  }

  componentDidMount() {
    //  this.displayMarkersParcelles();

    this.utilisateurService.getUtilisateurs().then((data) => {
      const utilisateurItems = [];

      data.data.map((value, idx) => {
        //alert(JSON.stringify(value))
        utilisateurItems.push({
          label: value.name + " - " + value.email,
          value: value,
        });
        return value;
      });

      this.setState({ utilisateursItems: utilisateurItems });
      this.setState({ isLoading: false });
    });

    this.supervisorService.countUtilisateurs(this, this.state.source);
    this.supervisorService.lastDateDataMeteo(this, this.state.source);
    this.supervisorService.countSitutations(this, this.state.source);
    this.supervisorService.countConseilByconseil(this, this.state.source);
  }

  componentDidUpdate() {}

  componentWillUnmount() {
    if (this.state.loadingParcelles) {
      this.state.source.cancel("Operation canceled - change page.");
      this.setState({ source: CancelToken.source() });
    }
  }

  changeZoomMap() {
    if (this.refs.mapsupervisor) {
      //    var map = this.refs.mapsupervisor.leafletElement;
      // alert(map.getZoom());
    }
  }

  consulDetailParcelle(idParcelle) {
    if (idParcelle) {
      this.props.history.push({
        pathname: "/situation",
        state: { idParcelle: idParcelle },
      });
    } else {
      this.props.history.push({
        pathname: "/situation",
        state: { idParcelle: this.state.parcelleMarkerSelect.id },
      });
    }
  }

  redirectCarteMesParcelles() {
    this.props.history.push({
      pathname: "/carte",
      state: { tendance: "MP" },
    });
  }

  integrateDatasManquante() {
    this.supervisorService.integrateDatasManquante(this);
  }

  actionTemplate(rowData, column) {
    return (
      <div>
        <Button
          type="button"
          icon="pi pi-search"
          className="p-button-success"
          onClick={(e) => this.consulDetailParcelle(rowData.parcelle.id)}
        ></Button>
      </div>
    );
  }

  onTypeVinFilterChange(event) {
    if (event.value && event.value.label) {
      this.dt.filter(event.value.label, "parcelle.typeVin.label", "equals");
    } else {
      this.dt.filter(event.value, "parcelle.typeVin.label", "equals");
    }

    this.setState({ filterVin: event.value });
  }

  onTypeSolFilterChange(event) {
    if (event.value && event.value.label) {
      this.dt.filter(event.value.label, "parcelle.typeSol.label", "equals");
    } else {
      this.dt.filter(event.value, "parcelle.typeSol.label", "equals");
    }

    this.setState({ filterSol: event.value });
  }

  onPrecociteFilterChange(event) {
    if (event.value && event.value.label) {
      this.dt.filter(event.value.label, "parcelle.phenologie.label", "equals");
    } else {
      this.dt.filter(event.value, "parcelle.phenologie.label", "equals");
    }

    this.setState({ filterPrecocite: event.value });
  }

  onEtatFilterChange(event) {
    if (event.value && event.value.label) {
      this.dt.filter(event.value.label, "data.conseil.libelle", "equals");
    } else {
      this.dt.filter(event.value, "data.conseil.libelle", "equals");
    }

    this.setState({ filterEtat: event.value });
  }

  sendToServer(payload, success, error) {
    return fetch("/api/slack", {
      method: "POST",
      body: JSON.stringify(payload),
    })
      .then(success)
      .catch(error);
  }

  uploadImage(image, success, error) {
    var form = new FormData();
    form.append("image", image);

    return fetch("/api/upload", { method: "POST", data: form })
      .then(({ url }) => success(url))
      .catch((err) => error(err));
  }

  displaySmiley(rowData, column) {
    return (
      <div>
        <div>
          {/* <i className="material-icons">insert_emoticon</i> */}
          {rowData.data.conseil.libelle}
        </div>
      </div>
    );
  }

  changeSelectUtilisateur(event) {
    this.clearMyAllLayer();
    this.setState({ selectedUtilisateur: event.value });
    this.displayMarkersParcelles(event.value.id);
  }

  clearMyAllLayer() {
    var map = this.refs.mapsupervisor.leafletElement;

    if (this.state.mapSurfaces) {
      for (var i = 0; i < this.state.mapSurfaces.length; i++) {
        map.removeLayer(this.state.mapSurfaces[i]);
      }
    }

    if (this.state.mapParcelles) {
      for (var i1 = 0; i1 < this.state.mapParcelles.length; i1++) {
        map.removeLayer(this.state.mapParcelles[i1]);
      }
    }
  }

  render() {

    if (!this.habilitationService.isAuthorize('SUPERVISOR')){

      return <Redirect to="/" />
    }


    let loadingDatatable = (
      <div>
        <div style={{ width: "50px", margin: "0 auto" }}>
          <RingLoader
            margin={150}
            sizeUnit={"px"}
            size={50}
            color={"#123abc"}
            loading={true}
            style={{ margin: "0 auto" }}
          />
        </div>
      </div>
    );

    let typeVin = [
      { label: "Tous types de vin", value: null },
      { label: EnumTypeVin.VIN1.label, value: EnumTypeVin.VIN1 },
      { label: EnumTypeVin.VIN2.label, value: EnumTypeVin.VIN2 },
      { label: EnumTypeVin.VIN3.label, value: EnumTypeVin.VIN3 },
      { label: EnumTypeVin.VIN4.label, value: EnumTypeVin.VIN4 },
    ];

    let typeVinFilter = (
      <Dropdown
        style={{ width: "100%" }}
        value={this.state.filterVin}
        options={typeVin}
        onChange={this.onTypeVinFilterChange}
      />
    );

    let typeSol = [
      { label: "Tous types de sol", value: null },
      { label: EnumTypeSol.FAIBLE.label, value: EnumTypeSol.FAIBLE },
      { label: EnumTypeSol.MOYEN.label, value: EnumTypeSol.MOYEN },
      { label: EnumTypeSol.FORT.label, value: EnumTypeSol.FORT },
    ];

    let typeSolFilter = (
      <Dropdown
        style={{ width: "100%" }}
        value={this.state.filterSol}
        options={typeSol}
        onChange={this.onTypeSolFilterChange}
      />
    );

    let precocite = [
      { label: "Toutes précocités", value: null },
      { label: EnumPhenologie.PRECOCE.label, value: EnumPhenologie.PRECOCE },
      { label: EnumPhenologie.MOYENNE.label, value: EnumPhenologie.MOYENNE },
      { label: EnumPhenologie.TARDIVE.label, value: EnumPhenologie.TARDIVE },
    ];

    let precociteFilter = (
      <Dropdown
        style={{ width: "100%" }}
        value={this.state.filterPrecocite}
        options={precocite}
        onChange={this.onPrecociteFilterChange}
      />
    );

    let etat = [
      { label: "Tous les états", value: null },
      {
        label: EnumConseilIrrigation.INUTILE.label,
        value: EnumConseilIrrigation.INUTILE,
      },
      {
        label: EnumConseilIrrigation.AVERTISSEMENT.label,
        value: EnumConseilIrrigation.AVERTISSEMENT,
      },
      {
        label: EnumConseilIrrigation.IRRIGATION.label,
        value: EnumConseilIrrigation.IRRIGATION,
      },
      {
        label: EnumConseilIrrigation.NAN.label,
        value: EnumConseilIrrigation.NAN,
      },
    ];

    let etatFilter = (
      <Dropdown
        style={{ width: "100%" }}
        value={this.state.filterEtat}
        options={etat}
        onChange={this.onEtatFilterChange}
      />
    );

    let headerGroup = (
      <ColumnGroup>
        <Row>
          <Column
            header="Mes situations"
            rowSpan={1}
            colSpan={4}
            style={{ backgroundColor: "#3F51B5", color: "white" }}
          />
          <Column
            header="Conseil irrigation"
            rowSpan={1}
            colSpan={1}
            style={{ backgroundColor: "#3F51B5", color: "white" }}
          />
          <Column header="Consulter" rowSpan={2} style={{ width: "90px" }} />
        </Row>
        <Row>
          <Column header="Nom" field="parcelle.nom" sortable={true} />
          <Column filter={true} filterElement={typeVinFilter} />
          <Column filter={true} filterElement={typeSolFilter} />
          <Column filter={true} filterElement={precociteFilter} />

          <Column filter={true} filterElement={etatFilter} />
        </Row>
      </ColumnGroup>
    );

    this.state.evenements.map(
      (item, key) => (
        //   {alert(JSON.stringify(item)); return (
        <TimeLine
          eventDate={item.dateFormat}
          eventTime={item.heureFormat}
          color={item.type.color}
          libelle={item.libelle}
          description={item.description}
          image={item.image}
          icon={item.type.icon}
        ></TimeLine>
      )
      //   )}
    );

    // if (this.state.isErrorLoading) { return <Redirect to="/maintenance" />; }

    return (
      <div>
        <Dialog
          header={
            this.state.parcelleMarkerSelect
              ? this.state.parcelleMarkerSelect.nom
              : ""
          }
          visible={this.state.visibleDetailDialogMarker}
          modal={true}
          dismissableMask={true}
          onHide={(e) => this.setState({ visibleDetailDialogMarker: false })}
        >
          {this.state.parcelleMarkerSelect && (
            <div>
              <div className="p-grid">
                <div className="p-col"> Type de vin </div>
                <div className="p-col">
                  {this.state.parcelleMarkerSelect.typeVin.label}
                </div>
              </div>

              <div className="p-grid">
                <div className="p-col"> Type de sol </div>
                <div className="p-col">
                  {this.state.parcelleMarkerSelect.typeSol.label}
                </div>
              </div>

              <div className="p-grid">
                <div className="p-col"> Précocité </div>
                <div className="p-col">
                  {this.state.parcelleMarkerSelect.phenologie.label}
                </div>
              </div>

              <br />
              <br />
              <div className="p-grid">
                <div className="p-col">
                  {" "}
                  <Button
                    label="Détail de ma situation"
                    icon="pi-md-search"
                    style={{ width: "100%" }}
                    onClick={(e) => {
                      this.consulDetailParcelle();
                    }}
                  />{" "}
                </div>
              </div>
            </div>
          )}
        </Dialog>

        <div className="p-grid dashboard">
          <div className="p-col-12 p-md-4">
            <div className="card overview">
              <div className="overview-content clearfix">
                <span className="overview-title">Utilisateurs</span>
                <span className="overview-badge"></span>
                <span className="overview-detail">
                  {this.state.nbrUtilisateurs}
                </span>
              </div>
              <div className="overview-footer">
                <img
                  src="/assets/layout/images/dashboard/icons_utilisateurs.svg"
                  style={{
                    width: "20%",
                    float: "right",
                    marginRight: "5px",
                    marginBottom: "0px",
                  }}
                  alt="meteos"
                />
              </div>
            </div>
          </div>
          <div className="p-col-12 p-md-4">
            <div className="card overview">
              <div className="overview-content clearfix">
                <span className="overview-title">Dernière données météo</span>
                <span className="overview-badge"></span>
                <span className="overview-detail">
                  {this.state.lastDateDataMeteo}
                </span>
              </div>
              <div className="overview-footer">
                {true && (
                  <Button
                    label="intégré données manquante"
                    className="orange-btn "
                    onClick={() => this.integrateDatasManquante()}
                    style={{ marginLeft: "15px", marginTop: "25px" }}
                  />
                )}

                <div className="loader">
                  <div style={{ width: "50px", margin: "0 auto" }}>
                    <RingLoader
                      margin={150}
                      sizeUnit={"px"}
                      size={50}
                      color={"#123abc"}
                      loading={this.state.integrateEnCours}
                      style={{ margin: "0 auto" }}
                    />
                  </div>
                </div>

                <img
                  src="/assets/layout/images/dashboard/icons_data_meteos.svg"
                  style={{
                    width: "15%",
                    float: "right",
                    marginRight: "15px",
                    marginBottom: "15px",
                  }}
                  alt="meteos"
                />
              </div>
            </div>
          </div>
          <div className="p-col-12 p-md-4">
            <div className="card overview">
              <div className="overview-content clearfix">
                <span className="overview-title">Situations</span>
                <span className="overview-badge"></span>
                <span className="overview-detail">
                  {this.state.nbrTotalSituation}
                </span>
              </div>
              <div className="overview-footer">
                <img
                  src="/assets/layout/images/dashboard/icons_situations.svg"
                  style={{
                    width: "15%",
                    float: "right",
                    marginRight: "15px",
                    marginBottom: "15px",
                  }}
                  alt="Progress"
                />
              </div>
            </div>
          </div>

          <div className="p-col-12 p-md-6 p-lg-3">
            <div className="p-grid card colorbox colorbox-1">
              <div className="p-col-4">
                <i className="material-icons">insert_emoticon</i>
              </div>
              <div className="p-col-8">
                <span className="colorbox-name">
                  {EnumConseilIrrigation.INUTILE.label}
                </span>
                <span className="colorbox-count">
                  {this.state.nbrTotalAucuneIrrigation}
                </span>
              </div>
            </div>
          </div>
          <div className="p-col-12 p-md-6 p-lg-3">
            <div className="p-grid card colorbox colorbox-yellow">
              <div className="p-col-4">
                <i className="material-icons">sentiment_dissatisfied</i>
              </div>
              <div className="p-col-8">
                <span className="colorbox-name">
                  {EnumConseilIrrigation.AVERTISSEMENT.label}
                </span>
                <span className="colorbox-count">
                  {this.state.nbrTotalContrainteRenfrocant}
                </span>
              </div>
            </div>
          </div>
          <div className="p-col-12 p-md-6 p-lg-3">
            <div className="p-grid card colorbox colorbox-red">
              <div className="p-col-4">
                <i className="material-icons">mood_bad</i>
              </div>
              <div className="p-col-8">
                <span className="colorbox-name">
                  {EnumConseilIrrigation.IRRIGATION.label}
                </span>
                <span className="colorbox-count">
                  {this.state.nbrTotalIrrigationSouhaite}
                </span>
              </div>
            </div>
          </div>
          <div className="p-col-12 p-md-6 p-lg-3">
            <div className="p-grid card colorbox colorbox-gray">
              <div className="p-col-4">
                <i className="material-icons">warning</i>
              </div>
              <div className="p-col-8">
                <span className="colorbox-name">
                  {EnumConseilIrrigation.NAN.label}
                </span>
                <span className="colorbox-count">{this.state.nbrTotalNan}</span>
              </div>
            </div>
          </div>

          <div
            className="p-col-12 p-md-12 p-lg-12"
            style={{ textAlign: "center" }}
          >
            <br />
            <br />
            <Dropdown
              value={this.state.selectedUtilisateur}
              style={{ width: "50%" }}
              className="select-one-item-center"
              options={this.state.utilisateursItems}
              onChange={(e) => {
                this.changeSelectUtilisateur(e);
              }}
              placeholder="Selectionner un utilisateur"
            />
            <br />
          </div>
        </div>

        {this.state.selectedUtilisateur && (
          <div className="p-grid dashboard">
            <div className="p-col-12 p-md-6 p-lg-4">
              <div className="p-grid card colorbox colorbox-1">
                <div className="p-col-4">
                  <i className="material-icons">insert_emoticon</i>
                </div>
                <div className="p-col-8">
                  <span className="colorbox-name">
                    {EnumConseilIrrigation.INUTILE.label}
                  </span>
                  <span className="colorbox-count">
                    {this.state.nbrAucuneIrrigation}
                  </span>
                  <span className="colorbox-count colorbox-max">
                    /{this.state.nbrParcelles}
                  </span>
                </div>
              </div>
            </div>
            <div className="p-col-12 p-md-6 p-lg-4">
              <div className="p-grid card colorbox colorbox-yellow">
                <div className="p-col-4">
                  <i className="material-icons">sentiment_dissatisfied</i>
                </div>
                <div className="p-col-8">
                  <span className="colorbox-name">
                    {EnumConseilIrrigation.AVERTISSEMENT.label}
                  </span>
                  <span className="colorbox-count">
                    {this.state.nbrVigilanceIrrigation}
                  </span>
                  <span className="colorbox-count colorbox-max">
                    /{this.state.nbrParcelles}
                  </span>
                </div>
              </div>
            </div>
            <div className="p-col-12 p-md-6 p-lg-4">
              <div className="p-grid card colorbox colorbox-red">
                <div className="p-col-4">
                  <i className="material-icons">mood_bad</i>
                </div>
                <div className="p-col-8">
                  <span className="colorbox-name">
                    {EnumConseilIrrigation.IRRIGATION.label}
                  </span>
                  <span className="colorbox-count">
                    {this.state.nbrIrrigation}
                  </span>
                  <span className="colorbox-count colorbox-max">
                    /{this.state.nbrParcelles}
                  </span>
                </div>
              </div>
            </div>

            {this.state.nbrError > 0 && (
              <div className="p-col-12 p-md-12 p-lg-12 ">
                <div className="p-grid card colorbox colorbox-gray">
                  <div className="p-col-4">
                    <i className="material-icons">warning</i>
                  </div>
                  <div className="p-col-8">
                    <span className="colorbox-name">
                      {EnumConseilIrrigation.NAN.label}
                    </span>
                    <span className="colorbox-count">
                      {this.state.nbrError}
                    </span>
                    <span className="colorbox-count colorbox-max">
                      /{this.state.nbrParcelles}
                    </span>
                  </div>
                </div>
              </div>
            )}
          </div>
        )}

        <div className="p-col-12 p-md-12 p-lg-12 ">
          <Map
            ref="mapsupervisor"
            center={this.state.mapCenter}
            zoom={11}
            minZoom={6}
            onZoomEnd={this.changeZoomMap()}
            style={{ height: "350px" }}
          >
            <TileLayer
              attribution='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
            />

            <div className="loader">
              <div style={{ width: "50px", margin: "0 auto" }}>
                <RingLoader
                  margin={150}
                  sizeUnit={"px"}
                  size={50}
                  color={"#123abc"}
                  loading={this.state.loadingParcelles}
                  style={{ margin: "0 auto" }}
                />
              </div>

              {this.state.loadingParcelles && (
                <div
                  style={{
                    fontSize: "16px",
                    margin: "0 auto",
                    color: "#3F51B5",
                  }}
                >
                  chargement <br /> {this.state.countLoadingParcelle} /{" "}
                  {this.state.nbrParcelles}
                </div>
              )}
            </div>

            <Control position="topright">
              <Button
                icon="pi-md-fullscreen"
                onClick={() => this.redirectCarteMesParcelles()}
                style={{ width: "44px", height: "44px" }}
                title="lien vers ma carte"
              />
            </Control>

            <LayersControl position="topright">
              <LayersControl.BaseLayer name="OpenStreet Map" checked>
                <TileLayer
                  attribution='&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png"
                />
              </LayersControl.BaseLayer>
              <LayersControl.BaseLayer name="ArcGIS World Topo Map">
                <TileLayer
                  attribution="Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community"
                  url="http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}"
                />
              </LayersControl.BaseLayer>

              <LayersControl.BaseLayer name="IGN photos aériennes">
                <TileLayer
                  attribution="IGN-F/Géoportail"
                  url="https://wxs.ign.fr/pratique/geoportail/wmts?&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}"
                />
              </LayersControl.BaseLayer>
            </LayersControl>
          </Map>
        </div>

        <div className="p-col-12 p-md-12">
          <DataTable
            ref={(el) => (this.dt = el)}
            headerColumnGroup={headerGroup}
            value={this.state.mapDatasJournalieresParcelle}
            emptyMessage={
              this.state.loadingParcelles
                ? loadingDatatable
                : "Aucune situation"
            }
            style={{ marginBottom: "20px" }}
            responsive={true}
          >
            <Column field="parcelle.nom" />
            <Column field="parcelle.typeVin.label" />
            <Column field="parcelle.typeSol.label" />
            <Column field="parcelle.phenologie.label" />

            {/* <Column body={this.displaySmiley}  field="conseil"  /> */}
            <Column field="data.conseil.libelle" />

            <Column
              body={this.actionTemplate}
              style={{ textAlign: "center", width: "8em" }}
            />
          </DataTable>
        </div>
      </div>
    );
  }
}
