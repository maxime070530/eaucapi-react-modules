import React, { Component } from "react";
import axios from "axios";
import { Button } from "primereact/button";
import { UtilisateurService } from "../../services/UtilisateurService";
import { Checkbox } from "primereact/checkbox";
import { HabilitationService } from "../../services/HabilitationService";
import { InputText } from "primereact/inputtext";
import { Growl } from "primereact/growl";
import { Message } from "primereact/message";
import { Spinner } from "primereact/spinner";
import { InputMask } from "primereact/inputmask";

import { Redirect } from 'react-router-dom'

const CancelToken = axios.CancelToken;

export class Utilisateur extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      isModification: false,

      buttonSendAccessible: false,

      listesAllRoles: [],
      selectedRoles: [],

      source: CancelToken.source(),

      utilisateur: {
        name: "",
      },
    };

    this.utilisateurService = new UtilisateurService();
    this.habilitationService = new HabilitationService();

    this.updateUtilisateur = this.updateUtilisateur.bind(this);
    this.annuler = this.annuler.bind(this);

    this.onRoleChange = this.onRoleChange.bind(this);
    this.onActifChange = this.onActifChange.bind(this);
  }

  componentDidMount() {
    this.setState({ isLoading: true });

    this.utilisateurService.loadUtilisateur(
      this.props.location.state.idUtilisateur,
      this,
      this.state.source
    );
    this.utilisateurService.loadAllRoles(this, this.state.source);
  }

  updateUtilisateur() {
    var utilisateur = this.state.utilisateur;
    var errorsMessagesTexte = [];

    this.setState({ errorEmail: false });
    this.setState({ errorPassword: false });
    this.setState({ errorNom: false });
    this.setState({ errorPrenom: false });

    if (!utilisateur.email) {
      this.setState({ errorEmail: true });
      errorsMessagesTexte.push("Email");
    }

    // else if (!EmailValidator.validate(utilisateur.email)) {

    // 	this.setState({ errorEmail: true });
    // 	errorsMessagesTexte.push('Email non valide');
    // }

    if (!utilisateur.nom) {
      this.setState({ errorNom: true });
      errorsMessagesTexte.push("Nom");
    }

    if (!utilisateur.prenom) {
      this.setState({ errorPrenom: true });
      errorsMessagesTexte.push("Prénom");
    }

    if (!utilisateur.nbrSituations || utilisateur.nbrSituations < 0) {
      this.setState({ errorNbrSituation: true });
      errorsMessagesTexte.push("Nombre de situation");
    }

    if (errorsMessagesTexte.length > 0) {
      this.state.errorsMessages.show({
        severity: "error",
        summary: "Champs non renseignés : ",
        detail: errorsMessagesTexte.join(", "),
      });
    } else {
      // call ws pour save
      utilisateur.roles = this.state.selectedRoles;

      this.utilisateurService.updateUtilisateur(utilisateur, this);
    }
  }

  annuler() {
    this.setState({ isModification: false });
    this.utilisateurService.loadUtilisateur(
      this.props.location.state.idUtilisateur,
      this,
      this.state.source
    );
    this.utilisateurService.loadAllRoles(this, this.state.source);
  }

  onRoleChange(e) {
    var selectedRoles = [...this.state.selectedRoles];

    if (e.checked) {
      selectedRoles.push(e.value);
    } else {
      selectedRoles.splice(selectedRoles.indexOf(e.value), 1);
    }

    this.setState({ selectedRoles: selectedRoles });
  }

  onActifChange(e) {
    var utilisateur = this.state.utilisateur;

    if (e.checked) {
      utilisateur.actif = true;
    } else {
      utilisateur.actif = false;
    }

    this.setState({ utilisateur: utilisateur });
  }

  render() {
    if (!this.habilitationService.isAuthorize("ADMIN")) {
      return <Redirect to="/" />;
    }

    // let loading = <div >
    // <div style={{ width: '50px', margin: '0 auto' }}>
    //     <RingLoader  margin={150}
    //         sizeUnit={"px"}
    //         size={50}
    //         color={'#123abc'}
    //         loading={true} style={{ margin: '0 auto' }} /></div>

    // </div>

    return (
      <div>
        <Growl ref={(el) => (this.state.errorsMessages = el)} />

        {this.state.confirmUpdate && (
          <div>
            <Message
              style={{ width: "100%", textAlign: "center", fontSize: "15px" }}
              severity="info"
              text="Modification effectuée"
            ></Message>{" "}
            <br />
          </div>
        )}

        <div className="p-grid">
          <div className="p-col-12">
            <div className="card card-w-title">
              <h1> {this.state.utilisateur.name} </h1>

              <div style={{ marginTop: "-50px" }}>
                {!this.state.isModification && (
                  <Button
                    icon="pi-md-edit"
                    style={{ float: "right" }}
                    className="deep-orange-btn"
                    onClick={(e) => {
                      this.setState({ isModification: true });
                      this.state.source.cancel(
                        "Operation canceled situation - modification."
                      );
                      this.setState({ source: CancelToken.source() });
                    }}
                  />
                )}
                {this.state.isModification && (
                  <div style={{ float: "right", background: "transparent" }}>
                    <Button
                      icon="pi pi-times"
                      className="p-button-secondary"
                      onClick={(e) => {
                        this.annuler();
                        this.setState({ isModification: false });
                      }}
                    />
                    <Button
                      icon="pi-md-check"
                      className="p-button-success"
                      style={{ marginLeft: "5px" }}
                      onClick={(e) => {
                        this.updateUtilisateur();
                      }}
                    />
                  </div>
                )}
              </div>

              <div style={{ marginTop: "80px" }}>
                {/* // ici parcours des roles !!!! */}

                <div className="p-grid">
                  <div className="p-col-12" style={{ padding: "15px" }}>
                    {this.state.utilisateur.image && (
                      <img
                        src={this.state.utilisateur.image}
                        alt="conseil irrigation"
                        width="80"
                      />
                    )}
                  </div>
                  <div className="p-col-6" style={{ padding: "15px" }}>
                    <span className="md-inputfield">
                      <InputText
                        className={this.state.errorEmail ? "p-error" : ""}
                        style={{ width: "80%" }}
                        value={this.state.utilisateur.email}
                        onChange={(e) => {
                          var util = this.state.utilisateur;
                          util.email = e.target.value;
                          this.setState({ utilisateur: util });
                        }}
                        disabled={true}
                      />
                      <label>Email</label>
                    </span>
                  </div>
                  <div className="p-col-6" style={{ padding: "15px" }}>
                    <label>Actif</label>
                    <br />
                    <Checkbox
                      inputId="is_actif"
                      value={true}
                      onChange={this.onActifChange}
                      checked={this.state.utilisateur.actif}
                      disabled={!this.state.isModification}
                    ></Checkbox>
                  </div>

                  <div className="p-col-6" style={{ padding: "15px" }}>
                    <span className="md-inputfield">
                      <InputText
                        className={this.state.errorNom ? "p-error" : ""}
                        style={{ width: "80%" }}
                        value={this.state.utilisateur.nom}
                        onChange={(e) => {
                          var util = this.state.utilisateur;
                          util.nom = e.target.value;
                          this.setState({ utilisateur: util });
                        }}
                        disabled={!this.state.isModification}
                      />
                      <label>Nom</label>
                    </span>
                  </div>
                  <div className="p-col-6" style={{ padding: "15px" }}>
                    <span className="md-inputfield">
                      <InputText
                        className={this.state.errorPrenom ? "p-error" : ""}
                        style={{ width: "80%" }}
                        value={this.state.utilisateur.prenom}
                        onChange={(e) => {
                          var util = this.state.utilisateur;
                          util.prenom = e.target.value;
                          this.setState({ utilisateur: util });
                        }}
                        disabled={!this.state.isModification}
                      />
                      <label>Prénom</label>
                    </span>
                  </div>

                  <div className="p-col-6" style={{ padding: "15px" }}>
                    <div className="p-inputgroup">
                      <span className="p-inputgroup-addon">
                        <i className="material-icons">today</i>
                      </span>
                      <InputMask
                        id="date_naissance"
                        mask="99/99/9999"
                        className={
                          this.state.errorDateNaissance ? "p-error" : ""
                        }
                        appendTo={document.body}
                        placeholder="Date de naissance"
                        showIcon={true}
                        value={this.state.utilisateur.dateNaissance}
                        disabled={true}
                      />
                    </div>
                  </div>

                  <div className="p-col-6" style={{ padding: "15px" }}>
                    <span className="md-inputfield">
                      <InputText
                        style={{ width: "80%" }}
                        value={this.state.utilisateur.telephone}
                        disabled={true}
                      />
                      <label>Téléphone</label>
                    </span>
                  </div>

                  <div className="p-col-6" style={{ padding: "15px" }}>
                    <span className="md-inputfield">
                      <InputText
                        className={this.state.errorAbonnement ? "p-error" : ""}
                        style={{ width: "80%" }}
                        value={this.state.utilisateur.abonnement}
                        disabled={true}
                      />
                      <label>Abonnement</label>
                    </span>
                  </div>

                  <div className="p-col-6" style={{ padding: "15px" }}>
                    <span className="md-inputfield">
                      <Spinner
                        className={
                          this.state.errorNbrSituation ? "p-error" : ""
                        }
                        value={this.state.utilisateur.nbrSituations}
                        disabled={true}
                      />
                      <label>Nombre de situation</label>
                    </span>
                  </div>

                  <div className="p-col-6" style={{ padding: "15px" }}>
                    <span className="md-inputfield">
                      <Spinner
                        className={
                          this.state.errorNbrSituation ? "p-error" : ""
                        }
                        value={this.state.utilisateur.nbrSituations}
                        onChange={(e) => {
                          var util = this.state.utilisateur;
                          util.nbrSituations = e.target.value;
                          this.setState({ utilisateur: util });
                        }}
                        disabled={!this.state.isModification}
                      />
                      <label>Prix</label>
                    </span>
                  </div>
                </div>

                <h3>Autorisations</h3>

                <div className="p-grid">
                  {this.state.listesAllRoles.map(
                    function (value, index) {
                      //  alert(JSON.stringify(value))

                      return (
                        <div className="p-col-4">
                          <Checkbox
                            inputId={"cb" + index}
                            value={value.code}
                            onChange={this.onRoleChange}
                            checked={this.state.selectedRoles.includes(
                              value.code
                            )}
                            disabled={!this.state.isModification}
                          ></Checkbox>
                          <label
                            htmlFor={"cb" + index}
                            className="p-checkbox-label"
                          >
                            {value.libelle}
                          </label>
                        </div>
                      );
                    }.bind(this)
                  )}
                </div>

                {this.state.isModification && (
                  <div>
                    <br />
                    <br />
                    <h3>Envoyer un nouveau email pour création mot de passe</h3>

                    <div className="p-grid">
                      <Button
                        disabled={!this.state.buttonSendAccessible}
                        icon="pi-md-send"
                        label="envoyer"
                        className="p-button deep-orange-btn"
                        onClick={(e) => {
                          this.utilisateurService.generateEmailLinkMDP(
                            this,
                            this.state.utilisateur.id
                          );
                        }}
                      />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
