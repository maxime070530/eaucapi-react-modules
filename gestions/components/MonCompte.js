import React, { Component } from "react";
import { InputText } from "primereact/inputtext";
import { UtilisateurService } from "../../services/UtilisateurService";

import { Checkbox } from "primereact/checkbox";

import axios from "axios";
import { RadioButton } from "primereact/radiobutton";

const CancelToken = axios.CancelToken;

export default class MonCompte extends Component {
  constructor() {
    super();
    this.state = {
      source: CancelToken.source(),

      utilisateur: {
        email: "",
        nom: "",
        prenom: "",
      },

      checkedRappelAvantFinModification: false,
    };

    this.utilisateurService = new UtilisateurService();
  }

  componentDidMount() {
    this.utilisateurService.loadInfoUtilisateur(this, this.state.source);
    this.setState({
      checkedRappelAvantFinModification:
        localStorage.getItem("ignore_rappel") === "true" ? true : false,
    });
  }

  render() {
    return (
      <div>
        <div className="p-grid">
          <div className="p-col-12 p-md-6 p-lg-6">
            <div className="card p-fluid">
              <h3>Mon compte</h3>

              <div className="p-grid">
                <div className="p-col-6" style={{ padding: "15px" }}>
                  <span className="md-inputfield">
                    <InputText
                      className={this.state.errorEmail ? "p-error" : ""}
                      disabled={true}
                      value={this.state.utilisateur.email}
                      onChange={(e) => {
                        var util = this.state.utilisateur;
                        util.email = e.target.value;
                        this.setState({ utilisateur: util });
                      }}
                      onKeyPress={this.connectEnterKey}
                    />
                    <label>Email</label>
                  </span>
                </div>

                <div className="p-col-6" style={{ padding: "15px" }}></div>

                <div className="p-col-6" style={{ padding: "15px" }}>
                  <span className="md-inputfield">
                    <InputText
                      className={this.state.errorNom ? "p-error" : ""}
                      disabled={true}
                      value={this.state.utilisateur.nom}
                      onChange={(e) => {
                        var util = this.state.utilisateur;
                        util.nom = e.target.value;
                        this.setState({ utilisateur: util });
                      }}
                      onKeyPress={this.connectEnterKey}
                    />
                    <label>Nom</label>
                  </span>
                </div>
                <div className="p-col-6" style={{ padding: "15px" }}>
                  <span className="md-inputfield">
                    <InputText
                      className={this.state.errorPrenom ? "p-error" : ""}
                      disabled={true}
                      value={this.state.utilisateur.prenom}
                      onChange={(e) => {
                        var util = this.state.utilisateur;
                        util.prenom = e.target.value;
                        this.setState({ utilisateur: util });
                      }}
                      onKeyPress={this.connectEnterKey}
                    />
                    <label>Prénom</label>
                  </span>
                </div>

                <div className="p-col-6" style={{ padding: "15px" }}>
                  <span className="md-inputfield">
                    <InputText
                      disabled={true}
                      value={this.state.utilisateur.dateNaissance}
                    />
                    <label>Date naissance</label>
                  </span>
                </div>

                <div className="p-col-6" style={{ padding: "15px" }}>
                  <span className="md-inputfield">
                    <InputText
                      disabled={true}
                      value={this.state.utilisateur.telephone}
                    />
                    <label>Téléphone</label>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="p-col-12 p-md-6 p-lg-6">
            <div className="card p-fluid">
              <h3>Mon Abonnement</h3>

              <div className="p-grid">
                <div className="p-col-6" style={{ padding: "15px" }}>
                  <div className="pricing-content">
                    <ul>
                      <li>
                        <span>{this.state.utilisateur.abonnement}</span>
                      </li>
                      <li>{this.state.utilisateur.nbrSituations} situations</li>
                      <li>{this.state.utilisateur.prix}€</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="p-col-12 p-md-6 p-lg-6">
            <div className="card p-fluid">
              <h3>Consentement RGPD</h3>

              <label>
                Je souhaite être informé des nouvelles fonctionnalités de notre
                outil irrigation
              </label>

              <br />
              <br />

              <div className="p-grid ">
                <div className="p-col-6">
                  <RadioButton
                    id="v_q1_accepte"
                    value={true}
                    name="question1"
                    onChange={(e) => {
                      var util = this.state.utilisateur;
                      util.consentementNewsFct = e.value;
                      this.setState({ utilisateur: util });
                      this.utilisateurService.updateConsentementRGPD(
                        "consentementNewsFct",
                        e.value,
                        this.state.source
                      );
                    }}
                    checked={this.state.utilisateur.consentementNewsFct}
                    className={this.state.errorQ1 ? "p-error" : ""}
                  />
                  <label htmlFor="v_q1_accepte" className="p-radiobutton-label">
                    Accepter
                  </label>
                </div>

                <div className="p-col-6">
                  <RadioButton
                    id="v_q1_refus"
                    value={false}
                    name="question1"
                    onChange={(e) => {
                      var util = this.state.utilisateur;
                      util.consentementNewsFct = e.value;
                      this.setState({ utilisateur: util });
                      this.utilisateurService.updateConsentementRGPD(
                        "consentementNewsFct",
                        e.value,
                        this.state.source
                      );
                    }}
                    checked={!this.state.utilisateur.consentementNewsFct}
                    className={this.state.errorQ1 ? "p-error" : ""}
                  />
                  <label htmlFor="v_q1_refus" className="p-radiobutton-label">
                    Refuser
                  </label>
                </div>
              </div>

              <br />
              <br />
              <label>
                Je souhaite être informé des offres d’abonnement disponibles en
                2021
              </label>

              <br />
              <br />

              <div className="p-grid ">
                <div className="p-col-6">
                  <RadioButton
                    id="v_q2_accepte"
                    value={true}
                    name="question2"
                    onChange={(e) => {
                      var util = this.state.utilisateur;
                      util.consentementOffres = e.value;
                      this.setState({ utilisateur: util });
                      this.utilisateurService.updateConsentementRGPD(
                        "consentementOffres",
                        e.value,
                        this.state.source
                      );
                    }}
                    checked={this.state.utilisateur.consentementOffres}
                    className={this.state.errorQ2 ? "p-error" : ""}
                  />
                  <label htmlFor="v_q2_accepte" className="p-radiobutton-label">
                    Accepter
                  </label>
                </div>

                <div className="p-col-6">
                  <RadioButton
                    id="v_q2_refus"
                    value={false}
                    name="question2"
                    onChange={(e) => {
                      var util = this.state.utilisateur;
                      util.consentementOffres = e.value;
                      this.setState({ utilisateur: util });
                      this.utilisateurService.updateConsentementRGPD(
                        "consentementOffres",
                        e.value,
                        this.state.source
                      );
                    }}
                    checked={!this.state.utilisateur.consentementOffres}
                    className={this.state.errorQ2 ? "p-error" : ""}
                  />
                  <label htmlFor="v_q2_refus" className="p-radiobutton-label">
                    Refuser
                  </label>
                </div>
              </div>

              <br />
              <br />
              <label>
                Nous utilisons des cookies et des informations non sensibles de
                votre appareil pour évaluer les performances de notre outil
              </label>

              <br />
              <br />

              <div className="p-grid ">
                <div className="p-col-6">
                  <RadioButton
                    id="v_q3_accepte"
                    value={true}
                    name="question3"
                    onChange={(e) => {
                      var util = this.state.utilisateur;
                      util.consentementAnalytics = e.value;
                      this.setState({ utilisateur: util });
                      this.utilisateurService.updateConsentementRGPD(
                        "consentementAnalytics",
                        e.value,
                        this.state.source
                      );
                    }}
                    checked={this.state.utilisateur.consentementAnalytics}
                    className={this.state.errorQ3 ? "p-error" : ""}
                  />
                  <label htmlFor="v_q3_accepte" className="p-radiobutton-label">
                    Accepter
                  </label>
                </div>

                <div className="p-col-6">
                  <RadioButton
                    id="v_q3_refus"
                    value={false}
                    name="question3"
                    onChange={(e) => {
                      var util = this.state.utilisateur;
                      util.consentementAnalytics = e.value;
                      this.setState({ utilisateur: util });
                      this.utilisateurService.updateConsentementRGPD(
                        "consentementAnalytics",
                        e.value,
                        this.state.source
                      );
                    }}
                    checked={!this.state.utilisateur.consentementAnalytics}
                    className={this.state.errorQ3 ? "p-error" : ""}
                  />
                  <label htmlFor="v_q3_refus" className="p-radiobutton-label">
                    Refuser
                  </label>
                </div>
              </div>
            </div>
          </div>

          <div className="p-col-12 p-md-6 p-lg-6"></div>

          <div className="p-col-12 p-md-6 p-lg-6">
            <div className="card p-fluid">
              <h3>Paramétrages</h3>

              <div className="p-grid">
                <div className="p-col-6" style={{ padding: "15px" }}>
                  <Checkbox
                    onChange={(e) => {
                      localStorage.setItem("ignore_rappel", e.checked);
                      this.setState({
                        checkedRappelAvantFinModification: e.checked,
                      });
                      this.props.callUpdateTopBar();
                    }}
                    checked={this.state.checkedRappelAvantFinModification}
                  ></Checkbox>
                  Je ne souhaite plus voir apparaitre le message de rappel
                  concernant le délai de modification des situations
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
